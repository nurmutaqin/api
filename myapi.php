<?php

header("Access-Control-Allow-Origin: *");
error_reporting(E_ALL);
ini_set('display_errors',1);
require_once "services/API.class.php"; 
require_once "lib/API.helper.php";

class MyAPI extends API {
    private $service_alias = array("lowfares"=>"lowfares", "reservation-book"=>"PNR_2", "reservation-issue"=>"issue");            

    public function MyAPI($request) {
        $r = API_Helper::convertArray($request, false);                

        parent::__construct($r);
    }

    public static function request() {        
        $request = json_decode(file_get_contents('php://input'));
        
        //strpos($user_agent, "okhttp") !== false && 
        if (!empty($request) && ("reservation-book" === $request->service || 
            "check-harga" === $request->service || 
            "hotel-search" === $request->service || 
            "hotel-cancel-policy" === $request->service || 
            "hotel-book" === $request->service ||
            "deposit-topup-confirmation" === $request->service ||
            "deposit-topup-request" === $request->service ||
            "hotel-detail-rooms" === $request->service || 
            "train-book" === $request->service ||
            "train-takeseat" === $request->service ||
            "book-flight-airlines" === $request->service ||
            "shuttle-bus-book" === $request->service
            )) 
        { //only for these of services            
            return $request;
        }
        
        else {
            return $request = filter_input_array(INPUT_POST);
        }
    }        

    /**
        * International Sabre
        * 
        * @return type
        * @throws Exception
        *      
        */    

    protected function lowfares() {
        try {
    //            echo (json_encode($this->args->service)); exit;            
            $params = array(
                "service"=>$this->args->service,
                "username"=>$this->args->username,
                "token"=>$this->args->apikey,
                "origin"=>$this->args->origin,
                "destination"=>$this->args->destination,
                "depdate"=>$this->args->depdate,
                "retdate"=>$this->args->retdate,
                "airline"=>$this->args->airline,
                "adult"=>$this->args->adult,
                "child"=>$this->args->child,
                "infant"=>$this->args->infant,
                "airline_all"=>$this->args->airline_all
            );
    //            echo $this->endpoint;
    //            echo $this->execute($params); exit;
            API_Helper::validateForms($this->args, $this->config->PARAM_SEARCH);            
            $response = json_encode($this->handler->processXml($this->endpoint, json_decode($this->execute($params))));

            $this->logger->log("\r\n\r\nPARAMS: {$this->args->service}\r\n\r\n" . print_r($params, true));
            $this->logger->log("\r\n\r\nRESPONSE: \r\n\r\n" . $response);
            
            return $response;

        } catch (Exception $e) {
            throw $e;
        }
    }

    protected function reservation_book_domestik() {
		if($this->args->adult){
			$adultchilds = $this->args->adultchilds;
			$data_adt = array();
			for($i=0;$i<count($adultchilds);$i++){
				$salutation = $adultchilds[$i]->salutation;
				$firstname = $adultchilds[$i]->firstname;
				$lastname = $adultchilds[$i]->lastname;
				$docnumber = $adultchilds[$i]->docnumber;
				$birthdate = $adultchilds[$i]->birthdate;
				$passport_number = $adultchilds[$i]->passport_number;
				$passport_issued = $adultchilds[$i]->passport_issued;
				$passport_expired = $adultchilds[$i]->passport_expired;
				$passport_issuecountry = $adultchilds[$i]->passport_issuecountry;
				$passport_birthcountry = $adultchilds[$i]->passport_birthcountry;
				$passport_nasionalitydocument = $adultchilds[$i]->passport_nasionalitydocument;
				$baggage = $adultchilds[$i]->baggage;
				$baggage_text = $adultchilds[$i]->baggage_text;
				$baggage_plg = $adultchilds[$i]->baggage_pulang;
				$baggage_plg_text = $adultchilds[$i]->baggage_pulang_text;

				$data_adt[$i]["salutation"] = $salutation;
				$data_adt[$i]["firstname"] = $firstname;
				$data_adt[$i]["lastname"] = $lastname;
				$data_adt[$i]["docnumber"] = $docnumber;
				$data_adt[$i]["birthdate"] = $birthdate;
				$data_adt[$i]["adultchild_passport_number"] = $passport_number;
				$data_adt[$i]["adultchild_passport_issued"] = $passport_issued;
				$data_adt[$i]["adultchild_passport_expired"] = $passport_expired;
				$data_adt[$i]["adultchild_passport_issuecountry"] = $passport_issuecountry;
				$data_adt[$i]["adultchild_passport_birthcountry"] = $passport_birthcountry;
				$data_adt[$i]["adultchild_passport_nasionalitydocument"] = $passport_nasionalitydocument;
				$data_adt[$i]["adultchild_birthdate"] = $birthdate;
				$data_adt[$i]["adultchild_baggage"] = $baggage;
				$data_adt[$i]["adultchild_baggage_text"] = $baggage_text;
				if(!empty($baggage_plg)){
					$data_adt[$i]["adultchild_baggage_pulang"] = $baggage_plg;
					$data_adt[$i]["adultchild_baggage_pulang_text"] = $baggage_plg_text;
				}
			}
			$adultchild = array(
				"data"=> $data_adt,
				"_count"=> $this->args->adult
			);
		}
		$data_chd = array();
		if($this->args->child){
			$childs = $this->args->childs;
			for($i=0;$i<count($childs);$i++){
				$salutation = $childs[$i]->salutation;
				$firstname = $childs[$i]->firstname;
				$lastname = $childs[$i]->lastname;
				$birthdate = $childs[$i]->birthdate;
				$passport_expired = $childs[$i]->passport_expired;
				$passport_issuecountry = $childs[$i]->passport_issuecountry;
				$passport_nasionalitydocument = $childs[$i]->passport_nasionalitydocument;
				$baggage = $childs[$i]->baggage;
				$baggage_text = $childs[$i]->baggage_text;
				$baggage_plg = $childs[$i]->baggage_pulang;
				$baggage_plg_text = $childs[$i]->baggage_pulang_text;
				
				$data_chd[$i]["salutation"] = $salutation;
				$data_chd[$i]["firstname"] = $firstname;
				$data_chd[$i]["lastname"] = $lastname;
				$data_chd[$i]["birthdate"] = $birthdate;
				$data_chd[$i]["child_passport_expired"] = $passport_expired;
				$data_chd[$i]["child_passport_issuecountry"] = $passport_issuecountry;
				$data_chd[$i]["child_passport_nasionalitydocument"] = $passport_nasionalitydocument;
				$data_chd[$i]["child_baggage"] = $baggage;
				$data_chd[$i]["child_baggage_text"] = $baggage_text;
				if(!empty($baggage_plg)){
					$data_chd[$i]["child_baggage_pulang"] = $baggage_plg;
					$data_chd[$i]["child_baggage_pulang_text"] = $baggage_plg_text;
				}
			}
		}
		$child = array("data"=> $data_chd,"_count"=>$this->args->child);
		$data_inf = array();
		if($this->args->infant){
			$infants = $this->args->infants;
			for($i=0;$i<count($infants);$i++){
				$salutation = $infants[$i]->salutation;
				$firstname = $infants[$i]->firstname;
				$lastname = $infants[$i]->lastname;
				$birthdate = $infants[$i]->birthdate;
				$parent = $infants[$i]->parent;
				$passport_expired = $infants[$i]->passport_expired;
				$passport_issuecountry = $infants[$i]->passport_issuecountry;
				$passport_nasionalitydocument = $infants[$i]->passport_nasionalitydocument;
				
				$data_inf[$i]["salutation"] = $salutation;
				$data_inf[$i]["firstname"] = $firstname;
				$data_inf[$i]["lastname"] = $lastname;
				$data_inf[$i]["birthdate"] = $birthdate;
				$data_inf[$i]["parent"] = $parent;
				$data_inf[$i]["infant_passport_expired"] = $passport_expired;
				$data_inf[$i]["infant_passport_issuecountry"] = $passport_issuecountry;
				$data_inf[$i]["infant_passport_nasionalitydocument"] = $passport_nasionalitydocument;
			}
		}
		$infant = array("data"=> $data_inf,"_count"=>$this->args->infant);

		$request_pp = "1";
		
		$args["function"] = "api.tickettransactions.reservations.book";
		if($this->args->airline == 10){
			$params = array(
							"merchant_id" => $this->info_->user->merchant_id,
							"booking_user_id" => $this->info_->user->id,
							"contact_name" => $this->args->nm_contact,
							"contact_telp" => $this->args->tlp_contact,
							"airlines_id" => $this->args->airline,
							"email" => $this->args->email,
							"luar_negeri" => 1,
							"data_adult" => $adultchild,
							"data_child" => $child,
							"data_infant" => $infant,
							"berangkat" => $this->args->origin,
							"tujuan" => $this->args->destination,
							"tglpergi" => $this->args->depdate,
							"dewasa" => $this->args->adult,
							"anak" => $this->args->child,
							"bayi" => $this->args->infant,
							"username" => $this->info_->user->name,
							"booking_rute_berangkat" => $this->args->origin,
							"booking_rute_tujuan" => $this->args->destination
						);
		} else if($this->args->airline == 2){
			$jml_transit_pergi = count($this->args->data_search_pergi[0]->flightno);
			if($jml_transit_pergi == 1){
				$jml_transit_pergi = 0;
			}
			$params = array(
				"merchant_id" => $this->info_->user->merchant_id,
				"user_id" => $this->info_->user->id,
				"nm_contact" => $this->args->nm_contact,
				"tlp_contact" => $this->args->tlp_contact,
				"booking_request_pp" => $request_pp,
				"data_search_pergi" => $this->args->data_search_pergi,
				"data_search_pulang" => $this->args->data_search_pulang,
				"jml_transit_pergi" => $jml_transit_pergi,
				"airlines_id" => 17,
				"email" => $this->args->email,
				"adultchild" => $adultchild,
				"child" => $child,
				"infant" => $infant,
				"booking_rute_berangkat" => $this->args->origin,
				"booking_rute_tujuan" => $this->args->destination,
				"booking_request_tglpergi" => $this->args->depdate,
				"booking_request_dewasa" => $this->args->adult,
				"booking_request_anak" => $this->args->child,
				"booking_request_bayi" => $this->args->infant,
				"username" => $this->info_->user->name
			);
		}
		
		if(!empty($this->args->retdate)){
			$jml_transit_pulang = count($this->args->data_search_pulang[0]->flightno);
			if($jml_transit_pulang == 1){
				$jml_transit_pulang = 0;
			}
			$request_pp = "2";
			if($this->args->airline == 2){
				$add_params = array("booking_request_tglpulang" => $this->args->retdate,"jml_transit_pulang" => $jml_transit_pulang);
			} else if($this->args->airline == 10){
				$add_params = array("tglpulang" => $this->args->retdate);
			}
			$add_params2 = array("booking_request_pp" => $request_pp);
			$add_params = array_merge($add_params, $add_params2);
			$params = array_merge($params, $add_params);
		}
		
		for($i=0;$i<count($this->args->seatId_pergi);$i++){
			$seatId_pergi = $this->args->seatId_pergi[$i];
			if($i==0){
				$args_ext = array("seatId_pergi" => $seatId_pergi);
			} else {
				$args_ext = array("seatId_pergi$i" => $seatId_pergi);
			}
			$params = array_merge($params, $args_ext);
		}
		
		for($i=0;$i<count($this->args->seatId_pulang);$i++){
			$seatId_pulang = $this->args->seatId_pulang[$i];
			if($i==0){
				$args_ext = array("seatId_pulang" => $seatId_pulang);
			} else {
				$args_ext = array("seatId_pulang$i" => $seatId_pulang);
			}
			$params = array_merge($params, $args_ext);
		}
		
		if($this->args->airline == 10){
			$args["input"] = array("data_p"=>$params,
							"airlines_id" => $this->args->airline);
		} else if($this->args->airline == 2) {
			$args["input"] = array("data_p"=>$params,
							"book_new" => "book_new");
		}
		$args = json_encode($args);
		$save = json_decode($this->filterResponse($this->execute(array("r"=>$args))));
		$response_save = $this->handler->processXml($this->endpoint, $save);
		return json_encode($response_save);
	}
	
    protected function reservation_book() {
        API_Helper::validateForms($this->args, $this->config->PARAM_BOOK);
        
		if($this->args->airline == '10' || $this->args->airline == '2'){
			$this->target_server = $this->baseUrl . "/" . $this->ibe_ws;
			$this->args->service = "reservation-book-domestik";
			$this->endpoint = "reservation_book_domestik";
			$response = $this->reservation_book_domestik();
			return $response;
		}
        $params = array();
        $params["service"] = $this->service_alias[$this->args->service];
        $params["search_type"] = "bfm";
        $params["username"] = $this->args->username;
        $params["apikey"] = $this->args->apikey;
        $params["booking_request_pp"] = (!empty($this->args->retdate) ? '2' : '1');
        $params["booking_rute_berangkat"] = $this->args->origin;
        $params["booking_rute_tujuan"] = $this->args->destination;
    //            $params["booking_rute_kembali"] = $this->args->return;
        $params["booking_request_tglpergi"] = $this->args->depdate;
        $params["booking_request_tglpulang"] = $this->args->retdate;
        $params["booking_request_dewasa"] = $this->args->adult;
        $params["booking_request_anak"] = $this->args->child;
        $params["booking_request_bayi"] = $this->args->infant;
        $params["airlines_id"] = 99;
        $params["searched_airline"] = $this->args->airline;
        $params["totalbasic"] = $this->args->total_basic;
        $params["totaltax"] = $this->args->total_tax;
        $params["totaljumlah"] = $this->args->total_pay;
    //            $params["max_transit_pergi"] = $this->args->max_transit_pergi;
    //            $params["max_transit_pulang"] = $this->args->max_transit_pulang;
    //            $params["jml_transit_pergi"] = $this->args->jml_transit_pergi;
    //            $params["jml_transit_pulang"] = $this->args->jml_transit_pulang;
        $params["insurance"] = 0;
        $params["c_route_departure"] = "Indonesia";
        $params["c_route_arrival"] = "Singapore";
        $params["nm_contact"] = $this->args->nm_contact;
        $params["tlp_countrycode"] = $this->args->tlp_countrycode;            
        $params["tlp_contact"] = $this->args->tlp_contact;
        $params["email"] = $this->args->email;
        
        $price_detail = array();
        
        foreach ($this->args->detail as $key => $value) {
            $price_detail[$key] = $value;
        }
        
        $seatid_dep_length = count($this->args->seatid_dep)-1;
        $seatid_ret_length = count($this->args->seatid_ret)-1;                

        foreach ($this->args->seatid_dep as $key=>$value) {
            $part = explode("|", $value);

            $deptime = explode("T", $part[5]);                
            $arrtime = explode("T", $part[6]);

            $params["flightNo_pergi"][] = $part[3] . $part[0];
            $params["additional_pergi"]["classID"][] = $part[7];
            $params["additional_pergi"]["flightNo"][] = $part[3] . $part[0];
            $params["additional_pergi"]["flightLoop"][] = 13;
            $params["additional_pergi"]["route"][] = "$part[1] - $part[2]";
            $params["additional_pergi"]["dep_time"][] = $deptime[1];
            $params["additional_pergi"]["arr_time"][] = $arrtime[1];
            $params["additional_pergi"]["paketTransit"][] = 0;

            $params["data_search_pergi"][0]["flightno"][] = "<b>$part[3]$part[0]</b>";
            $params["data_search_pergi"][0]["date"][] = "";
            $params["data_search_pergi"][0]["route"][] = "$part[1] - $part[2]";
            $params["data_search_pergi"][0]["depart_time"][] = $deptime[1];
            $params["data_search_pergi"][0]["arrive_time"][] = $arrtime[1];
            $params["data_search_pergi"][0]["classflight"][] = $part[7];
            $params["data_search_pergi"][0]["basic"][$seatid_dep_length] = $this->args->total_basic;
            $params["data_search_pergi"][0]["tax"][$seatid_dep_length] = $this->args->total_tax;
            $params["data_search_pergi"][0]["total"][$seatid_dep_length] = $this->args->total_pay;
            
            if (empty($this->args->retdate)) {
                $params["data_search_pergi"][0]["detail"] = API_Helper::convertArray($price_detail);
            }                        

            $index = ($key === 0 ? "" : $key);
            $params["seatId_pergi" . $index] = "$part[8]|$part[3]|$part[4]|$part[7]|$deptime[1]|$arrtime[1]|$part[0]|$part[1]|$part[2]|$part[5]|$part[6]";

        }
    //        print_r($params); exit;
        if (!empty($this->args->retdate)) {
            if (isset($this->args->seatid_ret) && count($this->args->seatid_ret) > 0) {
                foreach ($this->args->seatid_ret as $key=>$value) {
                    $part = explode("|", $value);
                    
                    $deptime = explode("T", $part[5]);
                    $arrtime = explode("T", $part[6]);

                    $params["flightNo_pulang"][] = $part[3] . $part[0];
                    $params["additional_pulang"]["classID"][] = $part[7];
                    $params["additional_pulang"]["flightNo"][] = $part[3] . $part[0];
                    $params["additional_pulang"]["flightLoop"][] = 13;
                    $params["additional_pulang"]["route"][] = "$part[1] - $part[2]";
                    $params["additional_pulang"]["dep_time"][] = $deptime[1];
                    $params["additional_pulang"]["arr_time"][] = $arrtime[1];
                    $params["additional_pulang"]["paketTransit"][] = 0;

                    $params["data_search_pulang"][0]["flightno"][] = "<b>$part[3]$part[0]</b>";
                    $params["data_search_pulang"][0]["date"][] = "";
                    $params["data_search_pulang"][0]["route"][] = "$part[1] - $part[2]";
                    $params["data_search_pulang"][0]["depart_time"][] = $deptime[1];
                    $params["data_search_pulang"][0]["arrive_time"][] = $arrtime[1];
                    $params["data_search_pulang"][0]["classflight"][] = $part[7];
                    $params["data_search_pulang"][0]["basic"][$seatid_ret_length] = $this->args->total_basic;
                    $params["data_search_pulang"][0]["tax"][$seatid_ret_length] = $this->args->total_tax;
                    $params["data_search_pulang"][0]["total"][$seatid_ret_length] = $this->args->total_pay;
                    $params["data_search_pulang"][0]["detail"] = API_Helper::convertArray($price_detail);

                    $index = ($key === 0 ? "" : $key);
                    $params["seatId_pulang" . $index] = "$part[8]|$part[3]|$part[4]|$part[7]|$deptime[1]|$arrtime[1]|$part[0]|$part[1]|$part[2]|$part[5]|$part[6]";
                }
            }
        }                        

        $passenger = array("childs"=>$this->args->childs, "infants"=>$this->args->infants);            
        foreach ($passenger as $k=>$v) {
            if (isset($v[0])) {
                if (count((array)$v[0])<1) {
                    $this->args->$k = array();
                }
            }
        }
        
        $params["adultchild"] = API_Helper::convertArray(API_Helper::restructData($this->args->adultchilds));
        $params["child"] = API_Helper::convertArray(API_Helper::restructData($this->args->childs));
        $params["infant"] = API_Helper::convertArray(API_Helper::restructData($this->args->infants));  
        $this->logger->log("REQUEST :\r\n\r\n" . print_r($params, true));
        $response = json_decode($this->filterResponse($this->execute($params)));         

        if (!isset($response->error)) {
            $this->logger->log("RESPONSE SABRE:\r\n\r\n" . print_r($response, true));            
            $this->target_server = $this->baseUrl . "/" . $this->ibe_ws;              
            
    //            $service = new Webservice_SRO($response);
    //            $user = $service->getUserInformation($this->args->username);
    //            
    //            echo "dsfsdf", print_r($user); exit;

    //            $passenger=new stdClass();
    //            $passenger->adultchilds = $params["adultchild"];
    //            $passenger->childs = $params["child"];
    //            $passenger->infants = $params["infant"];
    //
    //            $params2=API_Helper::convertArray($response, true);
    //            $params2["service"] = "reservation-book";
    //            $params2["Passenger"] = $passenger;
    //            $params2["username"] = $this->args->username;
    //            $params2["apikey"] = $this->args->apikey;
    //            $params2["airlines_id"] = 99;
    //            $params2["contact_name"] = $this->args->nm_contact;
    //            $params2["phone_area"] = $this->args->tlp_countrycode;
    //            $params2["contact_phone"] = $this->args->tlp_contact;
    //            $params2["contact_email"] = $this->args->email;
    //            $params2["seatid_dep"] = $this->args->seatid_dep;
    //            $params2["seatid_ret"] = $this->args->seatid_ret;
    //
    //            foreach ($this->args->seatid_dep as $key=>$value) {
    //                $index = ($key === 0 ? "" : $key);
    //                $params2["seatId_pergi".$index] = $params["seatId_pergi".$index];
    //            }
    //
    //            if (count($this->args->seatid_ret)>0) {
    //                foreach ($this->args->seatid_ret as $key=>$value) {
    //                    $index = ($key === 0 ? "" : $key);
    //                    $params2["seatId_pulang".$index] = $params["seatId_pulang".$index];
    //                }
    //            }
                                    
            $args["function"] = "api.tickettransactions.reservations.book";        
            $args["input"] = array(
                            "PNR" => $response->PNR,
                            "airlines_id" => 99,
                            "pnr_airline" => $response->pnr_airline,
                            "merchant_id" => $this->info_->user->merchant_id,
                            "booking_user_id" => $this->info_->user->id,
                            "booking_datetime" => date("Y-m-d H:i:s"),
                            "contact_name" => $this->args->nm_contact,
                            "phone_area" => $this->args->tlp_countrycode,
                            "contact_telp" => $this->args->tlp_contact,
                            "flights" => $response->TripDetail,
                            "timelimit" => $response->TimeLimit,
                            "paxpaid" => $response->PaxPaid,
                            "fareCalculation" => $response->FareCalculation,
                            "passengers" => $response->Passenger,
                            "email" => $this->args->email,
                            "commission" => $response->commission,
                            "pct_commission" => $response->pct_commission,
                            "nta" => $response->NTA,
                            "luar_negeri" => "",
                            "memory" => 0,
                            "username" => $this->args->username,
                            "international_wsvc"=>'international_wsvc',
                            "search_type"=>"bfm",
                            "total_basic"=>$this->args->total_basic,
                            "total_tax"=>$this->args->total_tax,
                            "total_paxprice"=>$this->args->total_pay,
                            "seatId_pergi"=>$params["seatId_pergi"]
                        );

    //            foreach ($r->seatid_dep as $key=>$value) {
    //                $index = ($key === 0 ? "" : $key);
    //                $args["input"]["seatId_pergi".$index] = $r->seatId_pergi.$index;
    //            }
    //
    //            if (count($r->seatid_ret)>0) {
    //                foreach ($r->seatid_ret as $key=>$value) {
    //                    $index = ($key === 0 ? "" : $key);
    //                    $args["input"]["seatId_pulang".$index] = $r->seatId_pulang.$index;
    //                }
    //            }
    //            print_r($args); exit;
            $args = json_encode($args);            
            $this->logger->log("\r\n\r\nPARAMS: {$this->args->service}\r\n\r\n " . print_r($args, true));
            
    //            $save = json_decode(file_get_contents("test/response_aws.json"));
//            file_put_contents("/var/www/html/sro/tes/response_save_to_ibe.log", print_r($args, true) . "\r\n", FILE_APPEND);
//            echo ($this->execute(array("r"=>$args))); exit;
            $save = json_decode($this->filterResponse($this->execute(array("r"=>$args))));
            $this->logger->log("\r\n\r\nTRIP DETAIL:\r\n\r\n" . print_r($save, true));
//            file_put_contents("/var/www/html/sro/tes/response_save_to_ibe.log", "Target Server: " . $this->target_server . "\r\n", FILE_APPEND);                  
            
            $response_save = $this->handler->processXml($this->endpoint, $save);
            $this->logger->log("\r\n\r\nSave to IBE:\r\n\r\n". print_r($response_save, true));
//            file_put_contents("/var/www/html/sro/tes/response_save_to_ibe.log", "TRIP DETAIL". print_r(save, true). "\r\n", FILE_APPEND);
//            file_put_contents("/var/www/html/sro/tes/response_save_to_ibe.log", "Save to IBE". print_r($response_save, true). "\r\n", FILE_APPEND);                                                            
            
            return json_encode($response_save);
        }

        else {
            throw new Exception($response->error);
        }
    }

    protected function reservation_issue() {
        try {
            API_Helper::validateForms($this->args, $this->config->PARAM_ISSUE);            
            
            if ($this->args->password !== $this->info_->user->password) {
                APIException::_throwException("Invalid password", null, 10007);
            }
            
            $session = array(
                "id" => $this->info_->session->id,
                "merchant_id" => $this->info_->user->merchant_id,
                "merchanttype_id" => $this->info_->user->merchanttype_id,
                "role_id" => $this->info_->user->role_id,
                "role_uri" => $this->info_->user->role_uri,
                "role_fullname" => $this->info_->user->role_fullname,
                "merchant_code" => $this->info_->user->merchant_code,
                "UUID" => $this->info_->session->uuid,
                "status_role" => $this->info_->user->status_role
            );                             
            
            //file_put_contents("/var/www/html/sro/api/test/response_login.log", "SESSION:\r\n".print_r($session, true). "\r\n");            
            
            $args["function"] = "api.tickettransactions.reservations.issue";
            $args["input"] = array(
                "id" => $this->args->reservation_id,
                "issue_user_id" => $this->info_->user->id,
                "username" => $this->info_->user->name,
                "merchant_id" => $this->info_->user->merchant_id,
                "foreign" => "foreign",
                "session" => $session
            );
            
            $args["session"] = array(
                "id" => $this->info_->session->id,
                "UUID" => $this->info_->session->uuid,
                "user_id" => $this->info_->session->user_id,
                "user_name" => $this->info_->session->user_name,
                "user_fullname" => $this->info_->session->user_fullname,
                "status_role" => $this->info_->session->status_role
            );
            
            $args = json_encode($args); //print_r($this->execute(array("r"=>$args))); exit;
            $exec = $this->filterResponse($this->execute(array("r"=>$args)));                                    
    //            $response = $this->filterResponse( file('test/response_issue_via_api.json', FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES) );                                    
            
//            file_put_contents("/var/www/html/sro/api/test/response_login.log", "RESPONSE:\r\n" . print_r($response, true), FILE_APPEND);    
            
            $response = json_encode($this->handler->processXml($this->endpoint, json_decode($exec)));
            
            $this->logger->log("\r\n\r\nPARAMS: {$this->args->service}\r\n\r\n" . print_r($args, true));            
            $this->logger->log("\r\n\r\nRESPONSE:\r\n\r\n". print_r($response, true));
            
            return $response;
            
        } catch (Exception $e) {
            throw $e;
        }       
    }        

    protected function check_balance() {
        API_Helper::validateForms($this->args, $this->config->PARAM_CHECKBALANCE);
        
        $args["function"] = "api.merchantmanager.merchants.getbyuserid";
        $args["input"] = array(
            "user_id" => $this->info_->user->id
        );
        
        $args["session"] = array(
            "id" => $this->info_->session->id,
            "UUID" => $this->info_->session->uuid,
            "user_id" => $this->info_->user->id,
            "user_name" => $this->info_->user->name,
            "user_fullname" => $this->info_->user->fullname,
        );
            
        $args = json_encode($args); //echo $args; exit;                
        
        $exec = $this->filterResponse($this->execute(array("r"=>$args)));
        $response = json_encode($this->handler->processXml($this->endpoint, json_decode($exec)));
        
        $this->logger->log("\r\n\r\nPARAMS: {$this->args->service}\r\n\r\n" . print_r($args, true));        
        $this->logger->log("\r\n\r\nRESPONSE:\r\n\r\n". print_r($response, true));
        
        return $response;
    }

    protected function read_pnr() {
        try {
            API_Helper::validateForms($this->args, $this->config->PARAM_READPNR);
            
			if(!empty($this->args->domestik)){
				$args["function"] = "api.tickettransactions.reservations.read";    
				$args["input"] = array(
					"user_id" => $this->info_->user->id,
					"username" => $this->args->username,
					"id" => $this->args->reservation_id,
					"merchant_id" => $this->info_->user->merchant_id
				);
			}
			else{
				$args["function"] = "api.tickettransactions.reservations.read";    
				$args["input"] = array(
					"user_id" => $this->info_->user->id,
					"username" => $this->args->username,
					"id" => $this->args->reservation_id,
					"merchant_id" => $this->info_->user->merchant_id,
					"foreign" => true
				);
			}
            $args = json_encode($args);
                        
            $exec = $this->filterResponse($this->execute(array("r"=>$args)));
            $response = json_encode($this->handler->processXml($this->endpoint, json_decode($exec)));
            
            $this->logger->log("\r\n\r\nPARAMS: {$this->args->service}\r\n\r\n" . print_r($args, true));
            $this->logger->log("\r\n\r\nRESPONSE: \r\n\r\n" . $response);
            
            return $response;

        } catch (Exception $e) {
            throw $e;
        }
    }        

    protected function sent_email() {
        $params=array();
        $params["service"] = $this->args->service;
        $params["username"] = $this->args->username;
        $params["apikey"] = $this->args->apikey;        
        $params["id"] = $this->args->reservation_id;
        
        API_Helper::validateForms($params, $this->config->PARAM_SENT_EMAIL);

        $args["function"] = "api.tickettransactions.reservations.ticket_email";    
        $args["input"] = array(
            "id" => $this->args->reservation_id,
            "username" => $this->args->username,
            "airlineid" => 99,
            "foreign" => "foreign"
        );
        $args = json_encode($args);                
        
        $exec = $this->filterResponse($this->execute(array("r"=>$args)));
        $response = json_encode($this->handler->processXml($this->endpoint, json_decode($exec)));
        
        $this->logger->log("\r\n\r\nPARAMS: {$this->args->service}\r\n\r\n" . print_r($args, true));            
        $this->logger->log("\r\n\r\nRESPONSE:\r\n\r\n". print_r($response, true));
        
        return $response;
    }

    protected function book_list() {
        try {
            API_Helper::validateForms($this->args, $this->config->PARAM_BOOK_LIST);
            
            $parameters = array("airline_code","contact_name", "pnr", "depdate", "arrdate", "bookby");
            if (
                !empty($this->args->airline_code) ||
                !empty($this->args->airlines_id) ||
                !empty($this->args->contact_name) ||
                !empty($this->args->pnr) ||
                !empty($this->args->depdate) ||
                !empty($this->args->arrdate) ||
                !empty($this->args->bookby) ||
                !empty($this->args->status)
                ) 
            {
				if(!empty($this->args->domestik)){
					$args["function"] = "api.tickettransactions.reservations.list";
					$args["input"] = array(
						"airlines_id" => $this->args->airlines_id,
						"contact_name" => $this->args->contact_name,
						"pnr" => $this->args->pnr,
						"transdate_start" => $this->args->depdate,
						"transdate_end" => $this->args->arrdate,
						"bookby" => $this->args->bookby,
						"status" => (!empty($this->args->status)?$this->args->status:1),
						"request_access_from" => "api",
						"user_id" => $this->info_->user->id,
						"user_name" => $this->info_->user->name,
						"rows" => 15,
						"page" => $this->args->page
					);
				}
				else{
					$args["function"] = "api.tickettransactions.reservations.list";
					$args["input"] = array(
						"airline_code" => $this->args->airline_code,
						"contact_name" => $this->args->contact_name,
						"pnr" => $this->args->pnr,
						"depdate" => $this->args->depdate,
						"arrdate" => $this->args->arrdate,
						"bookby" => $this->args->bookby,
						"status" => (!empty($this->args->status)?$this->args->status:1),
						"foreign" => "foreign",
						"request_access_from" => "api",
						"user_id" => $this->info_->user->id,
						"user_name" => $this->info_->user->name,
						"rows" => 15,
						"page" => $this->args->page
					);
				}
            }
            
            else {
				if(!empty($this->args->domestik)){
					$args["function"] = "api.tickettransactions.reservations.list.current";
					$args["input"] = array(
						"status" => 1,
						"booklist" => "booklist",
						"request_access_from" => "api",
						"user_id" => $this->info_->user->id,
						"user_name" => $this->info_->user->name,
						"rows" => 15,
						"page" => $this->args->page
					);
				}
                else{
					$args["function"] = "api.tickettransactions.reservations.list.current";
					$args["input"] = array(
						"status" => 1,
						"booklist" => "booklist",
						"foreign" => "foreign",
						"request_access_from" => "api",
						"user_id" => $this->info_->user->id,
						"user_name" => $this->info_->user->name,
						"rows" => 15,
						"page" => $this->args->page
					);
				}
            }                        
            
            $args = array(
                "r"=>json_encode($args),
                "_search"=>false,
                "rows"=>20,
                "page"=> (!empty($this->args->page)?$this->args->page:1),
                "sidx"=>"",
                "sordx"=>""
            );
    
            $exec = $this->filterResponse($this->execute($args));
            $response = json_encode($this->handler->processXml($this->endpoint, json_decode($exec)));
            
            $this->logger->log("\r\n\r\nPARAMS: {$this->args->service}\r\n\r\n" . print_r($args, true));            
            $this->logger->log("\r\n\r\nRESPONSE:\r\n\r\n". print_r($response, true));
            
            return $response;
            
        } catch (Exception $e) {
            throw $e;
        }
    }

    protected function cancel_book() {
        try {
            $args["function"] = "api.tickettransactions.reservations.cancel";
            $args["input"] = array(
                "id" => $this->args->reservation_id,
                "cancel_user_id" => $this->info_->user->id,
                "merchant_id" => $this->info_->user->merchant_id
            );
			
			if(isset($this->args->foreign) && $this->args->foreign == "true"){
				$args_ext = array("foreign" => $this->args->foreign);
				$args["input"] = array_merge($args["input"], $args_ext);
			}
            
            API_Helper::validateForms($this->args, $this->config->PARAM_INTERNATIONAL_CANCEL);  
            
            $args = json_encode($args); //echo $this->execute(array("r"=>$args)); exit;///
            
            $str = $this->filterResponse($this->execute(array("r"=>$args)));
            $response = json_encode($this->handler->processXml($this->endpoint, json_decode($str)));

            $this->logger->log("\r\n\r\nPARAMS: {$this->args->service}\r\n\r\n" . print_r($args, true));
            $this->logger->log("\r\n\r\nRESPONSE: \r\n\r\n" . $response);

            return $response;
        } catch (Exception $e) {
            throw $e;
        }
    }

    /**
        * 
        * Hotel
        */

    protected function hotel_search() {
        try {            
            if (is_array($this->args->room_detail)) {
                $rd = $this->args->room_detail;
            }
            
            else {
                $rd = json_decode($this->args->room_detail);
            }
            
            $room_det = new stdClass();
            $room_det->cekjml_adult = array();
            $room_det->cektype_bed = array();
            $room_det->cekjml_child = array();
            $room_det->cekage_child = array();
            $room_det->cekage_child2 = array();
            
            foreach ($rd as $value) {
                array_push($room_det->cekjml_adult, $value->adult);                
                array_push($room_det->cekjml_child, $value->child);                
                array_push($room_det->cektype_bed, $value->bed_type);                
                
                if ($value->child == 0) {
                    $value->age = array(0,0);
                } else if ($value->child == 1) {
                    $value->age[1] = 0;
                }
                
                foreach ($value->age as $k => $v) {                    
                    if ($k===0) {
                        array_push($room_det->cekage_child, $v);
                    }
                    
                    else if ($k===1) {
                        array_push($room_det->cekage_child2, $v);
                    }
                }                
                
            }
            
            $room_detail = json_encode($room_det);
            
            $params = array(                
                "codehotelcity"=>$this->args->hotel_id,
                "destcity"=>$this->args->dest_city,
                "destcountry"=>$this->args->passport_nationality,
                "flhotel"=>$this->args->hotel_name,
                "flnight"=>$this->args->night,
                "flpage"=>$this->args->page,
                "flroom"=>$this->args->room,
                "loop_room"=>$room_detail,
                "merchant_code"=>$this->info_->user->merchant_code,
                "name_user"=>$this->args->username,
                "paging"=>$this->args->paging,
                "paxpassport"=>$this->args->passport_nationality,
                "rand"=> ($this->args->page == 1? 'undefined':$this->args->rand),
                "tglin"=>$this->args->checkin,
                "tglout"=>$this->args->checkout,
                "adultchild"=>$this->args->adult,
                "child"=>$this->args->child
            );                        
    //            print_r(json_encode($params)); exit;
            API_Helper::validateForms($this->args, $this->config->PARAM_HOTEL_SEARCH);   
    //            print_r($params); exit;
    //            echo $this->execute($params); exit;
            $response = json_encode($this->handler->processXml($this->endpoint, json_decode($this->execute($params))));            

            $this->logger->log("\r\n\r\nPARAMS: {$this->args->service}\r\n\r\n" . print_r($params, true));
            $this->logger->log("\r\n\r\nRESPONSE: \r\n\r\n" . $response);
            
            return $response;
            
        } catch (Exception $e) {
            throw $e;
        }
    }

    protected function hotel_detail() {
        try {            
            $params = array(
                "id_hotel"=>$this->args->hotel_id,
                "nama_hotel_mg"=>$this->args->nama_hotel_mg,
                "internalcode"=>$this->args->internalcode,
                "DestCity"=>$this->args->destcity,
                "merchant_code"=>$this->info_->user->merchant_code,
                "user_name"=>$this->args->username
            );
            
            API_Helper::validateForms($this->args, $this->config->PARAM_HOTEL_DETAILS);            
            
            $response = json_encode($this->handler->processXml($this->endpoint, json_decode($this->execute($params))));            
            $this->logger->log("\r\n\r\nPARAMS: {$this->args->service}\r\n\r\n" . print_r($params, true));
            $this->logger->log("\r\n\r\nRESPONSE: \r\n\r\n" . $response);
            
            return $response;
            
        } catch (Exception $e) {
            throw $e;
        }
    }

    protected function hotel_detail_rooms() {
        try {
            $params = array(
                "id_hotel"=>$this->args->hotel_id,                
                "hotel_id_db"=>$this->args->hotel_id_db,
                "internal_code"=>$this->args->internal_code,
                "checkIn"=>$this->args->checkin,
                "checkOut"=>$this->args->checkout,
                "adult"=>$this->args->adult,
                "child"=>$this->args->child,
                "room"=>$this->args->room,
                "user_name"=>$this->args->username,
                "paxpassport"=>$this->args->passport_nationality,
                "DestCountry"=>$this->args->dest_country,
                "DestCity"=>$this->args->dest_city,
                "merchant_code"=>$this->info_->user->merchant_code
            );
            
            if ($this->args->username === "gunawan") {
                $rd = json_decode($this->args->room_detail);
            }
            
            else {
                $rd = $this->args->room_detail;
            }
            
            foreach ($rd as $key=>$value) {
                $delimiter = (0 === $key ? "":",");
                $params["cekjml_adult"] .= trim($delimiter . $value->adult);
                $params["cekjml_child"] .= trim($delimiter . $value->child);
                $params["cektype_bed"] .= trim($delimiter . $value->bed_type);
                
                $age_size = count($value->age);
                
                if ($age_size>0) {
                    foreach ($value->age as $k=>$v) { // [2,5]
                        $delimiter2 = (empty($params["cekage_child"]) ? "":",");
                        if ($k === 0) {
                            $params["cekage_child"] .= trim($delimiter2 . $v);

                            if ($age_size === 1) {
                                $params["cekage_child2"] .= trim($delimiter2 . "0");
                            }
                        }

                        else {
                            $delimiter2 = (empty($params["cekage_child2"]) ? "":",");
                            $params["cekage_child2"] .= trim($delimiter2 . $v);//
                        }
                    }
                } 
                
                else {
                    $params["cekage_child"] = 0;
                    $params["cekage_child2"] = 0;
                }
            }
            
            API_Helper::validateForms($this->args, $this->config->PARAM_HOTEL_DETAIL_ROOMS);
			
			$params_device = array("device_model" => $this->args->device_model);
            
            $response = json_encode($this->handler->processXml($this->endpoint, json_decode($this->execute($params)), $params_device));            
            $this->logger->log("\r\n\r\nPARAMS: {$this->args->service}\r\n\r\n" . print_r($params, true));
            $this->logger->log("\r\n\r\nRESPONSE: \r\n\r\n" . $response);
            
            return $response;
            
        } catch (Exception $e) {
            throw $e;
        }
    }

    protected function hotel_cancel_policy() {
        $rd = json_decode($this->args->room_detail);
        $room_det = new stdClass();
        $room_det->cekjml_adult = array();
        $room_det->cektype_bed = array();
        $room_det->cekjml_child = array();       
        $room_det->cekage_child = array();       
        $room_det->cekage_child2 = array();

        $params = array(
                "id"=>$this->args->id,
                "checkin"=>$this->args->checkin,
                "checkout"=>$this->args->checkout,
                "internal_code"=>$this->args->internal_code,
                "avail"=> $this->args->avail,
                "adult"=>$this->args->adult,
                "child"=>$this->args->child,                                
                "room"=>$this->args->room,                
                "user_name"=>$this->args->username,
                "merchant_code"=>$this->info_->user->merchant_code,                
                "paxpassport"=>$this->args->passport_nationality,
                "cancel_policy_id"=>$this->args->cancel_policy_id                
            );
    //            print_r($rd); exit;
        foreach ($rd as $key=>$value) {
            $delimiter = (0 === $key ? "":",");
            $params["cekjml_adult"] .= $delimiter . $value->adult;
            $params["cekjml_child"] .= $delimiter . $value->child;
            
            if ($key === 0) {
                foreach ($value->age as $k=>$v) {
                    $delimiter2 = (0 === $k ? "":",");
                    $params["cekage_child"] .= $delimiter2 . $v;
                }
                
                $age_size = count($value->age);
            }
            
            else if ($key === 1) {
                if (count($value->age)>0) {
                    foreach ($value->age as $k=>$v) {
                        $delimiter2 = (0 === $k ? "":",");
                        $params["cekage_child2"] .= $delimiter2 . $v;
                    }
                } 
                
                else {
                    $i=0;
                    while ($i<$age_size) {
                        $delimiter2 = (0 === $i ? "":",");
                        $params["cekage_child2"] .= $delimiter2 . "0";//
                        $i++;
                    }
                }
            }
                        
            $params["cektype_bed"] .= $delimiter . $value->bed_type;
        }
    //        print_r($params); exit;
    //        echo ($this->execute($params)); exit;
        API_Helper::validateForms($this->args, $this->config->PARAM_HOTEL_CANCEL_POLICY);            
        $response = json_encode($this->handler->processXml($this->endpoint, json_decode($this->execute($params))));            

        $this->logger->log("\r\n\r\nPARAMS: {$this->args->service}\r\n\r\n" . print_r($params, true));
        $this->logger->log("\r\n\r\nRESPONSE: \r\n\r\n" . $response);

        return $response;
    }

    protected function hotel_book() {
        try {           
            $rd = (is_string($this->args->room_detail)?json_decode($this->args->room_detail):$this->args->room_detail);//
            
            $room_det = new stdClass();
            $room_det->cekjml_adult = array();
            $room_det->cektype_bed = array();
            $room_det->cekjml_child = array();
			
			$session_book = date('YmdHis').md5(date('YmdHis').mt_rand());
            
            $args["function"] = "api.hoteltransactions.reservations.book";
			if(isset($this->args->book_hotel) && $this->args->book_hotel == 'book_hotel'){
				$args["input"] = array(                
						"note_request"=>$this->args->note_request,
						"merchant_id"=>$this->info_->user->merchant_id,
						"booking_user_id"=>$this->info_->user->id,
						"contact_name"=>$this->args->contact_name,
						"contact_telp"=>$this->args->contact_telp,
						"hotel_id"=>$this->args->hotel_id,
						"hotel_id_db"=>$this->args->hotel_id_db,
						"nm_hotel"=>$this->args->hotel_name,
						"InternalCode"=>$this->args->internal_code,
						"DestCountry"=>$this->args->dest_country,
						"DestCity"=>$this->args->dest_city,
						"merchant_code"=>$this->info_->user->merchant_code,
						"PaxPassport_ResNo"=>$this->args->passport_nationality,
						"id_room"=>$this->args->room_id,
						"name_roomtype"=>$this->args->room_type,
						"nameroom"=>$this->args->room_name,
						"jmlkmar"=>$this->args->room_qty,
						"inc_abf"=>$this->args->inc_abf,
						"pricetotal"=>$this->args->price_total,
						"price_publish"=>$this->args->price_publish,                    
						"price_travflex"=>$this->args->price_travflex,                    
						"checkin"=>$this->args->checkin,
						"checkout"=>$this->args->checkout,                    
						"email"=>$this->args->email,
						"book_hotel"=> $this->args->book_hotel,
						"topup_id"=> $this->args->topup_id,
						"username"=> $this->args->username,
						"session_book"=> $session_book,
						"debet_saldo"=> 'NO',
						"source_from"=>"api", 
						"user_id"=>$this->info_->user->id
					);
			}
			else{
				$args["input"] = array(                
						"note_request"=>$this->args->note_request,
						"merchant_id"=>$this->info_->user->merchant_id,
						"booking_user_id"=>$this->info_->user->id,
						"contact_name"=>$this->args->contact_name,
						"contact_telp"=>$this->args->contact_telp,
						"hotel_id"=>$this->args->hotel_id,
						"hotel_id_db"=>$this->args->hotel_id_db,
						"nm_hotel"=>$this->args->hotel_name,
						"InternalCode"=>$this->args->internal_code,
						"DestCountry"=>$this->args->dest_country,
						"DestCity"=>$this->args->dest_city,
						"merchant_code"=>$this->info_->user->merchant_code,
						"PaxPassport_ResNo"=>$this->args->passport_nationality,
						"id_room"=>$this->args->room_id,
						"name_roomtype"=>$this->args->room_type,
						"nameroom"=>$this->args->room_name,
						"jmlkmar"=>$this->args->room_qty,
						"inc_abf"=>$this->args->inc_abf,
						"pricetotal"=>$this->args->price_total,
						"price_publish"=>$this->args->price_publish,                    
						"price_travflex"=>$this->args->price_travflex,                    
						"checkin"=>$this->args->checkin,
						"checkout"=>$this->args->checkout,                    
						"email"=>$this->args->email,                                                                                                                                                
						"username"=>$this->args->username,
						"session_book"=> $session_book,
						"debet_saldo"=>"KO",
						"source_from"=>"api", 
						"user_id"=>$this->info_->user->id
					);
			}
            
            $price_room = new stdClass();
            $price_room->rowset = array();
            $rowset_obj_ = new stdClass();
            $rowset_obj_->price = $this->args->price_publish;
            array_push($price_room->rowset, $rowset_obj_);
            
            $args["input"]["price_room"] = $price_room;
            
            $room_type = new stdClass();
            $room_type->rowset = array();
            $rowset_obj = new stdClass();
            $rowset_obj->adult_num = new stdClass();
            $rowset_obj->adult_num->rowset = array();
            $rowset_obj->child_num = new stdClass();
            $rowset_obj->child_num->rowset = array();
            $rowset_obj->ChildAge1 = new stdClass();
            $rowset_obj->ChildAge1->rowset = array();
            $rowset_obj->ChildAge2 = new stdClass();
            $rowset_obj->ChildAge2->rowset = array();
            
            foreach ($rd as $key=>$value) {
                $delimiter = (0 === $key ? "":",");
                $args["input"]["cekjml_adult"] .= $delimiter . $value->adult;
                $args["input"]["cekjml_child"] .= $delimiter . $value->child;
                $args["input"]["cektype_bed"] .= $delimiter . $value->bed_type;
                
                array_push($rowset_obj->adult_num->rowset, $value->adult);
                
                if ($value->child == 0) {
                    $value->age = array(0,0);
                }
                
                else if ($value->child == 1) {
                    $value->age[1] = 0;
                }
                
                array_push($rowset_obj->child_num->rowset, $value->child);

                foreach ($value->age as $k => $v) {
                    if ($k === 0) {
                        array_push($rowset_obj->ChildAge1->rowset, $v);
                    }

                    else if ($k === 1) {
                        array_push($rowset_obj->ChildAge2->rowset, $v);
                    }
                }                                                
                                
                if ($value->age[1] == 0) { unset($value->age[1]); }
                $age_size = count($value->age);
                
                foreach ($value->age as $k=>$v) {
                    $delimiter2 = (0 === $k ? "":",");
                    if ($k === 0) {
                        $args["input"]["cekage_child"] .= $delimiter2 . $v;

                        if ($age_size === 1) {
                            $args["input"]["cekage_child2"] .= $delimiter2 . "0";
                        }
                    }

                    else {
                        $delimiter3 = (empty($args["input"]["cekage_child2"])? "":",");
                        $args["input"]["cekage_child2"] .= $delimiter3 . $v;
                    }
                }                                                

    //                if ($key === 1) {;                          
            }
                                    
            array_push($room_type->rowset, $rowset_obj);
            $args["input"]["room_type"] = $room_type;
            
            $this->args->adultchilds = (is_string($this->args->adultchilds)?json_decode($this->args->adultchilds):$this->args->adultchilds);
            $this->args->childs = (is_string($this->args->childs)?json_decode($this->args->childs):$this->args->childs);
            
            $passengers = new stdClass();
            $passengers->adultchilds = new stdClass();
            $passengers->adultchilds->data = array();
            $passengers->adultchilds->_count = count($this->args->adultchilds);
            
            $passengers->childs = new stdClass();
            $passengers->childs->data = array();
            $passengers->childs->_count = 0;
            
            foreach ($this->args->adultchilds as $value) {
                array_push($passengers->adultchilds->data, $value);
            }
            
            if (count($this->args->childs) > 0) {
                foreach ($this->args->childs as $value) {
                    array_push($passengers->childs->data, $value);
                }
                
                $passengers->childs->_count = count($this->args->childs);
            }
            
            $args["input"]["passenger"] = $passengers;
            $args = array("r"=>json_encode($args));//
    //            print_r($args); exit;
    //            echo $this->execute($args); exit; 
            
            API_Helper::validateForms($this->args, $this->config->PARAM_HOTEL_BOOK); //print_r($params); exit;
			
			$params_device = array("device_model" => $this->args->device_model);
            
            $exec = $this->filterResponse($this->execute($args));
            $response = json_encode($this->handler->processXml($this->endpoint, json_decode($exec), $params_device));            

            $this->logger->log("\r\n\r\nPARAMS: {$this->args->service}\r\n\r\n" . print_r($args, true));
            $this->logger->log("\r\n\r\nRESPONSE: \r\n\r\n" . $response);
            
            return $response;
            
        } catch (Exception $e) {
            throw $e;
        }
    }        

    protected function forget_password() {
		API_Helper::validateForms($this->args, $this->config->PARAM_FORGET_PASSWORD);
		
		$params = array(              
			"username"=>$this->args->user_name,
			"email"=>$this->args->email
		);
		
		$response = json_encode($this->handler->processXml($this->endpoint, json_decode($this->execute($params)), $options));
            
		$this->logger->log("\r\n\r\nPARAMS: {$this->args->service}\r\n\r\n" . print_r($params, true));
		$this->logger->log("\r\n\r\nRESPONSE: \r\n\r\n" . $response);
		
		return $response;
		
	}
	
	protected function confirm_otp_verify() {
		API_Helper::validateForms($this->args, $this->config->PARAM_CONFIRM_OTP_VERIFY);
		
		$password = md5($this->args->new_password);
		$params = array(              
			"qc"=>$this->args->qparam,
			"password"=>$password,
			"code_otp"=>$this->args->code_otp
		);
		
		$response = json_encode($this->handler->processXml($this->endpoint, json_decode($this->execute($params)), $options));
            
		$this->logger->log("\r\n\r\nPARAMS: {$this->args->service}\r\n\r\n" . print_r($params, true));
		$this->logger->log("\r\n\r\nRESPONSE: \r\n\r\n" . $response);
		
		return $response;
		
	}
	
    protected function regis_billing() {
		try {
			API_Helper::validateForms($this->args, $this->config->PARAM_REGIS_BILLING);
			file_put_contents("/var/www/html/sro/static/taqin/cek_param_Regis_Billing.txt","CEK IP : ".$_SERVER['REMOTE_ADDR']."\r\n",FILE_APPEND);
			// if($_SERVER['REMOTE_ADDR'] == '103.78.81.122' || $_SERVER['REMOTE_ADDR'] == '114.124.135.133' || $_SERVER['REMOTE_ADDR'] == '77.111.246.156' || $_SERVER['REMOTE_ADDR'] == '104.238.148.229'){
				$args["function"] = "api.registration.billing";        
				$args["input"] = array(
								"fullname" => $this->args->fullname,
								"ktp" => $this->args->ktp,
								"company" => $this->args->company,
								"email" => $this->args->email,
								"alamat" => $this->args->alamat,
								"provinsi" => $this->args->provinsi,
								"kota" => $this->args->kota,
								"kode_pos" => $this->args->kode_pos,
								"negara" => $this->args->negara,
								"tlp" => $this->args->tlp,
								"affiliasi_id" => $this->args->affiliasi_id,
								"param_hash" => $this->args->param_hash,
								"url_ktp" => $this->args->url_ktp,
								"url_ktp_self" => $this->args->url_ktp_self,
								"url_transfer" => $this->args->url_transfer,
								"ip_client" => $this->args->ip_client,
								"source" => $this->args->source
							);
				
				$args = json_encode($args);
				$save = json_decode($this->filterResponse($this->execute(array("r"=>$args))));
				$response = $this->handler->processXml($this->endpoint, $save);
			/*}
			else{
				$result_code = 10007;
				$result_status = "Unauthorized (401)";
				$result_message = 'Authentication Failed';
				$result_detail = 'Mohon maaf untuk registrasi sementara hanya bisa lewat email ke mitra@asiawisata.com';
				
				$response = array(
					"result_status"=>$result_status, 
					"result_code"=>$result_code, 
					"result_message"=>$result_message, 
					"result_detail"=>$result_detail
				);
			}*/
			
			return  json_encode($response);
			
		} catch (Exception $e) {
			throw $e;
		}
	}

	protected function regis_billing_read() {
		try {
			API_Helper::validateForms($this->args, $this->config->PARAM_REGIS_BILLING_READ);
			
			$args["function"] = "api.registration.billing.read";
			if(!empty($this->args->regis_id) && !empty($this->args->param_hash)){
				$args["input"] = array(
							"regis_id" => $this->args->regis_id,
							"param_hash" => $this->args->param_hash
						);
			}
			else if(!empty($this->args->regis_id)){
				$args["input"] = array(
							"regis_id" => $this->args->regis_id
						);
			}
			else if(!empty($this->args->param_hash)){
				$args["input"] = array(
							"param_hash" => $this->args->param_hash
						);
			}
			
			$args = json_encode($args);
			
			$save = json_decode($this->filterResponse($this->execute(array("r"=>$args))));
			$response = $this->handler->processXml($this->endpoint, $save);
			
			return  json_encode($response);
			
		} catch (Exception $e) {
			throw $e;
		}
	}
	
	protected function regis_billing_aktivasi() {
		try {
			API_Helper::validateForms($this->args, $this->config->PARAM_REGIS_BILLING_AKTIVASI);
			
			$args["function"] = "api.registration.billing.edit";        
			$args["input"] = array(
							"param_hash" => $this->args->param_hash
						);
			
			$args = json_encode($args);
			$save = json_decode($this->filterResponse($this->execute(array("r"=>$args))));
			$response = $this->handler->processXml($this->endpoint, $save);
			
			return  json_encode($response);
			
		} catch (Exception $e) {
			throw $e;
		}
	}
	
    protected function create_users() {
		try {
			API_Helper::validateForms($this->args, $this->config->PARAM_CREATE_USERS);
			$this->args->search_type = "marketing_channel";
			$this->args->keyword = "AW";
			$this->args->service = "check-suggestion";
			$this->endpoint = "check_suggestion";
			
			$this->target_server = $this->baseUrl . "/" . $this->{$this->config->ENDPOINT_SERVER_MAPPING[$this->endpoint]};
			$response = $this->check_suggestion();
			$ceksuges = json_decode($response);
			
			if($ceksuges->result_status == "OK" && $ceksuges->result_message == "Success"){
				$this->endpoint = "create_users";
				$this->target_server = $this->baseUrl . "/" . $this->ibe_ws;
				$args["function"] = "api.usermanager.users.create";        
				$args["input"] = array(
								"user_fullname" => $this->args->user_fullname,
								"user_address" => $this->args->user_address,
								"user_phone" => $this->args->user_phone,
								"user_email" => $this->args->user_email,
								"username" => $ceksuges->suggestion[0]->code,
								"reservation_type" => "0",
								"created_by" => 'admin_billing',
								"role_name" => '1011',
								"merchanttype_id" => '1006'
							);
				
				$args = json_encode($args);
				$save = json_decode($this->filterResponse($this->execute(array("r"=>$args))));
				$response_new_users = $this->handler->processXml($this->endpoint, $save);
				
				$this->endpoint = "roleassignments";
				$args2["function"] = "api.usermanager.roleassignments.create";        
				$args2["input"] = array(
								"user_id" =>$response_new_users->new_user->id,
								"role_id" => '1011'
							);
				
				$args2 = json_encode($args2);
				
				$save2 = json_decode($this->filterResponse($this->execute(array("r"=>$args2))));
				$response_role = $this->handler->processXml($this->endpoint, $save2);
				
				$this->endpoint = "create_merchant";
				$args3["function"] = "api.merchantmanager.merchants.create";        
				$args3["input"] = array(
								"merchant_code" => $ceksuges->suggestion[0]->code,
								"merchant_fullname" => $this->args->user_fullname,
								"merchant_merchanttype_id" => '1006',
								"merchant_address" => $this->args->user_address,
								"merchant_email" => $this->args->user_email,
								"merchant_phonenumber1" => $this->args->user_phone,
								"merchant_phonenumber2" => "",
								"merchant_phonenumber3" => "",
								"merchant_phonenumber4" => "",
								"merchant_parent_id" => '1000'
							);
				
				$args3 = json_encode($args3);
				
				$save3 = json_decode($this->filterResponse($this->execute(array("r"=>$args3))));
				$response_merchant = $this->handler->processXml($this->endpoint, $save3);
				
				$this->endpoint = "create_merchant_members";
				$args4["function"] = "api.merchantmanager.merchantmembers.create";        
				$args4["input"] = array(
								"merchant_id" => $response_merchant->new_merchant->merchant_id,
								"user_id" => $response_new_users->new_user->id
							);
				
				$args4 = json_encode($args4);
				
				$save4 = json_decode($this->filterResponse($this->execute(array("r"=>$args4))));
				$response_merchant_members = $this->handler->processXml($this->endpoint, $save4);
				
				return json_encode($response_merchant_members);
			}
			else{
				return $response;
			}
		} catch (Exception $e) {
			throw $e;
		}
	}
	
    protected function check_suggestion() {
        try {
			if($this->args->search_type == 'marketing_channel'){
				$this->config->PARAM_HOTEL_CHECK_SUGGESTION['username']['required'] = false;
				$this->config->PARAM_HOTEL_CHECK_SUGGESTION['apikey']['required'] = false;
			}
            API_Helper::validateForms($this->args, $this->config->PARAM_HOTEL_CHECK_SUGGESTION);
            $params = array(              
                "type"=>$this->args->search_type,
                "q"=>$this->args->keyword                
            );
			
            if ($this->args->search_type === "travel") {
                $params["dest"] = $this->args->dest;
                $params["typechoose"] = $this->args->typechoose;
                
                if ($this->args->typechoose === "arrive") {
                    $params["depart"] = $this->args->depart;
                    $params["desttext"] = $this->args->desttext;
                }
            }
			else if($this->args->search_type === "marketing_channel"){
				$params["typeid"] =  1006;
			}
                                    
            if (!isset($this->args->version_code) || (int)$this->args->version_code<11) {
//                if ($this->args->search_type === "hotel") {
//                    $params["mg"] = "mg";
//                }                
            }
            
            if ($this->args->search_type === "hotel") {
                $params["mg"] = "mg";
            }
                        
            $options = array(
                "endpoint" => $this->endpoint
            );
                        
            $response = json_encode($this->handler->processXml($this->endpoint, json_decode($this->execute($params)), $options));
            
            $this->logger->log("\r\n\r\nPARAMS: {$this->args->service}\r\n\r\n" . print_r($params, true));
            $this->logger->log("\r\n\r\nRESPONSE: \r\n\r\n" . $response);
            
            return $response;
            
        } catch (Exception $e) {
            throw $e;
        }
    }        

    protected function hotel_gallery_images() {
        try {
            $args["function"] = "api.manage.hotel.facility";    
            $args["input"] = array(
                "hotel_idtoimages" => $this->args->image_id
            );
            $args = json_encode($args);
            
            API_Helper::validateForms($this->args, $this->config->PARAM_HOTEL_GALLERY_IMAGES);            

            $response = $this->filterResponse($this->execute(array("r"=>$args)));
            return json_encode($this->handler->processXml($this->endpoint, json_decode($response)));
            
        } catch (Exception $e) {
            throw $e;
        }
    }

    protected function hotel_read_pnr() {
        try {                        
            API_Helper::validateForms($this->args, $this->config->PARAM_HOTEL_READPNR);
            
            $args["function"] = "api.hoteltransactions.reservations.read";    
            $args["input"] = array(
                "id" => $this->args->reservation_id,
                "merchant_id" => $this->info_->user->merchant_id
            ); 
            $args = json_encode($args);
                        
            $exec = $this->filterResponse($this->execute(array("r"=>$args))); //echo $exec; exit;
            $response = json_encode($this->handler->processXml($this->endpoint, json_decode($exec)));
            
            $this->logger->log("\r\n\r\nPARAMS: {$this->args->service}\r\n\r\n" . print_r($args, true));
            $this->logger->log("\r\n\r\nRESPONSE: \r\n\r\n" . $response);
            
            return $response;

        } catch (Exception $e) {
            throw $e;
        }
    }

    protected function hotel_voucher_list() {
        try {                        
            API_Helper::validateForms($this->args, $this->config->PARAM_HOTEL_VOUCHERLIST);
            
            $args["function"] = "api.hoteltransactions.reservations.list";    
            $args["input"] = array(
                "request_access_from" => "api",
                "user_id" => $this->info_->user->id,
                // "status" => 3,
                "rows" => 15,
                "page" => $this->args->page
            );
            
            if (
                !empty($this->args->contact_name) ||
                !empty($this->args->pnr) ||
                !empty($this->args->bookdate_from) ||
                !empty($this->args->bookdate_to) ||
                !empty($this->args->bookby)) 
            {
                $args_ext = array(
                    "guest_name" => $this->args->contact_name,
                    "code" => $this->args->pnr,
                    "transdate_start" => $this->args->bookdate_from,
                    "transdate_end" => $this->args->bookdate_to,
                    "bookby" => $this->args->bookby
                );
                
                $args["input"] = array_merge($args["input"], $args_ext);
            }
            
            $args = array(
                "r"=>json_encode($args)
            );                        
    //            print_r($args); exit;
    //            print_r($this->execute($args)); exit;
            $exec = $this->filterResponse($this->execute($args));
            $response = json_encode($this->handler->processXml($this->endpoint, json_decode($exec)));
            
            $this->logger->log("\r\n\r\nPARAMS: {$this->args->service}\r\n\r\n" . print_r($args, true));
            $this->logger->log("\r\n\r\nRESPONSE: \r\n\r\n" . $response);
            
            return $response;

        } catch (Exception $e) {
            throw $e;
        }
    }
	
	protected function destinations_airlines() {
        try {                        
            API_Helper::validateForms($this->args, $this->config->PARAM_DESTINATIONS_AIRLINES);
            
            $args["function"] = "api.airlines.route.list";    
            $args["input"] = array(
                "request_access_from" => "api",
                "user_id" => $this->info_->user->id
            );
			
            $args = array(
                "r"=>json_encode($args)
            );
			
            $exec = $this->filterResponse($this->execute($args));
            $response = json_encode($this->handler->processXml($this->endpoint, json_decode($exec)));
            
            $this->logger->log("\r\n\r\nPARAMS: {$this->args->service}\r\n\r\n" . print_r($args, true));
            $this->logger->log("\r\n\r\nRESPONSE: \r\n\r\n" . $response);
            
            return $response;

        } catch (Exception $e) {
            throw $e;
        }
    }
	
	protected function apikey_device_user() {
        try {                        
            API_Helper::validateForms($this->args, $this->config->PARAM_APIKEY_DEVICE_USER);
            
            $args["function"] = "api.apikey.device.user";    
            $args["input"] = array(
                "apikey_device" => $this->args->apikey_device,
                "userid_mitra" => $this->args->userid_mitra,
                "user_id" => $this->info_->user->id
            );
			
            $args = array(
                "r"=>json_encode($args)
            );
			
            $exec = $this->filterResponse($this->execute($args));
            $response = json_encode($this->handler->processXml($this->endpoint, json_decode($exec)));
            
            $this->logger->log("\r\n\r\nPARAMS: {$this->args->service}\r\n\r\n" . print_r($args, true));
            $this->logger->log("\r\n\r\nRESPONSE: \r\n\r\n" . $response);
            
            return $response;

        } catch (Exception $e) {
            throw $e;
        }
    }

    /**
        * 
        * Kereta API
        */

    protected function train_search_v2() {
        try {
            API_Helper::validateForms($this->args, $this->config->PARAM_TRAIN_SEARCH);   
            
            $params = array(
                "username" => $this->args->username,
                "ruteBerangkat" => $this->args->origin,
                "ruteTujuan" => $this->args->destination,
                "tanggal_pergi" => $this->args->depdate,
                "dewasa" => $this->args->adult,
                "anak" => $this->args->child,
                "bayi" => $this->args->infant,
                "random" => rand(0, 9999999)
            );            

            $response = json_encode($this->handler->processXml($this->endpoint, json_decode($this->execute($params))));            

            $this->logger->log("PARAMS: {$this->args->service}\r\n\r\n" . print_r($params, true));
            $this->logger->log("\r\n\r\nRESPONSE: \r\n\r\n" . $response);
            
            return $response;
        } catch (Exception $e) {
            throw $e;
        }
    }
	
	protected function train_search() {
        try {
            API_Helper::validateForms($this->args, $this->config->PARAM_TRAIN_SEARCH);   
            
            $params = array(
                "username" => $this->args->username,
                "ruteBerangkat" => $this->args->origin,
                "ruteTujuan" => $this->args->destination,
                "tanggal_pergi" => $this->args->depdate,
                "dewasa" => $this->args->adult,
                "anak" => $this->args->child,
                "bayi" => $this->args->infant,
                "random" => rand(0, 9999999)
            );            

            $response = json_encode($this->handler->processXml($this->endpoint, json_decode($this->execute($params))));            

            $this->logger->log("PARAMS: {$this->args->service}\r\n\r\n" . print_r($params, true));
            $this->logger->log("\r\n\r\nRESPONSE: \r\n\r\n" . $response);
            
            return $response;
        } catch (Exception $e) {
            throw $e;
        }
    }

    protected function train_book() {
        try {            
            API_Helper::validateForms($this->args, $this->config->PARAM_TRAIN_BOOK);   
                        
            $adlts = (is_string($this->args->adultchilds)?json_decode($this->args->adultchilds):$this->args->adultchilds);            
            $chlds = (is_string($this->args->childs)?json_decode($this->args->childs):$this->args->childs);            
            $infnts = (is_string($this->args->infants)?json_decode($this->args->infants):$this->args->infants);                                    
            
            $adultchilds = new stdClass();            
            $adultchilds->data = array();
            $adultchilds->_count = count($adlts);
            
            $childs = new stdClass();
            $childs->data = array();
            $childs->_count = 0;
            
            $infants = new stdClass();
            $infants->data = array();
            $infants->_count = 0;                        
            $manual_seat = "";            
            
            $wagon_class = array("EKO" => "EKONOMI", "BIS" => "BISNIS", "EKS" => "EXECUTIVE");
            
            foreach ($adlts as $value) {
                array_push($adultchilds->data, $value);
                
                if (!empty($value->seat)) {
                    $manual_seat_delimiter = (!empty($manual_seat)?",":"");
                    $manual_seat .= $manual_seat_delimiter . str_replace("_", "", $value->seat);
                    
                    $wagon = explode("#", $value->wagon);
                    $wagon_no = $wagon[0];                    
                    $wagon_code = $wagon[1]; //$wagon_class[$wagon[1]];
                }
            }
            
            if (count($chlds) > 0) {
                foreach ($chlds as $value) {
                    array_push($childs->data, $value);
                    
                    if (!empty($value->seat)) {
                        $manual_seat_delimiter = (!empty($manual_seat)?",":"");
                        $manual_seat .= $manual_seat_delimiter . str_replace("_", "", $value->seat);                        
                    }
                }
                
                $childs->_count = count($chlds);
            }
            
            if (count($infnts) > 0) {
                foreach ($infnts as $value) {
                    array_push($infants->data, $value);
                }
                
                $infants->_count = count($infnts);
            }
            
            $dtemp1 = substr($this->args->deptime, 0, 2);
            $dtemp2 = substr($this->args->deptime, 2, 2);
            $deptime_with_dot = "{$dtemp1}.{$dtemp2}";
            
            $atemp1 = substr($this->args->arrtime, 0, 2);
            $atemp2 = substr($this->args->arrtime, 2, 2);
            $arrtime_with_dot = "{$atemp1}.{$atemp2}";
            
            $depdate_without_hypen = str_replace("-", "", $this->args->depdate);
            $arrdate_without_hypen = str_replace("-", "", $this->args->arrdate);
            
            $total_pax = count($adlts) + count($chlds);
            
            $manual_seat_object = new stdClass();
            $manual_seat_object->seat = $manual_seat;                        
            $manual_seat_object->wagon_no = $wagon_no;                        
            $manual_seat_object->wagon_code = $wagon_code;                        
            
            $psgr = new stdClass();
            $psgr->adultchilds = $adultchilds;
            $psgr->childs = $childs;
            $psgr->infants = $infants;                        
            
            $input = array(
                "user_id"=> $this->info_->user->id,
                "loggerRequestID"=> null,
                "loggerSequenceNumber"=> null,
                "checktype_kereta"=> "kai",
                "booking_rute_berangkat"=> $this->args->origin,
                "booking_rute_tujuan"=> $this->args->destination,
                "booking_request_tglpergi"=> $this->args->depdate,
                "booking_request_dewasa"=> $this->args->adult,
                "booking_request_anak"=> $this->args->child,
                "booking_request_bayi"=> $this->args->infant,
                "data_search"=> array(
                        "checktype_kereta"=> "kai",
                        "id_kereta"=> $this->args->train_id,
                        "nama_kereta"=> $this->args->train_name,
                        "jam_pergi_kereta"=> $deptime_with_dot,
                        "jam_pergi"=> $this->args->deptime,
                        "jam_sampai_kereta"=> $this->args->arrtime,
                        "jam_sampai"=> $arrtime_with_dot,
                        "subclass"=> $this->args->subclass,
                        "subclass_kursi"=> null,
                        "kelas"=> $this->args->class,
                        "harga_dewasa"=> $this->args->adult_price,
                        "harga_anak"=> $this->args->child_price,
                        "harga_bayi"=> 0,
                        "tgl_pergi_kereta"=> $this->args->depdate,
                        "tgl_pergi"=> $depdate_without_hypen,
                        "tgl_sampai_kereta"=> $this->args->arrdate,
                        "tgl_sampai"=> $arrdate_without_hypen,
                        "route_departure"=> $this->args->origin_name,
                        "route_arrival"=> $this->args->destination_name,
                        "org"=> $this->args->origin,
                        "des"=> $this->args->destination
                ),
                "airlines_id"=> "0",                
                "passengers" => $psgr,         
                "nm_contact"=> $this->args->cp_name,
                "tlp_contact"=> $this->args->cp_phone,
    //                "email"=> $this->args->cp_email,
                "username"=> $this->info_->user->name,
                "merchant_id"=> $this->info_->user->merchant_id,
                "vendor"=> $this->args->vendor,
                // "vendor"=> "2",
                "manual_seat"=> $manual_seat_object,
                "company"=>"1"
            );
            
            $args["function"] = "api.tickettransactions.reservations.book.train";            
            $args["input"]["data_p"] = $input;
            $args["input"]["book_new"] = "book_new";            
            $args = array("r"=>json_encode($args));
    //            print_r($args); exit;//
            
            $exec = $this->execute($args);
            $response = json_encode($this->handler->processXml($this->endpoint, json_decode($exec)));

            $this->logger->log("\r\n\r\nPARAMS: {$this->args->service}\r\n\r\n" . print_r($args, true));
            $this->logger->log("\r\n\r\nRESPONSE: \r\n\r\n" . $response);
            
            return $response;
            
        } catch (Exception $e) {
            throw $e;
        }        
        
    }

    protected function train_seatmap() {
        try {
            API_Helper::validateForms($this->args, $this->config->PARAM_TRAIN_SEATMAP);   
            
            $args = array(
                "username" => $this->info_->user->name,
                "ruteBerangkat" => $this->args->origin,
                "ruteTujuan" => $this->args->destination,
                "tanggal_pergi" => $this->args->depdate,
                "train_no" => $this->args->train_no,
                "subclass" => $this->args->subclass,
                "random" => ""
            );
            
    //            $exec = $this->filterResponse($this->execute($args));
            $response = json_encode($this->handler->processXml($this->endpoint, json_decode($this->execute($args))));//

            $this->logger->log("\r\n\r\nPARAMS: {$this->args->service}\r\n\r\n" . print_r($args, true));
            $this->logger->log("\r\n\r\nRESPONSE: \r\n\r\n" . $response);
            
            return $response;
            
        } catch (Exception $e) {
            throw $e;
        }
    }
	
	protected function train_seatmap_v2() {
        try {
            API_Helper::validateForms($this->args, $this->config->PARAM_TRAIN_SEATMAP);   
            
            $args = array(
                "username" => $this->info_->user->name,
                "ruteBerangkat" => $this->args->origin,
                "ruteTujuan" => $this->args->destination,
                "tanggal_pergi" => $this->args->depdate,
                "train_no" => $this->args->train_no,
                "subclass" => $this->args->subclass,
                "pax_adult" => $this->args->pax_adult,
                "pax_child" => $this->args->pax_child,
                "pax_infant" => $this->args->pax_infant,
                "book_code" => $this->args->book_code,
                "booktime" => $this->args->booktime,
                "random" => ""
            );
            
            $response = json_encode($this->handler->processXml($this->endpoint, json_decode($this->execute($args))));//

            $this->logger->log("\r\n\r\nPARAMS: {$this->args->service}\r\n\r\n" . print_r($args, true));
            $this->logger->log("\r\n\r\nRESPONSE: \r\n\r\n" . $response);
            
            return $response;
            
        } catch (Exception $e) {
            throw $e;
        }
    }        

    protected function train_takeseat() {
        try {
            API_Helper::validateForms($this->args, $this->config->PARAM_TRAIN_TAKESEAT);   
            
            $args["function"] = "api.tickettransactions.reservations.takeseat.train";
            $args["input"] = array(
				"username" => $this->info_->user->name,
                "id" => $this->args->reservation_id,
                "book_code" => $this->args->book_code,
                "booking_date" => $this->args->booking_date,
                "take_seat" => $this->args->take_seat
            );
            
            $exec = $this->filterResponse($this->execute(array("r"=>json_encode($args)))); //print_r($exec); exit;
            $response = json_encode($this->handler->processXml($this->endpoint, json_decode($exec)));

            $this->logger->log("\r\n\r\nPARAMS: {$this->args->service}\r\n\r\n" . print_r($args, true));
            $this->logger->log("\r\n\r\nRESPONSE: \r\n\r\n" . $response);
            
            return $response;
        } catch (Exception $e) {
            throw $e;
        }
    }
	
	protected function train_read_pnr() {
        try {
            API_Helper::validateForms($this->args, $this->config->PARAM_TRAIN_READPNR);   
            
            $args["function"] = "api.tickettransactions.reservations.read.train";
            $args["input"] = array(
                "id" => $this->args->reservation_id
            );
            
            $exec = $this->filterResponse($this->execute(array("r"=>json_encode($args)))); //print_r($exec); exit;
            $response = json_encode($this->handler->processXml($this->endpoint, json_decode($exec)));

            $this->logger->log("\r\n\r\nPARAMS: {$this->args->service}\r\n\r\n" . print_r($args, true));
            $this->logger->log("\r\n\r\nRESPONSE: \r\n\r\n" . $response);
            
            return $response;
        } catch (Exception $e) {
            throw $e;
        }
    }

    protected function train_issue() {
        try {
            API_Helper::validateForms($this->args, $this->config->PARAM_TRAIN_ISSUE);   
            
            $session = array (
                "id"=> $this->info_->session->id,
                "merchant_id"=> $this->info_->user->merchant_id,
                "merchanttype_id"=> $this->info_->user->merchanttype_id,
                "role_id"=>$this->info_->user->role_id,
                "role_uri"=>$this->info_->user->role_uri,
                "role_fullname"=> $this->info_->user->role_fullname,
                "merchant_code"=>$this->info_->user->merchant_code,
                "UUID"=> $this->info_->session->uuid,
                "status_role"=>$this->info_->user->status_role
            );
            
            $args["function"] = "api.tickettransactions.reservations.issue.train";
            $args["input"] = array(
                "id" => $this->args->reservation_id,
                "issue_user_id" => $this->info_->user->id,
                "session" => $session
            );
			$this->logger->log("\r\n\r\nPARAMS: {$this->args->service}\r\n\r\n" . print_r($args, true));
            $exec = $this->filterResponse($this->execute(array("r"=>json_encode($args)))); //print_r($exec); exit;
            $response = json_encode($this->handler->processXml($this->endpoint, json_decode($exec)));

            $this->logger->log("\r\n\r\nRESPONSE: \r\n\r\n" . $response);
            
            return $response;
        } catch (Exception $e) {
            throw $e;
        }
    }

    protected function train_cancel_book() {
        try {
            API_Helper::validateForms($this->args, $this->config->PARAM_TRAIN_CANCELBOOK);
            
            $endpoint_ = $this->endpoint;
            $this->endpoint = "train_cancel_direct_kai";
            $this->target_server = $this->baseUrl . "/" . $this->train_cancel_direct_kai;
            
            $params = array(
                "book_code" => $this->args->pnr,
                "cancel_reason" => "user booking canceled",
                "username" => $this->info_->user->name
            );
            
            $respon_cx = json_decode($this->execute($params));
            
            if ($respon_cx->status === 'XX' || $respon_cx->err_code === '001003') {
                if (strpos($respon_cx->err_msg, "does not exist or already canceled") === false) {
                    $this->endpoint = $endpoint_;
                    $this->target_server = $this->baseUrl . "/" . $this->{$this->config->ENDPOINT_SERVER_MAPPING[$this->endpoint]};

                    $args["function"] = "api.tickettransactions.reservations.cancel.train";
                    $args["input"] = array(
                        "id" => $this->args->reservation_id,
                        "book_code" => $this->args->pnr,
                        "cancel_user_id" => $this->info_->user->id,
                        "username" => $this->info_->user->name               
                    );

                    $args = json_encode($args); //echo $this->execute(array("r"=>$args)); exit;

                    $str = $this->filterResponse($this->execute(array("r"=>$args)));
                    $response = json_encode($this->handler->processXml($this->endpoint, json_decode($str)));
                    
                    $this->logger->log("\r\n\r\nPARAMS: {$this->args->service}\r\n\r\n" . print_r($args, true));
                    $this->logger->log("\r\n\r\nRESPONSE SRO: \r\n\r\n" . $response, FILE_APPEND);
                    
                } else {
                    APIException::_throwException($respon_cx->err_msg, '', 10011);
                }                                
            } else {
                APIException::_throwException($respon_cx->err_msg, '', 10011);
            }
                        
            $this->logger->log("\r\n\r\nRESPONSE KAI: \r\n\r\n" . print_r($respon_cx, true), FILE_APPEND);            

            return $response;
        } catch (Exception $e) {
            throw $e;
        }
    }

    protected function train_reservation_list() {        
        try {
            API_Helper::validateForms($this->args, $this->config->PARAM_TRAIN_RSVLIST);
                        
            if (
                !empty($this->args->pnr) ||
                !empty($this->args->contact_name) ||                
                !empty($this->args->depdate) ||
                !empty($this->args->arrdate) ||
                !empty($this->args->bookby) ||
                !empty($this->args->status)
                )
            {
                $args["function"] = "api.tickettransactions.reservations.list.train";
                $args["input"] = array(
                    "pnr" => $this->args->pnr,
                    "contact_name" => $this->args->contact_name,                    
                    "transdate_start" => $this->args->depdate,
                    "transdate_end" => $this->args->arrdate,
                    "bookby" => $this->args->bookby,
                    "status" => $this->args->status,
                    "operator_company" => $this->args->provider,
                    "request_access_from" => "api",
                    "user_id" => $this->info_->user->id,
                    "user_name" => $this->info_->user->name,                    
                    "page" => "all",
                    "rows" => 15,
                    "page_train" => $this->args->page,
                    "sidx"=>"booking_datetime",
                    "sord"=>"desc"
                );
            }
            
            else {
                $args["function"] = "api.tickettransactions.reservations.list.current.train";
                $args["input"] = array(
                    "status" => "",                    
                    "request_access_from" => "api",
                    "user_id" => $this->info_->user->id,
                    "user_name" => $this->info_->user->name,                    
                    "page" => "all",
                    "rows" => 15,
                    "page_train" => $this->args->page,
                    "sidx"=>"booking_datetime",
                    "sord"=>"desc",
                    "request_access_from" => "api"
                );                
            }                        
    //            print_r(json_encode($args)); exit;
            $params = array(
                "r"=>json_encode($args)                                
            );
    //            print_r($params); exit;
    //            print_r($this->execute($params)); exit;
            $exec = $this->filterResponse($this->execute($params));
            
            $response = json_encode($this->handler->processXml($this->endpoint, json_decode($exec)));
            
            return $response;
            
        } catch (Exception $e) {
            throw $e;
        }
    }

    protected function deposit_topup_request() {
        try {
            API_Helper::validateForms($this->args, $this->config->PARAM_DEPOSIT_TOPUP);
            
            $role_list = array(1004, 1006, 1008, 1010, 1011);
            
            if (!in_array($this->info_->user->role_id, $role_list)) {
                APIException::_throwException(APIException::ERROR_DEPOSIT_TOPUP_NOTAUTHORIZED, $this->info_->user->name, 10020);
            }
            
            $args["function"] = "api.merchantmanager.topup.request";
            $args["input"] = array(
                "user_id" => $this->info_->user->id,
                "merchant_id" => $this->info_->user->merchant_id,
                "fullname" => $this->info_->user->fullname,
                "email" => $this->info_->user->email,
                "phone" => $this->info_->user->phone,
                "merchant_code" => $this->info_->user->merchant_code,
                "payment" => $this->args->payment,
                "value" => $this->args->value,
                "id_bank" => $this->args->bank_id,
                "tenor_bank" => $this->args->bank_tenor,
                "currency_label" => $this->args->currency_label,
                "payment_id" => $this->args->payment_id,
                "payment_label" => $this->args->payment_label,
                "hotel_name" => $this->args->hotel_name,
                "checkin" => $this->args->checkin,
				"checkout" => $this->args->checkout,
                "jml_room" => $this->args->room_qty
            );                        
            
            $args = json_encode($args); //echo $this->execute(array("r"=>$args)); exit;            
            
            $str = $this->filterResponse($this->execute(array("r"=>$args)));
            $response = json_encode($this->handler->processXml($this->endpoint, json_decode($str)));
			$cekres = json_decode($response);
			
            $this->logger->log("\r\n\r\nPARAMS: {$this->args->service}\r\n\r\n" . print_r($args, true));
            $this->logger->log("\r\n\r\nRESPONSE SRO: \r\n\r\n" . $response);
			
			if(isset($this->args->payment_id) && !empty($this->args->payment_id)){
				if($this->args->payment_label == "hotel"){
					$this->args->topup_id = $cekres->topup_request->topup_id;
					$this->args->book_hotel = "book_hotel";
					$this->args->service = "hotel-book";
					$this->endpoint = "hotel_book";

					$this->target_server = $this->baseUrl . "/" . $this->{$this->config->ENDPOINT_SERVER_MAPPING[$this->endpoint]};
					$cekresponse_pay = $this->hotel_book();
				}
			}
            
            return $response;
            
        } catch (Exception $e) {
            throw $e;
        }       
    }

    protected function deposit_topup_list() {
        try {
            API_Helper::validateForms($this->args, $this->config->PARAM_DEPOSIT_TOPUP);
            
            $role_list = array(1004, 1006, 1008, 1010, 1011);
            
            if (!in_array($this->info_->user->role_id, $role_list)) {
                APIException::_throwException(APIException::ERROR_DEPOSIT_TOPUP_NOTAUTHORIZED, $this->info_->user->name, 10020);
            }
            
            $args["function"] = "api.merchantmanager.topup.list";
            $args["input"] = array(
                "reference_code"=>$this->args->invoice_no,
                "transdate_start"=>$this->args->topupdate_from,
                "transdate_end"=>$this->args->topupdate_to,
                "status"=>$this->args->status,
                "payment_method"=> $this->args->payment_method,
                "merchant_id"=> $this->info_->user->merchant_id,
                "merchant_code"=>$this->info_->user->name,
                "merchanttype"=>$this->info_->user->merchanttype_id,
                "role_id"=>$this->info_->user->role_id,
                "rows"=>10,
                "page"=>$this->args->page
            );
            
            $args = json_encode($args);
            
            $str = $this->filterResponse($this->execute(array("r"=>$args)));
            $response = json_encode($this->handler->processXml($this->endpoint, json_decode($str)));
            
            $this->logger->log("\r\n\r\nPARAMS: {$this->args->service}\r\n\r\n" . print_r($args, true));
            $this->logger->log("\r\n\r\nRESPONSE SRO: \r\n\r\n" . $response);
            
            return $response;
            
        } catch (Exception $e) {
            throw $e;
        }
    }

    protected function deposit_topup_confirmation() {
        try {
            API_Helper::validateForms($this->args, $this->config->PARAM_DEPOSIT_TOPUP_CONFIRM);
            
            $role_list = array(1004, 1006, 1008, 1010, 1011);
            
            if (!in_array($this->info_->user->role_id, $role_list)) {
                APIException::_throwException(APIException::ERROR_DEPOSIT_TOPUP_NOTAUTHORIZED, $this->info_->user->name, 10020);
            }
            
            $args["function"] = "api.merchantmanager.topup.payment.confirmation.bank";
            $args["input"] = array(                
        "topup_id"=> $this->args->topup_id,
        "payment_id"=> $this->args->payment_id,
        "payment_label"=> $this->args->payment_label,
        "user_id"=>$this->info_->user->id,
        "merchant_id"=> $this->info_->user->merchant_id,
        "merchant_code"=>$this->info_->user->name
            );
            
            $args = json_encode($args); //echo $this->execute(array("r"=>$args)); exit;
            
            $exec = $this->filterResponse($this->execute(array("r"=>$args)));
            $response = json_encode($this->handler->processXml($this->endpoint, json_decode($exec)));
			$cekres = json_decode($response);
			
			if($cekres->confirmed){
				if(isset($this->args->payment_id) && !empty($this->args->payment_id)){
					if($this->args->payment_label == "ppob"){
						$this->args->service = "payment-issue";
						$this->endpoint = "payment_issue";
						
						$this->target_server = $this->baseUrl . "/" . $this->{$this->config->ENDPOINT_SERVER_MAPPING[$this->endpoint]};
						$cekresponse_pay = $this->payment_issue();
					}
					else if($this->args->payment_label == "domestik" || $this->args->payment_label == "international"){
						$this->endpoint = "reservations_issue";
						$args2["function"] = "api.tickettransactions.reservations.issue";
						$args2["input"] = array(                
							"id"=> $this->args->payment_id,
							"issue_user_id"=>$this->info_->user->id,
							"username"=>$this->info_->user->name
						);
						
						if($this->args->payment_label == "international"){
							$foreign = array("foreign"=>"foreign");
							$args2["input"] = array_merge($args2["input"],$foreign);
						}
							
						$args2 = json_encode($args2);
						
						$exec2 = $this->filterResponse($this->execute(array("r"=>$args2)));
						$cekresponse_pay = json_encode($this->handler->processXml($this->endpoint, json_decode($exec2)));
					}
					else if($this->args->payment_label == "train"){
						$this->args->service = "train-issue";
						$this->args->reservation_id = $this->args->payment_id;
						$this->endpoint = "train_issue";
						
						$this->target_server = $this->baseUrl . "/" . $this->{$this->config->ENDPOINT_SERVER_MAPPING[$this->endpoint]};
						$cekresponse_pay = $this->train_issue();
					}
					else if($this->args->payment_label == "hotel"){
						$this->args->service = "hotel-book";
						$this->endpoint = "hotel_book";
						
						$this->target_server = $this->baseUrl . "/" . $this->{$this->config->ENDPOINT_SERVER_MAPPING[$this->endpoint]};
						$cekresponse_pay = $this->hotel_book();
					}
					else if($this->args->payment_label == "shuttle_bus"){
						$this->args->service = "shuttle-bus-issue";
						$this->endpoint = "shuttle_bus_issue";
						$this->args->reservation_id = $this->args->payment_id;
						
						$this->target_server = $this->baseUrl . "/" . $this->{$this->config->ENDPOINT_SERVER_MAPPING[$this->endpoint]};
						$cekresponse_pay = $this->shuttle_bus_issue();
					}
					else{
						$cekresponse_pay->text = "Konfirmasi Transfer Berhasul tetapi Pembayaran Transaksi Gagal. Silakan Melakukan Transaksi Ulang.";
						$cekresponse_pay = json_encode($cekresponse_pay);
					}
					$response_pay = json_decode($cekresponse_pay);
					$response_pay->confirmed = true;
					$response = json_encode($response_pay);
				}
			}
			
			
            
            $this->logger->log("\r\n\r\nPARAMS: {$this->args->service}\r\n\r\n" . print_r($args, true));
            $this->logger->log("\r\n\r\nRESPONSE SRO: \r\n\r\n" . $response);
            
            return $response;
            
        } catch (Exception $e) {
            throw $e;
        }
    }

    protected function deposit_topup_payment_read() {
        try {
            API_Helper::validateForms($this->args, $this->config->PARAM_DEPOSIT_TOPUP_PAYMENT_READ);
//            print_r($this->info_); exit;
            $role_list = array(1004, 1006, 1008, 1010, 1011);
            
            if (!in_array($this->info_->user->role_id, $role_list)) {
                APIException::_throwException(APIException::ERROR_DEPOSIT_TOPUP_NOTAUTHORIZED, $this->info_->user->name, 10020);
            }
            
            $args["function"] = "api.merchantmanager.topup.payment.read";
            $args["input"] = array(                
                "topup_id"=> $this->args->topup_id				
            );
            
            $args = json_encode($args); //echo $this->execute(array("r"=>$args)); exit;
            
            $str = $this->filterResponse($this->execute(array("r"=>$args)));
            $response = json_encode($this->handler->processXml($this->endpoint, json_decode($str)));
            
            $this->logger->log("\r\n\r\nPARAMS: {$this->args->service}\r\n\r\n" . print_r($args, true));
            $this->logger->log("\r\n\r\nRESPONSE SRO: \r\n\r\n" . $response);
            
            return $response;
        } catch (Exception $e) {
            throw $e;
        }
    }
    
    protected function deposit_topup_payment_mocashbri() {
        //api.merchantmanager.topup.payment.mocashbri
        try {
            API_Helper::validateForms($this->args, $this->config->PARAM_PAYMENT_MOCASHBRI);                        
            
            $args["function"] = "api.merchantmanager.topup.payment.mocashbri";
            $args["input"] = array(		
                'bill_no'=> $this->args->invoice_no,
                'bill_reff'=> $this->args->invoice_no,
                'bill_gross'=> $this->args->total_payment,
                'cust_no'=> $this->info_->user->name,
                'cust_name'=> str_replace(" ", "+", "{$this->info_->user->name} ({$this->info_->user->fullname})"),
                'merchant_id'=> $this->info_->user->merchant_id,
                'topup_id'=> $this->args->topup_id,
                'pay_channel'=> $this->args->pay_channel,
                'deposite'=> $this->args->deposite,
                'surcharge'=> $this->args->surcharge
            );
            
            $args = json_encode($args); 
            
            $exec = $this->filterResponse($this->execute(array("r"=>$args))); //02122981198
            $response = json_encode($this->handler->processXml($this->endpoint, json_decode($exec)));
            
            $this->logger->log("\r\n\r\nPARAMS: {$this->args->service}\r\n\r\n" . print_r($args, true));
            $this->logger->log("\r\n\r\nRESPONSE SRO: \r\n\r\n" . $response);
            
            return $response;
        } catch (Exception $e) {
            throw $e;
        }
    }

    protected function payment_inquiry() {
        try {
            API_Helper::validateForms($this->args, $this->config->PARAM_PAYMENT_PPOB_INQUIRY);
			
			$payment_name = $this->args->payment_name;
			$device_model = $this->args->device_model;
			$category = $this->args->catg;
			if(isset($this->args->category)) {
				$category = $this->args->category;
			}
			if(isset($this->args->product)) {
				$payment_name = $this->args->product;
			}
            
            $args["function"] = "api.payment.inquiry";
            $args["input"] = array(
                "type"=> $this->args->product_code,
                "agen"=> $this->info_->user->merchant_code,
                "merchant_id"=> $this->info_->user->merchant_id,
                "user_id"=>$this->info_->user->id,
                "nama_payment"=> $payment_name
            );
            
            $param_options = array("pulsa", "datainet", "donasi", "fintech");
//            $param_options_customer_id_noneeded = array("INETDATA");
            $args["input"]["id_pel"] = $this->args->customer_id;
                        
            if ($payment_name == "PLN Prepaid" || 
                strpos($this->args->product_code, "vi|") > -1 || // hack for old version (temporary)
                strpos($this->args->product_code, "vp|") > -1 || // hack for old version (temporary)
                in_array($category, $param_options)) {
                
                $args["input"]["amount"] = $this->args->amount;
                $args["input"]["fee_amount"] = 1;
				if(empty($this->args->vendor)){
					$args["input"]["vendor"] = 'JK';
				} else{
					$args["input"]["vendor"] = $this->args->vendor;
				}
            } else {
                $args["input"]["fee_amount"] = "";
            }
            
            if(isset($this->args->phone_no) && !empty($this->args->phone_no)){
                $args["input"]["field_no_tlp"] = $this->args->phone_no;
                
            } else if ($category === "bpjs") { //belom dirubah api nya
                $args["input"]["bln_bpjs"] = $this->args->duration_month;
            }
			
			if(isset($category) && !empty($category)){
				$args["input"]["catg"] = $category;
			}
			
			if(isset($this->args->cek_menu) && !empty($this->args->cek_menu)){
				$args["input"]["cek_menu"] = $this->args->cek_menu;
			}
			
			if(isset($this->args->cek_favo) && !empty($this->args->cek_favo)){
				$args["input"]["cek_favo"] = $this->args->cek_favo;
			}
			
			if(isset($this->args->name_favo)){
				$args["input"]["name_favo"] = $this->args->name_favo;
			}
            
            $args = json_encode($args); //echo $this->execute(array("r"=>$args)); exit;
            
            $options = array("payment_category"=> $category);
			if(!empty($device_model)){
				$dev_model = array("device_model"=> $device_model);
				$options = array_merge($options,$dev_model);
			}
            
            $exec = $this->filterResponse($this->execute(array("r"=>$args)));
            $response = json_encode($this->handler->processXml($this->endpoint, json_decode($exec), $options));
            
            $this->logger->log("\r\n\r\nPARAMS: {$this->args->service}\r\n\r\n" . print_r($args, true));
            $this->logger->log("\r\n\r\nRESPONSE SRO: \r\n\r\n" . $response);
            
            return $response;
        } catch (Exception $e) {
            throw $e;
        }
    }

    protected function payment_issue() {
        try {
            API_Helper::validateForms($this->args, $this->config->PARAM_PAYMENT_PPOB_ISSUE);
            
            if (
                strpos($this->args->subproduct_code, "vi") > -1 || //hack for old version (temporary)
                $this->args->catg === "datainet") {
                $product_code = $this->args->amount;
                
            } else {
                $product_code = $this->args->product_code;
            }
            
            $payment_catg_1 = array("pulsa", "datainet");
            
            if (($this->args->catg === "pln" && $this->args->payment_name === "PLN Prepaid") || in_array($this->args->catg, $payment_catg_1)) {
                $fee_amount = 1;
            } else {
                $fee_amount = $this->args->fee_amount;
            }
                        
            $args["function"] = "api.payment.finnet";
            $args["input"] = array(
                "ppob_id"=> $this->args->payment_id,
                "amount"=> $this->args->amount,
                "fee_amount"=> $fee_amount,
                "pay_total"=> $this->args->payment_total,
                "pay_bit61"=> $this->args->customer_id,
                "pay_trxid"=> $this->args->payment_id,
                "product_code"=> $product_code,
                "id_pel"=> (isset($this->args->bill_number)?$this->args->bill_number:$this->args->customer_id),
                "agen"=> $this->info_->user->name,
                "merchant_id"=> $this->info_->user->merchant_id,
                "merchanttype_id"=> $this->info_->user->merchanttype_id,
                "user_id"=> $this->info_->user->id,
                "finnet_id"=> $this->args->finnet_id,
                "nama_payment"=> $this->args->payment_name
            );
            
            $param_options_sierra = array("pam", "multifinance");
            
            if ($this->args->payment_name === "PLN Prepaid" || $this->args->catg === "pam") { //prepaid, pdam
                $bill_ref = explode("/", $this->args->bill_ref);
                $args["input"]["ref_tagihan"] = $bill_ref;
            }
            
            if ($this->args->payment_name !== "PLN Postpaid" && $this->args->payment_name !== "PLN Non Taglis" && !in_array($this->args->catg, $param_options_sierra)) {
                $args["input"]["subproduct_code"]= $this->args->subproduct_code;
            }
            
            if ($this->args->catg === "bpjs" && isset($this->args->phone_no)) {
                $args["input"]["no_hp"] = $this->args->phone_no;
            }
            
            if (isset($this->args->reissue)) {
                $args["input"]["reissued"] = true;
            }
            
            $args = json_encode($args); //echo $this->execute(array("r"=>$args)); exit;
            
            $exec = $this->filterResponse($this->execute(array("r"=>$args))); //02122981198
            $response = json_encode($this->handler->processXml($this->endpoint, json_decode($exec)));
            
            $this->logger->log("\r\n\r\nPARAMS: {$this->args->service}\r\n\r\n" . print_r($args, true));
            $this->logger->log("\r\n\r\nRESPONSE SRO: \r\n\r\n" . $response);
            
            return $response;
        } catch (Exception $e) {
            throw $e;
        }
    }

    protected function payment_read() {
        try {
            API_Helper::validateForms($this->args, $this->config->PARAM_PAYMENT_PPOB_READ);
			
			$device_model = $this->args->device_model;
            
            $args["function"] = "api.payment.ppob.read";
            $args["input"] = array(		
                "id"=> $this->args->payment_id,
                "merchant_id"=> $this->info_->user->merchant_id,
                "parent_id"=> $this->info_->user->merchanttype_id
            );
            
            $args = json_encode($args);
			
			if(!empty($device_model)){
				$options = array("device_model"=> $device_model);
			}
            
            $exec = $this->filterResponse($this->execute(array("r"=>$args))); //02122981198
            $response = json_encode($this->handler->processXml($this->endpoint, json_decode($exec), $options));
            
            $this->logger->log("\r\n\r\nPARAMS: {$this->args->service}\r\n\r\n" . print_r($args, true));
            $this->logger->log("\r\n\r\nRESPONSE SRO: \r\n\r\n" . $response);
            
            return $response;
        } catch (Exception $e) {
            throw $e;
        }
    }

    protected function payment_cancel() {
        try {
            API_Helper::validateForms($this->args, $this->config->PARAM_PAYMENT_PPOB_CANCEL);                        
            
            $args["function"] = "api.payment.channel.cancel";
            $args["input"] = array(
                "id"=> $this->args->payment_id,
                "cancel_user_id"=> $this->info_->user->id		
            );
            
            $args = json_encode($args); //echo $this->execute(array("r"=>$args)); exit;
            
            $str = $this->filterResponse($this->execute(array("r"=>$args)));
            $response = json_encode($this->handler->processXml($this->endpoint, json_decode($str)));
            
            $this->logger->log("\r\n\r\nPARAMS: {$this->args->service}\r\n\r\n" . print_r($args, true));
            $this->logger->log("\r\n\r\nRESPONSE SRO: \r\n\r\n" . $response);
            
            return $response;
        } catch (Exception $e) {
            throw $e;
        }
    }

    protected function payment_check_status()  {
        try {
            API_Helper::validateForms($this->args, $this->config->PARAM_PAYMENT_PPOB_CHECKSTATUS);
            //print_r($this->args); exit;
            
            $args["function"] = "api.payment.checkstatus";
            $args["input"] = array(
                "ppob_id"=> $this->args->payment_id,
                "pay_trxid"=>$this->args->payment_id,
                "product_code"=>$this->args->product_code,
                "id_pel"=> $this->args->bill_number,
                "agen"=> $this->info_->user->name,
                "merchant_id"=>$this->info_->user->merchant_id,
                "merchanttype_id"=>$this->info_->user->merchanttype_id,
                "user_id"=>$this->info_->user->id,
                "finnet_id"=>$this->args->finnet_id,
                "subproduct"=>$this->args->subproduct_code,
                "status_db"=> "PENDING", //$this->args->payment_status, //PENDING
                "nama_payment"=>$this->args->payment_name
            );
            
            $args = json_encode($args); //echo $this->execute(array("r"=>$args)); exit;
            
            $str = $this->filterResponse($this->execute(array("r"=>$args)));
            $response = json_encode($this->handler->processXml($this->endpoint, json_decode($str)));
            
            $this->logger->log("\r\n\r\nPARAMS: {$this->args->service}\r\n\r\n" . print_r($args, true));
            $this->logger->log("\r\n\r\nRESPONSE SRO: \r\n\r\n" . $response);
            
            return $response;
        } catch (Exception $e) {
            throw $e;
        }
    }    

    protected function payment_list() {
        try {
            API_Helper::validateForms($this->args, $this->config->PARAM_PAYMENT_PPOB_LIST);                        
            
            $args["function"] = "api.payment.telkom.list";
            $args["input"] = array(
                "type_payment"=> $this->args->payment_type,                
                "trxid"=> $this->args->payment_id,
                "status"=> $this->args->status,
                "customer_id"=>$this->args->customer_id,
                "merchant_id"=>$this->info_->user->merchant_id,
                "parent_id"=>$this->info_->user->merchanttype_id,                                
                "rows"=>15,
                "page"=>$this->args->page
            );
            
            if ($this->info_->user->merchanttype_id == 1000 || $this->info_->user->role_id == 1001 || $this->info_->user->role_id == 1002) {
                $args["input"]["merchant_id"] = "";
            }
            
            $args = json_encode($args); //echo $this->execute(array("r"=>$args)); exit;
            
            $str = $this->filterResponse($this->execute(array("r"=>$args)));
            $response = json_encode($this->handler->processXml($this->endpoint, json_decode($str)));
            
            $this->logger->log("\r\n\r\nPARAMS: {$this->args->service}\r\n\r\n" . print_r($args, true));
            $this->logger->log("\r\n\r\nRESPONSE SRO: \r\n\r\n" . $response);
            
            return $response;
            
        } catch (Exception $e) {
            throw $e;
        }
    }

    protected function content_view() {
        try {
            API_Helper::validateForms($this->args, $this->config->PARAM_PAYMENT_CONTENT_VIEW);                        
            
            $args["function"] = "api.contents.view";
            $args["input"] = array(		
                "category"=> $this->args->category //1,2,7 (news, promo, image)		
            );
            
            $args = json_encode($args); 
            
            $exec = $this->filterResponse($this->execute(array("r"=>$args))); //02122981198
            $response = json_encode($this->handler->processXml($this->endpoint, json_decode($exec)));
            
            $this->logger->log("\r\n\r\nPARAMS: {$this->args->service}\r\n\r\n" . print_r($args, true));
            $this->logger->log("\r\n\r\nRESPONSE SRO: \r\n\r\n" . $response);
            
            return $response;
        } catch (Exception $e) {
            throw $e;
        }
    }

    protected function content_view_read() {
        try {
            API_Helper::validateForms($this->args, $this->config->PARAM_PAYMENT_CONTENT_VIEW_READ);                        
            
            $args["function"] = "api.contents.view.read";
            $args["input"] = array(		
                "category"=> $this->args->category //1,2,7 (news, promo, image)		
            );
            
            $args = json_encode($args); 
            
            $exec = $this->filterResponse($this->execute(array("r"=>$args))); //02122981198
            $response = json_encode($this->handler->processXml($this->endpoint, json_decode($exec)));
            
            $this->logger->log("\r\n\r\nPARAMS: {$this->args->service}\r\n\r\n" . print_r($args, true));
            $this->logger->log("\r\n\r\nRESPONSE SRO: \r\n\r\n" . $response);
            
            return $response;
        } catch (Exception $e) {
            throw $e;
        }
    }

    protected function user_change_pass() {
        try {
            API_Helper::validateForms($this->args, $this->config->PARAM_USER_CHANGE_PASSWORD);                        
//            print_r($this->info_); exit;
            $args["function"] = "api.usermanager.users.changepassword";
            $args["input"] = array(
                "user_id"=> $this->info_->user->id,
                "user_name"=> $this->info_->user->name,
                "user_password_old"=> md5($this->args->current_password),
                "user_password_new"=> md5($this->args->new_password)
            );                        
            
            $args = json_encode($args);
            $exec = $this->filterResponse($this->execute(array("r"=>$args)));
            
            if (!empty($exec)) {
                $exec = json_decode($exec);
            }
            
            $options = array("endpoint"=> $this->endpoint);            
            $response = json_encode($this->handler->processXml($this->endpoint, $exec, $options));
            
            $check_first = json_decode($response);
            
            if ($check_first->result) {
                $this->info_->user->password = md5($this->args->new_password);
                $resp_ = json_decode($this->get_user_by_name());
                $this->info_->user->key = $resp_->user->key;
                $this->sesshandler->write("info_{$this->args->apikey}", serialize($this->info_));
            }
            
            $this->logger->log("\r\n\r\nPARAMS: {$this->args->service}\r\n\r\n" . print_r($args, true));
            $this->logger->log("\r\n\r\nRESPONSE SRO: \r\n\r\n" . $response);
            
            return $response;
        } catch (Exception $e) {
            throw $e;
        }
    }
    
    protected function shuttle_bus_search() {
        try {
            API_Helper::validateForms($this->args, $this->config->PARAM_SHUTTLE_BUS_SEARCH);
                        
            $args = array(
                "op_travel"=> $this->args->operator,
                "code_depart"=> $this->args->origin,
                "code_arrive"=> $this->args->destination,
                "tanggal_pergi"=> $this->args->depdate,
                "passengers"=> $this->args->pax,
                "departure"=> $this->args->dep_label,
                "destination"=> $this->args->des_label
            );
			
            $exec = $this->filterResponse($this->execute($args));
            $response = json_encode($this->handler->processXml($this->endpoint, json_decode($exec)));
            
            $this->logger->log("\r\n\r\nPARAMS: {$this->args->service}\r\n\r\n" . print_r($args, true));
            $this->logger->log("\r\n\r\nRESPONSE SRO: \r\n\r\n" . $response);
            
            return $response;
        } catch (Exception $e) {
            throw $e;
        }
    }
	
    protected function shuttle_bus_cancel() {
        try {
            API_Helper::validateForms($this->args, $this->config->PARAM_SHUTTLE_BUS_CANCEL);                        
            
            $args["function"] = "api.tickettransactions.reservations.travel.cancel";
            $args["input"] = array(
                "id"=> $this->args->reservation_id,
                "cancel_user_id"=> $this->info_->user->id		
            );
            
            $args = json_encode($args); //echo $this->execute(array("r"=>$args)); exit;
			
            $str = $this->filterResponse($this->execute(array("r"=>$args)));
            $response = json_encode($this->handler->processXml($this->endpoint, json_decode($str)));
            
            $this->logger->log("\r\n\r\nPARAMS: {$this->args->service}\r\n\r\n" . print_r($args, true));
            $this->logger->log("\r\n\r\nRESPONSE SRO: \r\n\r\n" . $response);
            
            return $response;
        } catch (Exception $e) {
            throw $e;
        }
    }
    
    protected function shuttle_bus_data() {
        try{            
            API_Helper::validateForms($this->args, $this->config->PARAM_SHUTTLE_BUS_GETDATA);  
            
            $args = array(
                "r"=>json_encode(array(
                    "function"=> "api.get.data.shuttle",
                    "input"=> array(
                        "getdata"=> $this->args->data,
                        "op_travel"=> $this->args->bus_brand
                    )
                ))
            );
            
            $exec = $this->filterResponse($this->execute($args));
            $response = json_encode($this->handler->processXml($this->endpoint, json_decode($exec)));

            $this->logger->log("\r\n\r\nPARAMS: {$this->args->service}\r\n\r\n" . print_r($args, true));
            $this->logger->log("\r\n\r\nRESPONSE SRO: \r\n\r\n" . $response);

            return $response;
        } catch (Exception $e) {
            throw $e;
        }
    }
    
    protected function shuttle_bus_seat() {
        try{
            API_Helper::validateForms($this->args, $this->config->PARAM_SHUTTLE_BUS_SEAT);
                        
            $args = array(
                "username"=> $this->info_->user->name,
                "kursi_layout"=> $this->args->layout_seat,
                "op_travel"=> $this->args->operator,
                "code_arrive"=> $this->args->route_code,
                "tanggal_pergi"=> $this->args->depdate
            );
//            print_r($this->execute($args)); exit;
            $exec = $this->filterResponse($this->execute($args));
//            print_r($exec); exit;
            $response = json_encode($this->handler->processXml($this->endpoint, json_decode($exec)));
            
            $this->logger->log("\r\n\r\nPARAMS: {$this->args->service}\r\n\r\n" . print_r($args, true));
            $this->logger->log("\r\n\r\nRESPONSE SRO: \r\n\r\n" . $response);
            
            return $response;
        } catch (Exception $e) {
            throw $e;
        }
    }
    
    protected function shuttle_bus_book() {
        API_Helper::validateForms($this->args, $this->config->PARAM_SHUTTLE_BUS_BOOK);  
        
        $this->args->search_data = (is_string($this->args->search_data)?json_decode($this->args->search_data):$this->args->search_data);
        $this->args->adultchilds = (is_string($this->args->adultchilds)?json_decode($this->args->adultchilds):$this->args->adultchilds);
        
        $data_search_pergi = array();        
        
        foreach ($this->args->search_data as $key => $value) {
            $item = new stdClass();
            $item->travelCode = array($value->sched_code);
            $item->schdule = array($value->deptime);
            $item->route = array($value->route);
            $item->seat = array($value->seat);
            $item->total = array($value->total);
            $item->travelName = array($value->travelName);
            $item->busType = array($value->busType);
            $item->departTime = array($value->departTime);
            $item->arriveTime = array($value->arriveTime);
            $item->city = array($value->city);
            
            array_push($data_search_pergi, $item);            
        }              
        
        $adultchilds = new stdClass();        
        $adultchilds_item = array();        
        
        foreach ($this->args->adultchilds as $key => $value) {
            $item = new stdClass();
            $item->salutation = $value->salutation;
            $item->fullname = $value->fullname;
            $item->seat = $value->seat;
            $item->age = $value->age;
            
            array_push($adultchilds_item, $item);
        }
        
        $adultchilds->data = $adultchilds_item;
        $adultchilds->_count = count($adultchilds_item);                
        
        $args["function"] = "api.tickettransactions.reservations.book.travel";        
        $args["input"]["data_p"] = array(
            "user_id"=> $this->info_->user->id,
            "username"=> $this->info_->user->name,
            "merchant_id"=> $this->info_->user->merchant_id,
            "op_travel"=> $this->args->brand,
            "code_depart"=> $this->args->origin,
            "code_arrive"=> $this->args->destination,
            "tanggal_pergi"=> $this->args->depdate,
            "passengers"=> $this->args->pax,
            "seatId_pergi"=> $this->args->seatid_dep,
            "data_search_pergi"=> $data_search_pergi,
            "adultchild"=> $adultchilds,
            "nm_pemesan"=> $this->args->contact_name,
            "tlp_pemesan"=> $this->args->contact_phone,
            "email"=> $this->args->contact_email,
            "address"=> $this->args->contact_addr            
        );
        
        $args["input"]["book_new"] = "book_new";
        
//        print_r($args); exit;
//        print_r($this->execute(array("r"=>json_encode($args)))); exit; 
        $exec = $this->filterResponse($this->execute(array("r"=>json_encode($args))));
        $response = json_encode($this->handler->processXml($this->endpoint, json_decode($exec)));

        $this->logger->log("\r\n\r\nPARAMS: {$this->args->service}\r\n\r\n" . print_r($args, true));
        $this->logger->log("\r\n\r\nRESPONSE SRO: \r\n\r\n" . $response);

        return $response;
    }
    
    protected function shuttle_bus_read() {
        try{
            API_Helper::validateForms($this->args, $this->config->PARAM_SHUTTLE_BUS_READ);
                        
            $args["function"] = "api.transactions.reservations.travel.view";
            $args["input"] = array(
                "id"=> $this->args->reservation_id,
                "merchant_id"=> $this->info_->user->merchant_id,
                "parent_id"=> $this->info_->user->merchanttype_id
            );
//            echo $this->execute(array("r"=> json_encode($args))); exit;
			$args = json_encode($args);
			
            $exec = $this->filterResponse($this->execute(array("r"=>$args)));
            $response = json_encode($this->handler->processXml($this->endpoint, json_decode($exec)));
            
            $this->logger->log("\r\n\r\nPARAMS: {$this->args->service}\r\n\r\n" . print_r($args, true));
            $this->logger->log("\r\n\r\nRESPONSE SRO: \r\n\r\n" . $response);
            
            return $response;
        } catch (Exception $e) {
            throw $e;
        }
    }
    
    protected function shuttle_bus_issue() {
        try{
            API_Helper::validateForms($this->args, $this->config->PARAM_SHUTTLE_BUS_ISSUE);
            
			$args["function"] = "api.tickettransactions.reservations.travel.issue";
            $args["input"] = array(
                "id"=> $this->args->reservation_id,
                "issue_user_id"=> $this->info_->user->id		
            );
			
			$args = json_encode($args);
			
            $exec = $this->filterResponse($this->execute(array("r"=>$args)));
            $response = json_encode($this->handler->processXml($this->endpoint, json_decode($exec)));
            
            $this->logger->log("\r\n\r\nPARAMS: {$this->args->service}\r\n\r\n" . print_r($args, true));
            $this->logger->log("\r\n\r\nRESPONSE SRO: \r\n\r\n" . $response);
            
            return $response;
        } catch (Exception $e) {
            throw $e;
        }
    }
    
    protected function shuttle_bus_list() {
        try{
            API_Helper::validateForms($this->args, $this->config->PARAM_SHUTTLE_BUS_LIST);
            
            $args["r"] = json_encode(array(
                "function"=>"api.tickettransactions.reservations.list.travel",
                "input" =>             
                    array(
                        "bookcode"=> $this->args->pnr,
                        "contact_name"=> $this->args->contact_name,
                        "contact_telp"=> $this->args->contact_telp,
                        "booking_datetime"=> $this->args->booking_datetime,
                        "issue_datetime"=> $this->args->issue_datetime,
                        "travel_merchant_id"=> ($this->info_->user->merchant_id === "1000"?"":$this->info_->user->merchant_id),
                        "merchant_code"=> $this->info_->user->merchant_code,
						"rows"=> $this->args->rows,
						"page"=> $this->args->page,
                        "status"=> (!empty($this->args->status)?$this->args->status:"1")
                    )
            ));
            
            $exec = $this->filterResponse($this->execute($args));
            $response = json_encode($this->handler->processXml($this->endpoint, json_decode($exec)));
            
            $this->logger->log("\r\n\r\nPARAMS: {$this->args->service}\r\n\r\n" . print_r($args, true));
            $this->logger->log("\r\n\r\nRESPONSE SRO: \r\n\r\n" . $response);
            
            return $response;
            
        } catch (Exception $e) {
            throw $e;
        }
    }
    
    protected function umrah_schedule_list() {
        try {
            API_Helper::validateForms($this->args, $this->config->PARAM_UMRAH_SCHEDULE_LIST);
            
            $args["r"] = json_encode(array(
                "function"=>"api.umrah.schedule.list",
                "input" =>
                    array(
						"user_id"=> $this->info_->user->id,
						"username"=> $this->info_->user->name,
                        "umrah_list" => $this->args->list,
						"rows"=> $this->args->rows,
						"page"=> $this->args->page,
						"sord"=> $this->args->sord,
						"sidx"=> $this->args->sidx
                    )
            ));
            
            $exec = $this->filterResponse($this->execute($args));
            $response = json_encode($this->handler->processXml($this->endpoint, json_decode($exec)));
            
            $this->logger->log("\r\n\r\nPARAMS: {$this->args->service}\r\n\r\n" . print_r($args, true));
            $this->logger->log("\r\n\r\nRESPONSE SRO: \r\n\r\n" . $response);
            
            return $response;
        } catch (Exception $e) {
            throw $e;
        }
    }
	
	protected function invoice_umrah() {
        try {
            API_Helper::validateForms($this->args, $this->config->PARAM_INVOICE_UMRAH);
			
            $args["r"] = json_encode(array(
                "function"=>"api.tickettransactions.reservations.generate_invoice",
                "input" =>
                    array(
                        "id_umrah" => $this->args->umrah_id,
                        "user_id"=> $this->info_->user->id,
                        "invoice_umrah"=> "invoice_umrah"
                    )
            ));
            
            $exec = $this->filterResponse($this->execute($args));
            $response = json_encode($this->handler->processXml($this->endpoint, json_decode($exec)));
            
            $this->logger->log("\r\n\r\nPARAMS: {$this->args->service}\r\n\r\n" . print_r($args, true));
            $this->logger->log("\r\n\r\nRESPONSE SRO: \r\n\r\n" . $response);
            
            return $response;
        } catch (Exception $e) {
            throw $e;
        }
    }
	
	protected function umrah_registration_list() {
        try {
            API_Helper::validateForms($this->args, $this->config->PARAM_UMRAH_REGISTRATION_LIST);
			
			if($this->info_->user->merchant_id == 1000){
				$merchant_id = '';
			}
			else{
				$merchant_id = $this->info_->user->merchant_id;
			}
            
            $args["r"] = json_encode(array(
                "function"=>"api.reservations.umrah.list",
                "input" =>
                    array(
                        "merchant_id" => $merchant_id,
                        "no_umrah" => $this->args->no_umrah,
                        "no_ktp" => $this->args->no_ktp,
                        "status" => $this->args->status,
						"rows"=> $this->args->rows,
						"page"=> $this->args->page,
						"sord"=> $this->args->sord,
						"sidx"=> $this->args->sidx
                    )
            ));
            
            $exec = $this->filterResponse($this->execute($args));
            $response = json_encode($this->handler->processXml($this->endpoint, json_decode($exec)));
            
            $this->logger->log("\r\n\r\nPARAMS: {$this->args->service}\r\n\r\n" . print_r($args, true));
            $this->logger->log("\r\n\r\nRESPONSE SRO: \r\n\r\n" . $response);
            
            return $response;
        } catch (Exception $e) {
            throw $e;
        }
    }
	
	protected function umrah_registration_detail() {
        try {
            API_Helper::validateForms($this->args, $this->config->PARAM_UMRAH_REGISTRATION_DETAIL);
            
            $args["r"] = json_encode(array(
                "function"=>"api.reservations.umrah.detail",
                "input" =>
                    array(
                        "id" => $this->args->umrah_id,
                        "merchant_id" => $this->info_->user->merchant_id,
                        "parent_id"=> $this->info_->user->merchanttype_id
                    )
            ));
            
            $exec = $this->filterResponse($this->execute($args));
            $response = json_encode($this->handler->processXml($this->endpoint, json_decode($exec)));
            
            $this->logger->log("\r\n\r\nPARAMS: {$this->args->service}\r\n\r\n" . print_r($args, true));
            $this->logger->log("\r\n\r\nRESPONSE SRO: \r\n\r\n" . $response);
            
            return $response;
        } catch (Exception $e) {
            throw $e;
        }
    }
    
    protected function umrah_pilgrims_registration() {
        try {
            API_Helper::validateForms($this->args, $this->config->PARAM_UMRAH_PILGRIMS_REGISTER);
            
            $args["r"] = json_encode(array(
                "function"=>"api.daftar.umrah",
                "input" =>
                    array(
                        "scheduled" => $this->args->depdate,                        
                        "title" => $this->args->pilgrims_title,
                        "fullname" => $this->args->pilgrims_fullname,
                        "name_father" => $this->args->pilgrims_fathername,
                        "place_birth" => $this->args->pilgrims_birthplace,
                        "date_birth" => $this->args->pilgrims_birthday,
                        "no_ktp" => $this->args->pilgrims_docnumber,
                        "gender" => $this->args->pilgrims_gender,
                        "citizen" => $this->args->pilgrims_citizen,
                        "address" => $this->args->pilgrims_address,
                        "handphone" => $this->args->pilgrims_phonenumber,
                        "email" => $this->args->pilgrims_email,
                        "hub_mahram" => $this->args->pilgrims_mahram_rel,
                        "package_label" => $this->args->room_type,
                        "package_option" => $this->args->umrah_package_price,
                        "umrah_photo" => (empty($this->args->pilgrims_photo)?array():$this->args->pilgrims_photo),
                        "name_umrah" => $this->args->umrah_package_name,
                        "types_ramadhan" => $this->args->umrah_package_ramadhan_type,
                        "merchant_id" => $this->info_->user->merchant_id,
                        "create_user_id" => $this->info_->user->id,
                    )
            ));
            
//            echo $this->execute($args); exit;
            
            $exec = $this->filterResponse($this->execute($args));
            $response = json_encode($this->handler->processXml($this->endpoint, json_decode($exec)));
            
            $this->logger->log("\r\n\r\nPARAMS: {$this->args->service}\r\n\r\n" . print_r($args, true));
            $this->logger->log("\r\n\r\nRESPONSE SRO: \r\n\r\n" . $response);
            
            return $response;
        } catch (Exception $e) {
            throw $e;
        }
    }
    
    protected function payment_list_lookup() {
        try {
            API_Helper::validateForms($this->args, $this->config->PARAM_PAYMENT_PPOB_LIST);
			
			$operator = '';
			if(isset($this->args->provider)){
				$operator = $this->args->provider;
			}
			if(isset($this->args->operator)){
				$operator = $this->args->operator;
			}
			if(isset($this->args->product)){
				$operator = $this->args->product;
			}
			
			$datappob = '';
			if(isset($this->args->data)){
				$datappob = $this->args->data;
			}
			if(isset($this->args->category)){
				$datappob = $this->args->category;
			}
            
            $args["function"] = "api.get.data.ppob";
            $args["input"] = array(
                "getdata"=> $datappob,
                "operator"=> $operator,
                "device"=> $this->args->device_model
            );
            
            $args = json_encode($args); //print_r($this->execute(array("r"=>$args))); exit;
            
            $exec = $this->filterResponse($this->execute(array("r"=>$args))); //02122981198
            $response = json_encode($this->handler->processXml($this->endpoint, json_decode($exec)));
            
            $this->logger->log("\r\n\r\nPARAMS: {$this->args->service}\r\n\r\n" . print_r($args, true));
            $this->logger->log("\r\n\r\nRESPONSE SRO: \r\n\r\n" . $response);
            
            return $response;
        } catch (Exception $e) {
            throw $e;
        }
    }
    
    protected function report_transaction_list() {
        try {
            API_Helper::validateForms($this->args, $this->config->PARAM_REPORT_TRSC_LIST);
            
            $args["function"] = "api.merchantmanager.transactions.list";
            $args["input"] = array(
                "transaction_transdate_start"=> $this->args->begindate,
                "transaction_transdate_end"=> $this->args->enddate,
                "merchant_id"=> $this->info_->user->merchant_id,
                "rows"=> $this->args->rows,
                "page"=> $this->args->page,
				"sord"=> $this->args->sord,
				"sidx"=> $this->args->sidx
            );
            
            $args = json_encode($args);
            
            $exec = $this->filterResponse($this->execute(array("r"=>$args)));
            $response = json_encode($this->handler->processXml($this->endpoint, json_decode($exec)));
            
            $this->logger->log("\r\n\r\nPARAMS: {$this->args->service}\r\n\r\n" . print_r($args, true));
            $this->logger->log("\r\n\r\nRESPONSE SRO: \r\n\r\n" . $response);
            
            return $response;
        } catch (Exception $e) {
            throw $e;
        }
    }
    
    protected function print_ticket() {
		try {
            API_Helper::validateForms($this->args, $this->config->PARAM_PRINT_TICKET);
			$kode_booking = $this->args->kode_booking;
			if($this->args->airlines_id == 10 || $this->args->airlines_id == 6 || $this->args->airlines_id == 3){
				$params = array(
					"pnr"=> $this->args->kode_booking,
					"airlines_id"=> $this->args->airlines_id,
					"reservation_id"=> $this->args->reservation_id
				);
				
				if($this->args->airlines_id == 6){
					 $params = array_merge($params, array(
						"datapost" => "datapost"
					));
				}
					
				$args["r"] = json_encode(array(
					"function"=>"api.tickettransactions.reservations.ticket_save",
					"input" => $params
				));
				
				$exec = $this->filterResponse($this->execute($args));
				$response = json_encode($this->handler->processXml($this->endpoint, json_decode($exec)));
				
				$this->logger->log("\r\n\r\nPARAMS: {$this->args->service}\r\n\r\n" . print_r($args, true));
			}
			else if(!empty($this->args->ppob) && !empty($this->args->ppob_id)){
				$params = array(
					"ppob"=> $this->args->ppob,
					"ppob_id"=> $this->args->ppob_id,
					"merchant_id" => $this->info_->user->merchant_id,
					"user_id"=> $this->info_->user->id
				);
				
				$args["r"] = json_encode(array(
					"function"=>"api.tickettransactions.reservations.ticket_save",
					"input" => $params
				));
				
				$exec = $this->filterResponse($this->execute($args));
				$response = json_encode($this->handler->processXml($this->endpoint, json_decode($exec)));
				
				$this->logger->log("\r\n\r\nPARAMS: {$this->args->service}\r\n\r\n" . print_r($args, true));
			}
			else if($this->args->airlines_id == 4){
				$params = array(
					"id"=> $this->args->reservation_id
				);
				
				$args["r"] = json_encode(array(
					"function"=>"api.tickettransactions.reservations.ticket_print",
					"input" => $params
				));
				
				$exec = $this->filterResponse($this->execute($args));
				$response = json_encode($this->handler->processXml($this->endpoint, json_decode($exec)));
				
				$this->logger->log("\r\n\r\nPARAMS: {$this->args->service}\r\n\r\n" . print_r($args, true));
			}
			else if($this->args->airlines_id == 2){
				$res_print = array(
					"result_status" => "OK",
					"result_code" => "00000",
					"result_message" => "Success",
					"url_ticket" => array("https://www.ibe.co.id/static/ticket/lion/$kode_booking.pdf"),
				);
				$response = json_encode($res_print);
			}
			else{
				$res_print = array(
					"result_status" => "FAILED",
					"result_code" => "01010",
					"result_message" => "Maintenance",
					"url_ticket" => array(""),
				);
				$response = json_encode($res_print);
			}
			
			$this->logger->log("\r\n\r\nRESPONSE SRO: \r\n\r\n" . $response);
            
            return $response;
			
		} catch (Exception $e) {
            throw $e;
        }
	}
	
    protected function send_ticket() {
        try {
            API_Helper::validateForms($this->args, $this->config->PARAM_SEND_TICKET);
            
            $params = array(
                        "reservation_id"=> $this->args->reservation_id,
                        "email"=> $this->args->email,
                        "userid"=> $this->info_->user->id,
                    );
            
            if (isset($this->args->voucher_num) && !empty($this->args->voucher_num)) {
                $params = array_merge($params, array(
                    "datapost" => json_encode($this->args->datapost),
                    "merchant_id" => $this->info_->user->merchant_id,
                    "book_code" => $this->args->book_code,
                    "voucher_num" => $this->args->voucher_num,
                    "hotel_id" => 100
                ));
                
            } else if (isset($this->args->train_id)) {
                $params = array_merge($params, array(
                    "datapost" => json_encode($this->args->datapost),
                    "merchant_id" => $this->info_->user->merchant_id,
                    "book_code" => $this->args->book_code,
                    "kereta_id" => $this->args->train_id
                ));
            }
			
			if($this->args->airlines_id == 10){
				$this->args->service = "print-ticket";
				$this->endpoint = "print_ticket";
				
				$response_print = $this->print_ticket();
				$cekprint_ticket = json_decode($response_print);
				$result_code = $cekprint_ticket->result_code;
				$result_status = $cekprint_ticket->result_status;
				if($result_code == '00000' && $result_status == 'OK'){
					$this->args->service = "send-ticket";
					$this->endpoint = "send_ticket";
					$args["r"] = json_encode(array(
						"function"=>"api.send.email.tickets", //api.tickettransactions.reservations.ticket_email
						"input" => $params
					));
				}
			}
            else{
				$args["r"] = json_encode(array(
					"function"=>"api.send.email.tickets", //api.tickettransactions.reservations.ticket_email
					"input" => $params
				));
			}

            $exec = $this->filterResponse($this->execute($args));
            $response = json_encode($this->handler->processXml($this->endpoint, json_decode($exec)));
            
            $this->logger->log("\r\n\r\nPARAMS: {$this->args->service}\r\n\r\n" . print_r($args, true));
            $this->logger->log("\r\n\r\nRESPONSE SRO: \r\n\r\n" . $response);
            
            return $response;
        } catch (Exception $e) {
            throw $e;
        }
    }
	
	protected function merchant_notif_add() {
        try {
            API_Helper::validateForms($this->args, $this->config->PARAM_MERCHANT_NOTIF_ADD);
			
			// $title = $this->args->title;
			// $contents = $this->args->contents;
			// $merchanttype = $this->args->merchanttype;
			// $images_notif = $this->args->images_notif;
			// $status_notif = $this->args->status;
			// $device = $this->args->device;
			// $actions = $this->args->actions;
			
			// if($device == '1'){
				// $device = '0'; //android
			// }
			// else if($device == '2'){
				// $device = '1'; //iphone
			// }
			
			// if($status_notif == '1'){
				// $status_notif = '0'; //publish
			// }
			// else if($status_notif == '2'){
				// $status_notif = '1'; //non publish
			// }
			
			
			$args["function"] = "api.merchant.notif.add";
			$args["input"] = array(
				"title" => "TEST NOTIF",
				"contents" => "TEST NOTIF DETAIL",
				"images_notif" => "",
				"device" => '0',
				"merchanttype" => '*',
				"status"=> '0',
				"actions"=> '',
                "create_userid"=> $this->info_->user->id
			);
            
            $args = json_encode($args);
            $exec = $this->filterResponse($this->execute(array("r"=>$args)));
            $response = json_encode($this->handler->processXml($this->endpoint, json_decode($exec)));
            
            $this->logger->log("\r\n\r\nPARAMS: {$this->args->service}\r\n\r\n" . print_r($args, true));
            $this->logger->log("\r\n\r\nRESPONSE SRO: \r\n\r\n" . $response);
            
            return $response;
            
        } catch (Exception $e) {
            throw $e;
        }
    }
	
	protected function merchant_notif_list() {
        try {
            API_Helper::validateForms($this->args, $this->config->PARAM_MERCHANT_NOTIF_LIST);
			
			$merchanttype_id = $this->info_->user->merchanttype_id;
			$status_notif = $this->args->status;
			if($merchanttype_id == 1000){
				$merchanttype_id = '*';
			}
			
			if($status_notif == '1'){
				$status_notif = '0';
			}
			else if($status_notif == '2'){
				$status_notif = '1';
			}
			
			
			$args["function"] = "api.merchant.notif";
			$args["input"] = array(
				"merchanttype_id" => $merchanttype_id,
				"device" => 0,
				"status" => $status_notif,
				"rows" => 10,
				"page" => $this->args->page,
				"sidx"=> $this->args->sidx,
                "sord"=> $this->args->sord
			);
            
            $args = array(
                "r"=>json_encode($args),
                "_search"=>false,
                "rows"=>10,
                "page"=> (!empty($this->args->page)?$this->args->page:1),
                "sidx"=> $this->args->sidx,
                "sord"=> $this->args->sordx
            );
			
            $exec = $this->filterResponse($this->execute($args));
            $response = json_encode($this->handler->processXml($this->endpoint, json_decode($exec)));
            
            $this->logger->log("\r\n\r\nPARAMS: {$this->args->service}\r\n\r\n" . print_r($args, true));            
            $this->logger->log("\r\n\r\nRESPONSE:\r\n\r\n". print_r($response, true));
            
            return $response;
            
        } catch (Exception $e) {
            throw $e;
        }
    }
	
	protected function favorite_ppob() {
        try {
            API_Helper::validateForms($this->args, $this->config->PARAM_FAVORITE_PPOB);
			
			$catg = $this->args->catg_ppob;
			$operator = $this->args->operator;
			if(empty($operator)){
				$operator = 'Choose';
			}
			
			$args["function"] = "api.favorite.ppob";
			$args["input"] = array(
				"merchant_id" => $this->info_->user->merchant_id,
				"catg" => $catg,
				"operator" => $operator
			);
			
            $args = json_encode($args);
            $exec = $this->filterResponse($this->execute(array("r"=>$args)));
            $response = json_encode($this->handler->processXml($this->endpoint, json_decode($exec)));
            
            $this->logger->log("\r\n\r\nPARAMS: {$this->args->service}\r\n\r\n" . print_r($args, true));
            $this->logger->log("\r\n\r\nRESPONSE SRO: \r\n\r\n" . $response);
            
            return $response;
            
        } catch (Exception $e) {
            throw $e;
        }
    }
	
	protected function delete_favorite_ppob() {
        try {
            API_Helper::validateForms($this->args, $this->config->PARAM_FAVORITE_PPOB);
			
			$data_favo = $this->args->data_favo;
			$catg_ppob = $this->args->catg_ppob;
			
			$args["function"] = "api.payment.delete.favo";
			$args["input"] = array(
				"data_favo" => $data_favo,
				"merchant_id" => $this->info_->user->merchant_id,
				"page" => $catg_ppob,
				"user_id" => $this->info_->user->id
			);
			
            $args = json_encode($args);
            $exec = $this->filterResponse($this->execute(array("r"=>$args)));
            $response = json_encode($this->handler->processXml($this->endpoint, json_decode($exec)));
            
            $this->logger->log("\r\n\r\nPARAMS: {$this->args->service}\r\n\r\n" . print_r($args, true));
            $this->logger->log("\r\n\r\nRESPONSE SRO: \r\n\r\n" . $response);
            
            return $response;
            
        } catch (Exception $e) {
            throw $e;
        }
    }
	
	protected function check_harga() {
        try {
            API_Helper::validateForms($this->args, $this->config->PARAM_CHECKCOOKIE_LION);
			$berangkat = $this->args->berangkat;
			$tujuan = $this->args->tujuan;
			$tglPergi = $this->args->tglPergi;
			$tglPulang = $this->args->tglPulang;
			$isReturn = $this->args->isReturn;
			$dewasa = $this->args->dewasa;
			$anak = $this->args->anak;
			$bayi = $this->args->bayi;
			$seatId_pergi = $this->args->seatId_pergi;
			$seatId_pulang = $this->args->seatId_pulang;
			$cookie_lion = $this->args->cookie_lion;
			$airlines_id = $this->args->airlines_id;
			$cek_harga = $this->args->cek_harga;
			
			$this->target_server = $this->baseUrl . "/" . $this->function_api;
			
			if($airlines_id == 2){
				$this->args->service = "cookie-lion";
				$this->args->status_proses = 1;
				$this->endpoint = "cookie_lion";
				for($i=0;$i<5;$i++){
					$response_cookie = $this->cookie_lion();
					$de_cookie = json_decode($response_cookie);
					$cookie_lion = $de_cookie->cookie;
					if(!empty($cookie_lion)){
						break;
					}
				}
			}
			
			$this->args->service = "check-harga";
			$this->endpoint = "check_harga";
				
			if($cek_harga){
				$swapReturn = $this->args->swapReturn;
				if($swapReturn){
					$booking_request_pp = 2;
				}
				else{
					$booking_request_pp = 1;
				}
				$data_search_pergi = $this->args->data_search_pergi;
				$jml_transit_pergi = count($data_search_pergi[0]->flightno);
				$flightNo_pergi = array();
				for($i=0;$i<$jml_transit_pergi;$i++){
					$flightNo_pergi[$i] = $data_search_pergi[0]->flightno[$i];
				}
				if($jml_transit_pergi == 1){
					$jml_transit_pergi = 0;
				}
				
				$data_search_pulang = $this->args->data_search_pulang;
				$jml_transit_pulang = count($data_search_pulang[0]->flightno);
				$flightNo_pulang = array();
				for($i=0;$i<$jml_transit_pulang;$i++){
					$flightNo_pulang[$i] = $data_search_pulang[0]->flightno[$i];
				}
				if($jml_transit_pulang == 1){
					$jml_transit_pulang = 0;
				}
				
				$this->target_server = $this->baseUrl . "/" . $this->book_flights_lionweb;
				$cek_params = array(
					"lion_cek_harga"=>$cek_harga,
					"cek_harga"=>$cek_harga,
					"cookie"=>$cookie_lion,
					"jml_transit_pergi"=>$jml_transit_pergi,
					"jml_transit_pulang"=>$jml_transit_pulang,
					"flightNo_pergi"=> $flightNo_pergi,
					"flightNo_pulang"=> $flightNo_pulang,
					"booking_request_pp"=> $booking_request_pp,
					"booking_rute_berangkat"=> $berangkat,
					"booking_rute_tujuan"=> $tujuan,
					"booking_request_tglpergi"=> $tglPergi,
					"booking_request_tglpulang"=> $tglPulang,
					"booking_request_dewasa"=> $dewasa,
					"booking_request_anak"=> $anak,
					"booking_request_bayi"=> $bayi,
					"data_search_pergi"=> $data_search_pergi,
					"data_search_pulang"=> $data_search_pulang,
					"username"=> $this->info_->user->name
				);
				$params = http_build_query($cek_params);
			}
			else{
				$params = array(              
					"function_name"=>"check_harga",
					"berangkat"=>$berangkat,
					"tujuan"=>$tujuan,
					"tglPergi"=>$tglPergi,
					"tglPulang"=>$tglPulang,
					"isReturn"=>$isReturn,
					"dewasa"=>$dewasa,
					"anak"=>$anak,
					"bayi"=>$bayi,
					"seatId_pergi"=>$seatId_pergi,
					"seatId_pulang"=>$seatId_pulang,
					"cookie_lion"=> $cookie_lion,
					"airlines_id"=>$airlines_id,
					"username"=> $this->info_->user->name
				);
				
			}
            $response = json_encode($this->handler->processXml($this->endpoint, json_decode($this->execute($params)), $options));
            $this->logger->log("\r\n\r\nPARAMS: {$this->args->service}\r\n\r\n" . print_r($args, true));
            $this->logger->log("\r\n\r\nRESPONSE SRO: \r\n\r\n" . $response);
            
            return $response;
            
        } catch (Exception $e) {
            throw $e;
        }
    }
	
	protected function cookie_lion() {
        try {
            API_Helper::validateForms($this->args, $this->config->PARAM_CHECKCOOKIE_LION);
			
			$status_proses = $this->args->status_proses;
			
			if($status_proses == 1){
				$params = array(              
					"function_name"=>"cek_cookie",
					"status_proses"=>$status_proses,
					"username"=> $this->info_->user->name
				);
				$hasil_cookie = $this->handler->processXml($this->endpoint, json_decode($this->execute($params)), $options);
				
				if(!empty($hasil_cookie->cookie)){
					$response = json_encode($hasil_cookie);
				}
				else{
					$params = array(              
						"function_name"=>"captcha",
						"status_proses"=>$status_proses,
						"username"=> $this->info_->user->name
					);
					$response = json_encode($this->handler->processXml($this->endpoint, json_decode($this->execute($params)), $options));
				}
				
			}
			else{
				$params = array(              
					"function_name"=>"captcha",
					"status_proses"=>$status_proses,
					"username"=> $this->info_->user->name
				);
				$response = json_encode($this->handler->processXml($this->endpoint, json_decode($this->execute($params)), $options));
			}
            
            $this->logger->log("\r\n\r\nPARAMS: {$this->args->service}\r\n\r\n" . print_r($args, true));
            $this->logger->log("\r\n\r\nRESPONSE SRO: \r\n\r\n" . $response);
            
            return $response;
            
        } catch (Exception $e) {
            throw $e;
        }
    }
	
	protected function search_flight_airlines() {
        try {
            API_Helper::validateForms($this->args, $this->config->PARAM_SEARCH_FLIGHT_AIRLINES);
			
			$airlines_id = $this->args->airlines_id;
			$dewasa = $this->args->dewasa;
			$anak = $this->args->anak;
			$bayi = $this->args->bayi;
			$ruteBerangkat = $this->args->ruteBerangkat;
			$ruteTujuan = $this->args->ruteTujuan;
			$ruteKembali = $this->args->ruteKembali;
			$pp = $this->args->pp;
			$tanggal_pergi = $this->args->tanggal_pergi;
			$tanggal_pulang = $this->args->tanggal_pulang;
			
			if($airlines_id == 2){
				$this->args->service = "cookie-lion";
				$this->args->status_proses = 1;
				$this->endpoint = "cookie_lion";
				$this->target_server = $this->baseUrl_aws . "/" . $this->function_api;
				$response_cookie = $this->cookie_lion();
				$de_cookie = json_decode($response_cookie);
				$cookie_lion = $de_cookie->cookie;
			}
			print_r("COOKIE LION : $cookie_lion");
			if(!empty($cookie_lion)){
				$this->target_server = $this->baseUrl_aws . "/" . $this->search_flights_lion;
				$this->endpoint = "search_flight_airlines";
				$params = array(              
					"airlines_id"=>$airlines_id,
					"dewasa"=>$dewasa,
					"anak"=>$anak,
					"bayi"=>$bayi,
					"ruteBerangkat"=>$ruteBerangkat,
					"ruteTujuan"=>$ruteTujuan,
					"ruteKembali"=>$ruteKembali,
					"pp"=>$pp,
					"cookie"=>$cookie_lion,
					"tanggal_pergi"=>$tanggal_pergi,
					"tanggal_pulang"=>$tanggal_pulang,
					"username"=> $this->info_->user->name
				);
			}
			
			$exec = $this->filterResponse($this->execute($params));
			$response = json_encode($this->handler->processXml($this->endpoint, json_decode($exec)));
            
            $this->logger->log("\r\n\r\nPARAMS: {$this->args->service}\r\n\r\n" . print_r($args, true));
            $this->logger->log("\r\n\r\nRESPONSE:\r\n\r\n". print_r($response, true));
            
            return $response;
            
        } catch (Exception $e) {
            throw $e;
        }
    }
	
	protected function book_flight_airlines() {
        try {
            API_Helper::validateForms($this->args, $this->config->PARAM_BOOK_FLIGHT_AIRLINES);
			
			$airlines_id = $this->args->airlines_id;
			$adultchild = $this->args->adultchild;
			$child = $this->args->child;
			$infant = $this->args->infant;
			$dewasa = $this->args->dewasa;
			$anak = $this->args->anak;
			$bayi = $this->args->bayi;
			$ruteBerangkat = $this->args->ruteBerangkat;
			$ruteTujuan = $this->args->ruteTujuan;
			$ruteKembali = $this->args->ruteKembali;
			$pp = $this->args->pp;
			$tanggal_pergi = $this->args->tanggal_pergi;
			$tanggal_pulang = $this->args->tanggal_pulang;
			$seatId_pergi = $this->args->seatId_pergi;
			$flightNo_pergi = $this->args->flightNo_pergi;
			$jml_transit_pergi = $this->args->jml_transit_pergi;
			$jml_transit_pulang = $this->args->jml_transit_pulang;
			$data_search_pergi = $this->args->data_search_pergi;
			$data_search_pulang = $this->args->data_search_pulang;
			$c_route_departure = $this->args->c_route_departure;
			$c_route_arrival = $this->args->c_route_arrival;
			$data_dinput = json_decode($this->args->data_dinput,true);
			$nm_contact = $this->args->nm_contact;
			$tlp_countrycode = $this->args->tlp_countrycode;
			$tlp_arecode = $this->args->tlp_arecode;
			$tlp_contact = $this->args->tlp_contact;
			$email = $this->args->email;
			
			// print_r($data_dinput);
			if($airlines_id == 2){
				$this->args->service = "cookie-lion";
				$this->args->status_proses = 2;
				$this->endpoint = "cookie_lion";
				$this->target_server = $this->baseUrl_aws . "/" . $this->function_api;
				$response_cookie = $this->cookie_lion();
				$de_cookie = json_decode($response_cookie);
				$cookie_lion = $de_cookie->cookie;
			}
			
			if(!empty($cookie_lion)){
				$this->target_server = $this->baseUrl_aws . "/" . $this->book_flights_lion;
				$this->endpoint = "book_flight_airlines";
				$params = array(              
					"airlines_id"=>$airlines_id,
					"booking_request_dewasa"=>$dewasa,
					"booking_request_anak"=>$anak,
					"booking_request_bayi"=>$bayi,
					"booking_rute_berangkat"=>$ruteBerangkat,
					"booking_rute_tujuan"=>$ruteTujuan,
					"booking_request_pp"=>$pp,
					"cookie"=>$cookie_lion,
					"booking_request_tglpergi"=>$tanggal_pergi,
					"booking_request_tglpulang"=>$tanggal_pulang,
					"seatId_pergi"=>$seatId_pergi,
					"adultchild"=>$adultchild,
					"child"=>$child,
					"infant"=>$infant,
					"flightNo_pergi"=>$flightNo_pergi,
					"jml_transit_pergi"=>$jml_transit_pergi,
					"jml_transit_pulang"=>$jml_transit_pulang,
					"data_search_pergi"=>$data_search_pergi,
					"data_search_pulang"=>$data_search_pulang,
					"c_route_departure"=>$c_route_departure,
					"c_route_arrival"=>$c_route_arrival,
					"nm_contact"=>$nm_contact,
					"tlp_countrycode"=>$tlp_countrycode,
					"tlp_arecode"=>$tlp_arecode,
					"tlp_contact"=>$tlp_contact,
					"email"=>$email,
					"user_id"=> $this->info_->user->id,
					"username"=> $this->info_->user->name
				);
				
				for($i=0;$i<count($data_dinput);$i++){
					$j = $i+1;
					$salutation = $data_dinput[$i]['salutation'];
					$firstname = $data_dinput[$i]['firstname'];
					$lastname = $data_dinput[$i]['lastname'];
					$type_doc = $data_dinput[$i]['type_doc'];
					$docnumber = $data_dinput[$i]['docnumber'];
					$passport_expired = $data_dinput[$i]['adultchild_passport_expired'];
					$passport_issuecountry = $data_dinput[$i]['adultchild_passport_issuecountry'];
					$passport_nasionalitydocument = $data_dinput[$i]['adultchild_passport_nasionalitydocument'];
					$birthdate = $data_dinput[$i]['adultchild_birthdate'];
					$baggage_text = $data_dinput[$i]['adultchild_baggage_text'];
					
					$params2 = array(
						"dinput_gelar$j" => $salutation,
						"dinput_nm_awal$j" => $firstname,
						"dinput_nm_akhir$j" => $lastname,
						"dinput_type$j" => $type_doc,
						"dinput_gff$j" => "",
						"dinput_passport_miles$j" => "0",
						"dinput_no_dokumen$j" => $docnumber,
						"dinput_birthdate$j" => $birthdate,
						"dinput_passport_nasionalitydocument$j" => $passport_nasionalitydocument,
					);
				}
				
				$params = array_merge($params,$params2);
			}
			
			$exec = $this->filterResponse($this->execute($params));
			$response = json_encode($this->handler->processXml($this->endpoint, json_decode($exec)));
            
            $this->logger->log("\r\n\r\nPARAMS: {$this->args->service}\r\n\r\n" . print_r($args, true));
            $this->logger->log("\r\n\r\nRESPONSE:\r\n\r\n". print_r($response, true));
            
            return $response;
            
        } catch (Exception $e) {
            throw $e;
        }
    }
	
	protected function issued_airlines() {
        try {
            API_Helper::validateForms($this->args, $this->config->PARAM_ISSUED_AIRLINES);
			
			$airlines_id = $this->args->airlines_id;
			$kode_booking = $this->args->kode_booking;
			
			if($airlines_id == 2){
				$this->args->service = "cookie-lion";
				$this->args->status_proses = 3;
				$this->endpoint = "cookie_lion";
				$this->target_server = $this->baseUrl_aws . "/" . $this->function_api;
				$response_cookie = $this->cookie_lion();
				$de_cookie = json_decode($response_cookie);
				$cookie_lion = $de_cookie->cookie;
				$cons_id = $de_cookie->cons_id;
			}
			
			if(!empty($cookie_lion)){
				$this->target_server = $this->baseUrl_aws . "/" . $this->issued_lion;
				$this->endpoint = "issued_airlines";
				$params = array(              
					"airlines_id"=>$airlines_id,
					"kode_booking"=>$kode_booking,
					"cookie"=>$cookie_lion,
					"cons_id"=>$cons_id,
					"username"=> $this->info_->user->name
				);
			}
			
			$exec = $this->filterResponse($this->execute($params));
			$response = json_encode($this->handler->processXml($this->endpoint, json_decode($exec)));
            
            $this->logger->log("\r\n\r\nPARAMS: {$this->args->service}\r\n\r\n" . print_r($args, true));
            $this->logger->log("\r\n\r\nRESPONSE:\r\n\r\n". print_r($response, true));
            
            return $response;
            
        } catch (Exception $e) {
            throw $e;
        }
    }
	
	protected function airlines_search() {
        try {
            API_Helper::validateForms($this->args, $this->config->PARAM_AIRLINES_SEARCH);
			
            $args = array(
                "ruteBerangkat"=> $this->args->origin,
                "ruteTujuan"=> $this->args->destination,
                "tanggal_pergi"=> $this->args->depdate,
                "dewasa"=> $this->args->adult,
                "input_pergi"=> $this->args->input_pergi,
                "airlines"=> $this->args->airline,
                "random"=> rand(),
                "ruteKembali"=> $this->args->ruteKembali,
                "tanggal_pulang"=> $this->args->retdate,
                "anak"=> $this->args->child,
                "bayi"=> $this->args->infant,
				"username"=> $this->info_->user->name,
            );
			
			$this->target_server = $this->baseUrl . "/" . $this->search_flight_garuda;
			$args_ext = array("operation" => "garuda_api","type_garuda" => "eco");
			$args = array_merge($args, $args_ext);
			
            $exec = $this->filterResponse($this->execute($args));
            $response = json_encode($this->handler->processXml($this->endpoint, json_decode($exec)));
            
            $this->logger->log("\r\n\r\nPARAMS: {$this->args->service}\r\n\r\n" . print_r($args, true));
            $this->logger->log("\r\n\r\nRESPONSE SRO: \r\n\r\n" . $response);
            
            return $response;
        } catch (Exception $e) {
            throw $e;
        }
    }
	
	protected function exchange_rate() {
        try {
            API_Helper::validateForms($this->args, $this->config->PARAM_EXCHANGE_RATE);
            
            $args["function"] = "api.exchange.rate.list";
            $args["input"] = array(
				"sord"=> "desc",
				"sidx"=> "effective_date"
            );
            
            $args = json_encode($args);
            
            $exec = $this->filterResponse($this->execute(array("r"=>$args)));
            $response = json_encode($this->handler->processXml($this->endpoint, json_decode($exec)));
            
            $this->logger->log("\r\n\r\nPARAMS: {$this->args->service}\r\n\r\n" . print_r($args, true));
            $this->logger->log("\r\n\r\nRESPONSE SRO: \r\n\r\n" . $response);
            
            return $response;
        } catch (Exception $e) {
            throw $e;
        }
    }
	
}