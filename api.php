<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);

require_once("myapi.php");
//  Requests from the same server don't have a HTTP_ORIGIN header
if (!array_key_exists('HTTP_ORIGIN', filter_input_array(INPUT_SERVER))) {
    $_SERVER["HTTP_ORIGIN"] = filter_input(INPUT_SERVER, "SERVER_NAME");
}

try {    
//    file_put_contents("/var/www/html/sro/api/response.log", print_r(filter_input_array(INPUT_REQUEST), true));    
    
    $API = new MyAPI(MyAPI::request(), filter_input(INPUT_SERVER, "HTTP_ORIGIN")); 
    echo $API->processAPI();
    
} catch (Exception $e) {
    echo json_encode(APIException::_response(
            array(
                "code"=>$e->getCode(),
                "message"=>$e->getMessage(),
            )
        ));
}
