<?php
define("ROOT_PATH", dirname(__DIR__) . "/");
define("LIB_PATH", ROOT_PATH . "lib/");
define("SERVICES_PATH", ROOT_PATH . "services/");

require_once(SERVICES_PATH . "Db.class.php");
require_once(LIB_PATH . "APILogger.php");
require_once(LIB_PATH . "params.config.php");
require_once(LIB_PATH . "APIException.php");
require_once(LIB_PATH . "APIHelper.php");