<?php
// lib

// Convert from dd/mm/yyyy to yyyy-mm-dd
function convert_date($date){
	$exp =explode('/',$date);
	$date = "$exp[2]-$exp[1]-$exp[0]";
	return $date;
}

function convert($str){
	$str=str_replace(",","",$str);
	$pos=strpos($str, '.');
	if($pos){
		$str=substr($str,0,$pos);
	}else{
		$pos=strpos($str, ' IDR');
		$str=substr($str,0,$pos);
	}	
	return $str;
}

function extractKey($s, $keyword1, $keyword2) {
	// Mengambil string diantara dua keyword
	$l1=strlen($keyword1);
	$x1=strpos($s, $keyword1);
	$x2=strpos($s, $keyword2); 
	$l=$x2-($x1+$l1);
	return substr($s, $x1+$l1, $l); 
 } 

function cleanScripts($book){
 $z=0;
 do {
	$x1=strpos($book, '<script', $z);
	if ($x1) {
		$scriptFound=true; 
		$x2=strpos($book, '</script>', $x1+1);
		$book_p1=substr($book, 0, $x1-1);
		$book_p2=substr($book, $x2+9);
		$book=$book_p1.$book_p2;
		$z=$x1;
	} else {
		$scriptFound=false;
	}
 } while ($scriptFound);
	return $book;
}


function extractBetweenKeyword($s, $keyword1, $keyword2) {
	// Mengambil string diantara dua keyword
	$l1=strlen($keyword1);
	$x1=strpos($s, $keyword1);
	$x2=strpos($s, $keyword2, $x1+$l1); 
	$l=$x2-($x1+$l1);
	return substr($s, $x1+$l1, $l); 
 }
 
function getBetweenTag($s, $tagName) {
	// Mengambil string diantara start tag dan end tag, tapi cari endtag mulai dari akhir string
	$x1=strpos($s, '<'.$tagName);
	$x1_a=strpos($s, '>', $x1);
	$x2=strrpos($s, '</'.$tagName.'>');
	$l=$x2-($x1_a+1);
	return substr($s, $x1_a+1, $l);
}

function getBetweenTagForward($s, $tagName) {
	// Mengambil string diantara start tag dan end tag, tapi cari endtag maju dari start tag, bukan dari akhir string seperti getBetweenTag
	$x1=strpos($s, '<'.$tagName);
	$x1_a=strpos($s, '>', $x1);
	$x2=strpos($s, '</'.$tagName.'>', $x1_a+1);
	$l=$x2-($x1_a+1);
	return substr($s, $x1_a+1, $l);
}

function getAfterKeyword($s, $keyword) {
	//mengambil seluruh setelah keyword yg ditentukan
	$l=strlen($keyword);
	$x1=strpos($s, $keyword);
	return substr($s, $x1+$l);
}

function getBeforeKeyword($s, $keyword) {
	$x1=strpos($s, $keyword);
	return substr($s, 0, $x1);
}

function getBodyTag($s) {
	$s=getAfterKeyword($s,'</head>');
	$s=getBeforeKeyword($s, '</html>');
	return $s;
}

function getHTML_nocookie($url){
	global $logger;
  $c = curl_init();
	curl_setopt($c, CURLOPT_AUTOREFERER, 1);
	curl_setopt($c, CURLOPT_HTTPGET, 1);
	curl_setopt($c, CURLINFO_HEADER_OUT,1);
	curl_setopt($c, CURLOPT_VERBOSE, 1); 
	curl_setopt($c, CURLOPT_HEADER, 1); 
	curl_setopt($c, CURLOPT_RETURNTRANSFER, 1); 
	curl_setopt($c, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']); 
	curl_setopt($c, CURLOPT_URL, $url);
	$logger->log("GET HTML $url");
	$hasil = curl_exec($c);
	$logger->logfile($hasil);
	curl_close($c);
	return $hasil;
}

function getHTML_cookie($url,$cookie=null){
	$c = curl_init();
	curl_setopt($c, CURLOPT_COOKIE, 'PHPSESSID='.$cookie); 
	curl_setopt($c, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($c, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);	
	curl_setopt($c, CURLOPT_URL, $url); 
	#curl_setopt($c, CURLOPT_URL, "http://web.batavia-air.co.id/MyPage/booking/keterangan_printInt.php?kdbooking=$issueCode"); 
	$hasil = curl_exec($c);
	curl_close($c);
	return $hasil;
}

function postHTML($url,$cookie,$serverid){
	global $logger;
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_HEADER, true);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
	curl_setopt($ch, CURLOPT_COOKIE, "ASP.NET_SessionId=$cookie; SERVERID=$serverid");
	curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
	curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']); 
	$data = curl_exec($ch);
	$data = html_entity_decode($data, ENT_NOQUOTES, "UTF-8");
	$data = iconv('UTF-8', 'US-ASCII//TRANSLIT', $data); 
	$logger->logfile($data);
	curl_close($ch); 
	return $data;      	 
}

function postHTMLSearch($url,$cookie,$serverid){
	global $logger;
	$ch = curl_init();
	// curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_HEADER, true);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
	curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
	curl_setopt($ch, CURLOPT_COOKIE, "ASP.NET_SessionId=$cookie; SERVERID=$serverid");
	curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']); 
	$data = curl_exec($ch);
	$data = html_entity_decode($data, ENT_NOQUOTES, "UTF-8");
	$data = iconv('UTF-8', 'US-ASCII//TRANSLIT', $data); 
	$logger->logfile($data);
	curl_close($ch); 
	return $data;      	 
}

function postHTML2($url,$referer,$cookie,$serverid){
	global $logger;
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_HEADER, true);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
	curl_setopt($ch, CURLOPT_COOKIE, "ASP.NET_SessionId=$cookie; SERVERID=$serverid"); 
	curl_setopt($ch, CURLOPT_REFERER, $referer);
	$data = curl_exec($ch);
	$data = html_entity_decode($data, ENT_NOQUOTES, "UTF-8");
	$data = iconv('UTF-8', 'US-ASCII//TRANSLIT', $data); 
	$logger->logfile($data);
	curl_close($ch); 
	return $data;      	 
}

function postHTML_noResponseHeader($url, $referer=null, $cookie=null, $postfields=null){
	global $logger;
  $c = curl_init();
	curl_setopt($c, CURLOPT_RETURNTRANSFER, true); 
  curl_setopt($c, CURLOPT_FOLLOWLOCATION, true); 
	curl_setopt($c, CURLOPT_MAXREDIRS, 2);
	if ($cookie) curl_setopt($c, CURLOPT_COOKIE, 'PHPSESSID='.$cookie); 
	if ($referer) curl_setopt($c, CURLOPT_REFERER, $referer);
	curl_setopt($c, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']); 
	curl_setopt($c, CURLOPT_URL, $url);
	if ($postfields) curl_setopt($c, CURLOPT_POST, 1);
	if ($postfields) curl_setopt($c, CURLOPT_POSTFIELDS, $postfields);
	$hasil = curl_exec($c);
	$logger->logfile($hasil);
	curl_close($c);
	return $hasil;      	 
}

class Logger{
	public $requestId=null;
	public $sequenceNumber=0;
	public $folderName='temp';
	public $username=null;
	public $activity=null;
	public function __construct($requestId=null, $sequenceNumber=null,$username,$activity) {
		if (!$requestId) {
			$this->requestId=qxc\System::NowAsISOFilenameFrendly().'_'.qxc\System::UUID()->string;
		} else {
			$this->requestId=$requestId;
			if ($sequenceNumber) {
				$this->sequenceNumber=$sequenceNumber;
			} else {
				$this->sequenceNumber=0;
			}
		}
		
		$this->username = $username; // adiputra
		$this->activity = $activity; // adiputra
	}
	public function logfile($content) {
		$this->lastfilename=$this->folderName.'/'.$this->requestId.'_'.$this->sequenceNumber.'_'.$this->activity.'_'.$this->username.'.xml';
		file_put_contents($this->lastfilename, $content);
		$this->log("Saving content to $this->lastfilename");
		$this->sequenceNumber=$this->sequenceNumber+1;
	}
	public function log($text) {
		file_put_contents($this->folderName.'/'.$this->requestId.'_'.$this->activity.'_'.$this->username.'.log', $text."\r\n", FILE_APPEND);
	}	
}

// $logger=new Logger();
