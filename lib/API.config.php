<?php
class API_Config {
    public $PARAM_AUTH_LOGIN = array(
        "service" => array('type'=>'string','required'=>true),
        "username"=> array('type'=>'string','required'=>true),
//        "apikey"=> array('type'=>'string','required'=>true),
//        "password"=> array('type'=>'string','required'=>true)
    );
    
    /***************
     * International
     **************/
    public $PARAM_SEARCH = array(
                "service" => array('type'=>'string','required'=>true),
                "username"=> array('type'=>'string','required'=>true),
                "apikey"=> array('type'=>'string','required'=>true),
                "origin"=> array('type'=>'string','required'=>true,'length'=>3),
                "destination"=> array('type'=>'string','required'=>true,'length'=>3),
                "depdate"=> array('type'=>'string','required'=>true,'format'=>array('date'=>"Y-m-d")),
                "retdate"=> array('type'=>'string','required'=>false,'format'=>array('date'=>"Y-m-d")),
//                "airline"=> array('type'=>'string','required'=>true,'length'=>2),
                "adult"=> array('type'=>'string','required'=>true),
                "child"=> array('type'=>'string','required'=>false),
                "infant"=> array('type'=>'string','required'=>false),
//                "airline_all"=> array('type'=>'bool','required'=>true)
            );
    
    public $PARAM_BOOK = array(
                "service" => array('type'=>'string','required'=>true),
                "username"=> array('type'=>'string','required'=>true),
                "apikey"=> array('type'=>'string','required'=>true),
                "seatid_dep"=> array('type'=>'array','required'=>false,'format'=>array('seatid'=>'string')),
                "seatid_ret"=> array('type'=>'array','required'=>false,'format'=>array('seatid'=>'string')),
                "total_basic"=> array('type'=>'string','required'=>false),
                "total_tax"=> array('type'=>'string','required'=>false),
                "total_pay"=> array('type'=>'string','required'=>false),
                "origin"=> array('type'=>'string','required'=>true,'length'=>3),
                "destination"=> array('type'=>'string','required'=>true,'length'=>3),
                "depdate"=> array('type'=>'string','required'=>true,'format'=>array('date'=>"Y-m-d")),
                "retdate"=> array('type'=>'string','required'=>false,'format'=>array('date'=>"Y-m-d")),
                "airline"=> array('type'=>'string','required'=>true,'length'=>2),
                "adult"=> array('type'=>'string','required'=>true),
                "child"=> array('type'=>'string','required'=>false),
                "infant"=> array('type'=>'string','required'=>false),
                "adultchilds"=> array('type'=>'arrayobject','required'=>true),
                "childs"=> array('type'=>'arrayobject','required'=>false),
                "infants"=> array('type'=>'arrayobject','required'=>false),
                "nm_contact"=> array('type'=>'string','required'=>true),
                "tlp_countrycode"=> array('type'=>'string','required'=>false),
                "tlp_contact"=> array('type'=>'string','required'=>true),
                "email"=> array('type'=>'string','required'=>false,'format'=>array('email'=>true)),
                "endpoint"=> array('type'=>'string','required'=>false),
                "ws_local"=> array('type'=>'string','required'=>false)
            );
    
    public $PARAM_ISSUE = array(
                "service" => array('type'=>'string','required'=>true),
                "username"=> array('type'=>'string','required'=>true),
                "apikey"=> array('type'=>'string','required'=>true),                
                "reservation_id"=> array('type'=>'string','required'=>true),
                "password"=> array('type'=>'string','required'=>true)
            );
    
    public $PARAM_INTERNATIONAL_CANCEL = array(
        "service" => array('type'=>'string','required'=>true),                
        "username"=> array('type'=>'string','required'=>true),
        "apikey"=> array('type'=>'string','required'=>true),
        "reservation_id"=> array('type'=>'string','required'=>true)        
    );

    public $PARAM_CHECKBALANCE = array(
        "service" => array('type'=>'string','required'=>true),                
        "username"=> array('type'=>'string','required'=>true),
        "apikey"=> array('type'=>'string','required'=>true)                
    );
    
    public $PARAM_READPNR = array(
        "service" => array('type'=>'string','required'=>true),                
        "username"=> array('type'=>'string','required'=>true),
        "apikey"=> array('type'=>'string','required'=>true),
        "reservation_id"=> array('type'=>'string','required'=>true)
    );
    
    public $PARAM_SENT_EMAIL = array(
        "service" => array('type'=>'string','required'=>true),                
        "username"=> array('type'=>'string','required'=>true),
        "apikey"=> array('type'=>'string','required'=>true),
        "reservation_id"=> array('type'=>'string','required'=>true)
    );
    
    public $PARAM_BOOK_LIST = array(
        "service" => array('type'=>'string','required'=>true),                
        "username"=> array('type'=>'string','required'=>true),
        "apikey"=> array('type'=>'string','required'=>true),
        "airline_code"=> array('type'=>'string','required'=>false),
        "contact_name"=> array('type'=>'string','required'=>false),
        "pnr"=> array('type'=>'string','required'=>false),
        "depdate"=> array('type'=>'string','required'=>false),
        "arrdate"=> array('type'=>'string','required'=>false),
        "bookby"=> array('type'=>'string','required'=>false),
        "status"=> array('type'=>'string','required'=>false),
        "page"=> array('type'=>'string','required'=>false)
    );        
    
    /*****************************
     * Hotel
     ****************************/
    
    public $PARAM_HOTEL_SEARCH = array(
        "service" => array('type'=>'string','required'=>true),
        "username"=> array('type'=>'string','required'=>true),
        "apikey"=> array('type'=>'string','required'=>true),
        "hotel_id"=> array('type'=>'string','required'=>true),
        "hotel_name"=> array('type'=>'string','required'=>true),
        "night"=> array('type'=>'string','required'=>true),
        "page"=> array('type'=>'string','required'=>true),
        "room"=> array('type'=>'string','required'=>true),
        "room_detail"=> array('type'=>'arrayobject','required'=>true),
        "passport_nationality"=> array('type'=>'string','required'=>true),                
        "checkin"=> array('type'=>'string','required'=>true),
        "checkout"=> array('type'=>'string','required'=>true),
        "adult"=> array('type'=>'string','required'=>true),
        "child"=> array('type'=>'string','required'=>false),
    );
    
    public $PARAM_HOTEL_DETAILS = array(
        "service" => array('type'=>'string','required'=>true),
        "username"=> array('type'=>'string','required'=>true),
        "apikey"=> array('type'=>'string','required'=>true),
        "hotel_id"=> array('type'=>'string','required'=>true)        
    );
    
    public $PARAM_HOTEL_DETAIL_ROOMS = array(
        "service" => array('type'=>'string','required'=>true),
        "username"=> array('type'=>'string','required'=>true),
        "apikey"=> array('type'=>'string','required'=>true),
        "hotel_id"=> array('type'=>'string','required'=>true),
        "hotel_id_db"=> array('type'=>'string','required'=>true),
        "internal_code"=> array('type'=>'string','required'=>false),
        "checkin"=> array('type'=>'string','required'=>true),
        "checkout"=> array('type'=>'string','required'=>true),
        "adult"=> array('type'=>'string','required'=>true),
        "child"=> array('type'=>'string','required'=>false),
        "room"=> array('type'=>'string','required'=>true),
        "passport_nationality"=> array('type'=>'string','required'=>true),
        "dest_country"=> array('type'=>'string','required'=>true),
        "dest_city"=> array('type'=>'string','required'=>true),        
        "room_detail"=> array('type'=>'','required'=>true)        
    );
    
    public $PARAM_HOTEL_GALLERY_IMAGES = array(
        "service" => array('type'=>'string','required'=>true),
        "username"=> array('type'=>'string','required'=>true),
        "apikey"=> array('type'=>'string','required'=>true),
        "image_id"=> array('type'=>'string','required'=>true)
              
    );
    
    public $PARAM_HOTEL_CANCEL_POLICY = array(
        "service" => array('type'=>'string','required'=>true),
        "username"=> array('type'=>'string','required'=>true),
        "apikey"=> array('type'=>'string','required'=>true),
        "id"=> array('type'=>'string','required'=>true),
        "room"=> array('type'=>'string','required'=>true),
        "room_detail"=> array('type'=>'','required'=>true),
        "passport_nationality"=> array('type'=>'string','required'=>true),
        "checkin"=> array('type'=>'string','required'=>true),
        "checkout"=> array('type'=>'string','required'=>true),
        "adult"=> array('type'=>'string','required'=>true),
        "child"=> array('type'=>'string','required'=>false),
        "avail"=> array('type'=>'string','required'=>true),
        "internal_code"=> array('type'=>'string','required'=>false),
        "cancel_policy_id"=> array('type'=>'string','required'=>false),
    );
    
    public $PARAM_HOTEL_BOOK = array(
        "service" => array('type'=>'string','required'=>true),
        "username"=> array('type'=>'string','required'=>true),
        "apikey"=> array('type'=>'string','required'=>true),
        "booking_user_id"=> array('type'=>'string','required'=>true),
        "contact_name"=> array('type'=>'string','required'=>true),
        "contact_telp"=> array('type'=>'string','required'=>true),
        "hotel_id"=> array('type'=>'string','required'=>true),
        "hotel_id_db"=> array('type'=>'string','required'=>true),
        "hotel_name"=> array('type'=>'string','required'=>true),
        "internal_code"=> array('type'=>'string','required'=>false),
        "dest_country"=> array('type'=>'string','required'=>true),        
        "dest_city"=> array('type'=>'string','required'=>true),        
        "passport_nationality"=> array('type'=>'string','required'=>true),        
        "room_type"=> array('type'=>'string','required'=>true),        
        "room_name"=> array('type'=>'string','required'=>true),        
        "room_id"=> array('type'=>'string','required'=>true),        
        "room_detail"=> array('type'=>'','required'=>true),        
        "inc_abf"=> array('type'=>'string','required'=>true),        
        "price_total"=> array('type'=>'string','required'=>true),        
        "price_publish"=> array('type'=>'string','required'=>true),        
        "price_travflex"=> array('type'=>'string','required'=>true),        
        "room_qty"=> array('type'=>'string','required'=>true),        
        "checkin"=> array('type'=>'string','required'=>true),        
        "checkout"=> array('type'=>'string','required'=>true),                
        "email"=> array('type'=>'string','required'=>true),                
        "note_request"=> array('type'=>'string','required'=>false),
        "adultchilds"=> array('type'=>'','required'=>true),
        "childs"=> array('type'=>'','required'=>false)
    );
    
    public $PARAM_HOTEL_CHECK_SUGGESTION = array(
        "service" => array('type'=>'string','required'=>true),
        "username"=> array('type'=>'string','required'=>true),
        "apikey"=> array('type'=>'string','required'=>true),
        "search_type"=> array('type'=>'string','required'=>true),
        "keyword"=> array('type'=>'string','required'=>true)
    );
	
	public $PARAM_CREATE_USERS = array(
        "service" => array('type'=>'string','required'=>true),
        "username"=> array('type'=>'string','required'=>false),
        "apikey"=> array('type'=>'string','required'=>false),
        "user_fullname"=> array('type'=>'string','required'=>true),
        "user_address"=> array('type'=>'string','required'=>true),
        "user_phone"=> array('type'=>'string','required'=>true),
        "user_email"=> array('type'=>'string','required'=>true)
    );
	
	public $PARAM_REGIS_BILLING = array(
        "service" => array('type'=>'string','required'=>true),
        "username"=> array('type'=>'string','required'=>false),
        "apikey"=> array('type'=>'string','required'=>false),
        "fullname"=> array('type'=>'string','required'=>true),
        "tlp"=> array('type'=>'string','required'=>true),
        "email"=> array('type'=>'string','required'=>true),
        "ktp"=> array('type'=>'string','required'=>false),
        "param_hash"=> array('type'=>'string','required'=>false),
        "source"=> array('type'=>'string','required'=>false)
    );
	
	public $PARAM_REGIS_BILLING_AKTIVASI = array(
        "service" => array('type'=>'string','required'=>true),
        "username"=> array('type'=>'string','required'=>false),
        "apikey"=> array('type'=>'string','required'=>false),
        "param_hash"=> array('type'=>'string','required'=>true)
    );
	public $PARAM_REGIS_BILLING_READ = array(
        "service" => array('type'=>'string','required'=>true),
        "username"=> array('type'=>'string','required'=>false),
        "apikey"=> array('type'=>'string','required'=>false),
        "regis_id"=> array('type'=>'string','required'=>false),
        "param_hash"=> array('type'=>'string','required'=>false)
    );
	
	public $PARAM_FORGET_PASSWORD = array(
        "service" => array('type'=>'string','required'=>true),
        "username"=> array('type'=>'string','required'=>false),
        "apikey"=> array('type'=>'string','required'=>false),
        "user_name"=> array('type'=>'string','required'=>true),
        "email"=> array('type'=>'string','required'=>true)
    );
	
	public $PARAM_CONFIRM_OTP_VERIFY = array(
        "service" => array('type'=>'string','required'=>true),
        "username"=> array('type'=>'string','required'=>false),
        "apikey"=> array('type'=>'string','required'=>false),
        "qparam"=> array('type'=>'string','required'=>true),
        "new_password"=> array('type'=>'string','required'=>true),
        "code_otp"=> array('type'=>'string','required'=>false)
    );
    
    public $PARAM_HOTEL_READPNR = array(
        "service" => array('type'=>'string','required'=>true),
        "username"=> array('type'=>'string','required'=>true),
        "apikey"=> array('type'=>'string','required'=>true),
        "reservation_id"=> array('type'=>'string','required'=>true)        
    );
    
    public $PARAM_HOTEL_VOUCHERLIST = array(
        "service" => array('type'=>'string','required'=>true),
        "username"=> array('type'=>'string','required'=>true),
        "apikey"=> array('type'=>'string','required'=>true)
    );
    
    /*****************************/
    
    /////// KERETA \\\\\\\\\
    
    public $PARAM_TRAIN_SEARCH = array(
        "service" => array('type'=>'string','required'=>true),
        "username"=> array('type'=>'string','required'=>true),
        "apikey"=> array('type'=>'string','required'=>true),
        "origin"=> array('type'=>'string','required'=>true), //array("min"=>2,"max"=>3)
        "destination"=> array('type'=>'string','required'=>true),
        "depdate"=> array('type'=>'string','required'=>true,'format'=>array('date'=>"Y-m-d")),        
        "adult"=> array('type'=>'string','required'=>true),
        "child"=> array('type'=>'string','required'=>false),
        "infant"=> array('type'=>'string','required'=>false)        
    );
    
    public $PARAM_TRAIN_BOOK = array(
        "service" => array('type'=>'string','required'=>true),
        "username"=> array('type'=>'string','required'=>true),
        "apikey"=> array('type'=>'string','required'=>true),
        "origin"=> array('type'=>'string','required'=>true),
        "destination"=> array('type'=>'string','required'=>true),
        "depdate"=> array('type'=>'string','required'=>true),        
        "adult"=> array('type'=>'string','required'=>true),
        "child"=> array('type'=>'string','required'=>false),
        "infant"=> array('type'=>'string','required'=>false),        
        "cp_name"=> array('type'=>'string','required'=>true),        
        "cp_phone"=> array('type'=>'string','required'=>true),        
//        "cp_email"=> array('type'=>'string','required'=>true)        
    );
    
    public $PARAM_TRAIN_SEATMAP = array(
        "service" => array('type'=>'string','required'=>true),
        "username"=> array('type'=>'string','required'=>true),
        "apikey"=> array('type'=>'string','required'=>true),
        "origin"=> array('type'=>'string','required'=>true),
        "destination"=> array('type'=>'string','required'=>true),
        "depdate"=> array('type'=>'string','required'=>true),
        "train_no"=> array('type'=>'string','required'=>true),
        "subclass"=> array('type'=>'string','required'=>true),
        "depdate"=> array('type'=>'string','required'=>true)
    );
	
	public $PARAM_TRAIN_TAKESEAT = array(
        "service" => array('type'=>'string','required'=>true),
        "username"=> array('type'=>'string','required'=>true),
        "apikey"=> array('type'=>'string','required'=>true),
        "reservation_id"=> array('type'=>'string','required'=>true),
        "book_code"=> array('type'=>'string','required'=>false),
        "booking_date"=> array('type'=>'string','required'=>false),
        "take_seat"=> array('type'=>'arrayobject','required'=>true)
    );
    
    public $PARAM_TRAIN_CANCELBOOK = array(
        "service" => array('type'=>'string','required'=>true),
        "username"=> array('type'=>'string','required'=>true),
        "apikey"=> array('type'=>'string','required'=>true),
        "reservation_id"=> array('type'=>'string','required'=>true),
        "pnr"=> array('type'=>'string','required'=>true)
    );
    
    public $PARAM_TRAIN_RSVLIST = array(
        "service" => array('type'=>'string','required'=>true),
        "username"=> array('type'=>'string','required'=>true),
        "apikey"=> array('type'=>'string','required'=>true)        
    );
    
    public $PARAM_DEPOSIT_TOPUP_REQUEST = array(
        "service" => array('type'=>'string','required'=>true),
        "username"=> array('type'=>'string','required'=>true),
        "apikey"=> array('type'=>'string','required'=>true),
        "payment"=> array('type'=>'string','required'=>true),
        "value"=> array('type'=>'string','required'=>true),
        "bank_id"=> array('type'=>'string','required'=>true),
        "bank_tenor"=> array('type'=>'string','required'=>true),
        "currency_label"=> array('type'=>'string','required'=>true)
    );
    
    public $PARAM_DEPOSIT_TOPUP_LIST = array(
        "service" => array('type'=>'string','required'=>true),
        "username"=> array('type'=>'string','required'=>true),
        "apikey"=> array('type'=>'string','required'=>true),
        "book_code"=> array('type'=>'string','required'=>false),
        "contact_name"=> array('type'=>'string','required'=>false),
        "contact_tlp"=> array('type'=>'string','required'=>false),
        "booking_datetime"=> array('type'=>'string','required'=>false),
        "issue_datetime"=> array('type'=>'string','required'=>false)
    );
    
    public $PARAM_DEPOSIT_TOPUP_CONFIRM = array(
        "service" => array('type'=>'string','required'=>true),
        "username"=> array('type'=>'string','required'=>true),
        "apikey"=> array('type'=>'string','required'=>true),
        "topup_id"=> array('type'=>'string','required'=>true)
    );
    
    public $PARAM_DEPOSIT_TOPUP_PAYMENT_READ = array(
        "service" => array('type'=>'string','required'=>true),
        "username"=> array('type'=>'string','required'=>true),
        "apikey"=> array('type'=>'string','required'=>true),
        "topup_id"=> array('type'=>'string','required'=>true)
    );
    
    public $PARAM_PAYMENT_MOCASHBRI = array(
        "service" => array('type'=>'string','required'=>true),
        "username"=> array('type'=>'string','required'=>true),
        "apikey"=> array('type'=>'string','required'=>true),
        "topup_id"=> array('type'=>'string','required'=>true),
        
    );
    
    public $PARAM_PAYMENT_PPOB_INQUIRY = array(
        "service" => array('type'=>'string','required'=>true),
        "username"=> array('type'=>'string','required'=>true),
        "apikey"=> array('type'=>'string','required'=>true),
        "product_code"=> array('type'=>'string','required'=>true),
        "customer_id"=> array('type'=>'string','required'=>false)        
    );
    
    public $PARAM_PAYMENT_PPOB_ISSUE = array(
        "service" => array('type'=>'string','required'=>true),
        "username"=> array('type'=>'string','required'=>true),
        "apikey"=> array('type'=>'string','required'=>true),
        "payment_id"=> array('type'=>'string','required'=>true),
        "customer_id"=> array('type'=>'string','required'=>false)        
    );
    public $PARAM_PAYMENT_PPOB_READ = array(
        "service" => array('type'=>'string','required'=>true),
        "username"=> array('type'=>'string','required'=>true),
        "apikey"=> array('type'=>'string','required'=>true),
        "payment_id"=> array('type'=>'string','required'=>true)        
    );
    public $PARAM_PAYMENT_PPOB_CANCEL = array(
        "service" => array('type'=>'string','required'=>true),
        "username"=> array('type'=>'string','required'=>true),
        "apikey"=> array('type'=>'string','required'=>true),
        "payment_id"=> array('type'=>'string','required'=>true)
    );
    public $PARAM_PAYMENT_PPOB_CHECKSTATUS = array(
        "service" => array('type'=>'string','required'=>true),
        "username"=> array('type'=>'string','required'=>true),
        "apikey"=> array('type'=>'string','required'=>true),
        "payment_id"=> array('type'=>'string','required'=>true),
        "customer_id"=> array('type'=>'string','required'=>false),
        "product_code"=> array('type'=>'string','required'=>true),
        "subproduct_code"=> array('type'=>'string','required'=>true),
        "payment_status"=> array('type'=>'string','required'=>false)
    );
    
    public $PARAM_PAYMENT_PPOB_LIST = array(
        "service" => array('type'=>'string','required'=>true),
        "username"=> array('type'=>'string','required'=>true),
        "apikey"=> array('type'=>'string','required'=>true),
        "payment_id"=> array('type'=>'string','required'=>false),
        "payment_type"=> array('type'=>'string','required'=>false),
        "customer_id"=> array('type'=>'string','required'=>false),
        "status"=> array('type'=>'string','required'=>false),
        "page"=> array('type'=>'string','required'=>false)
    );
    
    public $PARAM_PAYMENT_CONTENT_VIEW = array(
        "service" => array('type'=>'string','required'=>true),
        "username"=> array('type'=>'string','required'=>true),
        "apikey"=> array('type'=>'string','required'=>true),
        "category"=> array('type'=>'string','required'=>true)
    );
    
    public $PARAM_PAYMENT_CONTENT_VIEW_READ = array(
        "service" => array('type'=>'string','required'=>true),
        "username"=> array('type'=>'string','required'=>true),
        "apikey"=> array('type'=>'string','required'=>true),
        "category"=> array('type'=>'string','required'=>true)
    );
    
    public $PARAM_USER_CHANGE_PASSWORD = array(
        "service" => array('type'=>'string','required'=>true),
        "username"=> array('type'=>'string','required'=>true),
        "apikey"=> array('type'=>'string','required'=>true),
        "current_password"=> array('type'=>'string','required'=>true),
        "new_password"=> array('type'=>'string','required'=>true)
    );
	
	public $PARAM_SHUTTLE_BUS_CANCEL = array(
        "service" => array('type'=>'string','required'=>true),
        "username"=> array('type'=>'string','required'=>true),
        "apikey"=> array('type'=>'string','required'=>true),
        "reservation_id"=> array('type'=>'string','required'=>true)
    );
    
    public $PARAM_SHUTTLE_BUS_SEARCH = array(
        "service" => array('type'=>'string','required'=>true),
        "username"=> array('type'=>'string','required'=>true),
        "apikey"=> array('type'=>'string','required'=>true),        
        "operator"=> array('type'=>'string','required'=>true),
        "origin"=> array('type'=>'string','required'=>true),
        "destination"=> array('type'=>'string','required'=>true),
        "depdate"=> array('type'=>'string','required'=>true),
        "pax"=> array('type'=>'string','required'=>true)
    );
    
    public $PARAM_SHUTTLE_BUS_READ = array(
        "service" => array('type'=>'string','required'=>true),
        "username"=> array('type'=>'string','required'=>true),
        "apikey"=> array('type'=>'string','required'=>true),        
        "reservation_id"=> array('type'=>'string','required'=>true)
    );
    
    public $PARAM_SHUTTLE_BUS_SEAT = array(
        "service" => array('type'=>'string','required'=>true),
        "username"=> array('type'=>'string','required'=>true),
        "apikey"=> array('type'=>'string','required'=>true),        
        "operator"=> array('type'=>'string','required'=>true),
        "route_code"=> array('type'=>'string','required'=>true),
        "depdate"=> array('type'=>'string','required'=>true)        
    );
    
    public $PARAM_SHUTTLE_BUS_ISSUE = array(
        "service" => array('type'=>'string','required'=>true),
        "username"=> array('type'=>'string','required'=>true),
        "apikey"=> array('type'=>'string','required'=>true),        
        "reservation_id"=> array('type'=>'string','required'=>true)        
    );
    
    public $PARAM_SHUTTLE_BUS_LIST = array(
        "service" => array('type'=>'string','required'=>true),
        "username"=> array('type'=>'string','required'=>true),
        "apikey"=> array('type'=>'string','required'=>true)        
    );
    
    public $PARAM_REPORT_TRSC_LIST = array(
        "service" => array('type'=>'string','required'=>true),
        "username"=> array('type'=>'string','required'=>true),
        "apikey"=> array('type'=>'string','required'=>true),        
        "begindate"=> array('type'=>'string','required'=>false),
        "enddate"=> array('type'=>'string','required'=>false)        
    );
    
    public $PARAM_SEND_TICKET = array(
        "service" => array('type'=>'string','required'=>true),
        "username"=> array('type'=>'string','required'=>true),
        "apikey"=> array('type'=>'string','required'=>true),        
        "reservation_id"=> array('type'=>'string','required'=>true),
        "email"=> array('type'=>'string','required'=>true)
    );
    
    public $PARAM_UMRAH_SCHEDULE_LIST = array(
        "service" => array('type'=>'string','required'=>true),
        "username"=> array('type'=>'string','required'=>false),
        "apikey"=> array('type'=>'string','required'=>false),        
        "list"=> array('type'=>'string','required'=>true)
    );
	
	public $PARAM_UMRAH_REGISTRATION_LIST = array(
        "service" => array('type'=>'string','required'=>true),
        "username"=> array('type'=>'string','required'=>true),
        "apikey"=> array('type'=>'string','required'=>true),        
        "no_ktp"=> array('type'=>'string','required'=>false),        
        "status"=> array('type'=>'string','required'=>false),        
        "no_umrah"=> array('type'=>'string','required'=>false)
    );
	
	public $PARAM_UMRAH_REGISTRATION_DETAIL = array(
        "service" => array('type'=>'string','required'=>true),
        "username"=> array('type'=>'string','required'=>true),
        "apikey"=> array('type'=>'string','required'=>true),        
        "umrah_id"=> array('type'=>'string','required'=>false)
    );
	
	public $PARAM_INVOICE_UMRAH = array(
        "service" => array('type'=>'string','required'=>true),
        "username"=> array('type'=>'string','required'=>true),
        "apikey"=> array('type'=>'string','required'=>true),        
        "umrah_id"=> array('type'=>'string','required'=>false)
    );
    
    public $PARAM_UMRAH_PILGRIMS_REGISTRATION = array(
        "service" => array('type'=>'string','required'=>true),
        "username"=> array('type'=>'string','required'=>true),
        "apikey"=> array('type'=>'string','required'=>true),        
        "depdate"=> array('type'=>'string','required'=>true),
        "pilgrims_fullname"=> array('type'=>'string','required'=>true),
        "pilgrims_birthplace"=> array('type'=>'string','required'=>true),
        "pilgrims_birthday"=> array('type'=>'string','required'=>true),
        "pilgrims_docnumber"=> array('type'=>'string','required'=>true),
        "pilgrims_gender"=> array('type'=>'string','required'=>true),
        "pilgrims_citizen"=> array('type'=>'string','required'=>true),
        "pilgrims_address"=> array('type'=>'string','required'=>true),
        "pilgrims_phonenumber"=> array('type'=>'string','required'=>true),
        "pilgrims_email"=> array('type'=>'string','required'=>true),
        "room_type"=> array('type'=>'string','required'=>true),
        "umrah_package_price"=> array('type'=>'string','required'=>true)        
    );
	
	public $PARAM_PRINT_TICKET = array(
        "service" => array('type'=>'string','required'=>true),
        "username"=> array('type'=>'string','required'=>true),
        "apikey"=> array('type'=>'string','required'=>true),        
        "reservation_id"=> array('type'=>'string','required'=>false),
        "kode_booking"=> array('type'=>'string','required'=>false),
        "airlines_id"=> array('type'=>'string','required'=>false)
    );
	
	public $PARAM_MERCHANT_NOTIF_LIST = array(
        "service" => array('type'=>'string','required'=>true),                
        "username"=> array('type'=>'string','required'=>false),
        "apikey"=> array('type'=>'string','required'=>false),
        "status"=> array('type'=>'string','required'=>false),
        "page"=> array('type'=>'string','required'=>false)
    );
	
	public $PARAM_MERCHANT_NOTIF_ADD = array(
        "service" => array('type'=>'string','required'=>true),                
        "username"=> array('type'=>'string','required'=>true),
        "apikey"=> array('type'=>'string','required'=>true)
    );
	
	public $PARAM_DESTINATIONS_AIRLINES = array(
        "service" => array('type'=>'string','required'=>true),                
        "username"=> array('type'=>'string','required'=>true),
        "apikey"=> array('type'=>'string','required'=>true)
    );
	
	public $PARAM_APIKEY_DEVICE_USER = array(
        "service" => array('type'=>'string','required'=>true),                
        "username"=> array('type'=>'string','required'=>true),
        "apikey"=> array('type'=>'string','required'=>true),
        "apikey_device"=> array('type'=>'string','required'=>true),
        "userid_mitra"=> array('type'=>'string','required'=>true)
    );
	
	public $PARAM_SEARCH_FLIGHT_AIRLINES = array(
        "service" => array('type'=>'string','required'=>true),                
        "username"=> array('type'=>'string','required'=>true),
        "apikey"=> array('type'=>'string','required'=>true),
        "airlines_id"=> array('type'=>'string','required'=>true),
        "dewasa"=> array('type'=>'string','required'=>true),
        "anak"=> array('type'=>'string','required'=>false),
        "bayi"=> array('type'=>'string','required'=>false),
        "ruteBerangkat"=> array('type'=>'string','required'=>true),
        "ruteTujuan"=> array('type'=>'string','required'=>true),
        "ruteKembali"=> array('type'=>'string','required'=>false),
        "pp"=> array('type'=>'string','required'=>true),
        "tanggal_pergi"=> array('type'=>'string','required'=>true),
        "tanggal_pulang"=> array('type'=>'string','required'=>false)
    );
	
	public $PARAM_BOOK_FLIGHT_AIRLINES = array(
        "service" => array('type'=>'string','required'=>true),                
        "username"=> array('type'=>'string','required'=>true),
        "apikey"=> array('type'=>'string','required'=>true),
        "airlines_id"=> array('type'=>'string','required'=>true),
        "dewasa"=> array('type'=>'string','required'=>true),
        "anak"=> array('type'=>'string','required'=>false),
        "bayi"=> array('type'=>'string','required'=>false),
        "ruteBerangkat"=> array('type'=>'string','required'=>true),
        "ruteTujuan"=> array('type'=>'string','required'=>true),
        "ruteKembali"=> array('type'=>'string','required'=>false),
        "pp"=> array('type'=>'string','required'=>true),
        "seatId_pergi"=> array('type'=>'string','required'=>true),
        "tanggal_pergi"=> array('type'=>'string','required'=>true),
        "tanggal_pulang"=> array('type'=>'string','required'=>false)
    );
	
	public $PARAM_ISSUED_AIRLINES = array(
        "service" => array('type'=>'string','required'=>true),                
        "username"=> array('type'=>'string','required'=>true),
        "apikey"=> array('type'=>'string','required'=>true),
        "airlines_id"=> array('type'=>'string','required'=>true),
        "kode_booking"=> array('type'=>'string','required'=>true)
    );
	
	public $PARAM_CHECKCOOKIE_LION = array(
        "service" => array('type'=>'string','required'=>true),                
        "username"=> array('type'=>'string','required'=>true),
        "apikey"=> array('type'=>'string','required'=>true),
        "status_proses"=> array('type'=>'integer','required'=>false)
    );
	
	public $PARAM_CHECK_HARGA = array(
        "service" => array('type'=>'string','required'=>true),                
        "username"=> array('type'=>'string','required'=>true),
        "apikey"=> array('type'=>'string','required'=>true),
        "berangkat"=> array('type'=>'string','required'=>true),
        "tujuan"=> array('type'=>'string','required'=>true),
        "tglPergi"=> array('type'=>'string','required'=>true),
        "isReturn"=> array('type'=>'string','required'=>true),
        "dewasa"=> array('type'=>'string','required'=>true),
        "airlines_id"=> array('type'=>'string','required'=>true)
    );
	
	public $PARAM_FAVORITE_PPOB = array(
        "service" => array('type'=>'string','required'=>true),                
        "username"=> array('type'=>'string','required'=>true),
        "apikey"=> array('type'=>'string','required'=>true),
        "catg_ppob"=> array('type'=>'string','required'=>true),
        "operator"=> array('type'=>'string','required'=>false)
    );
	
	public $PARAM_DELETE_FAVORITE_PPOB = array(
        "service" => array('type'=>'string','required'=>true),                
        "username"=> array('type'=>'string','required'=>true),
        "apikey"=> array('type'=>'string','required'=>true),
        "data_favo"=> array('type'=>'string','required'=>true),
        "catg_ppob"=> array('type'=>'string','required'=>true)
    );
	
	public $PARAM_AIRLINES_SEARCH = array(
        "service" => array('type'=>'string','required'=>true),
        "username"=> array('type'=>'string','required'=>true),
        "apikey"=> array('type'=>'string','required'=>true),        
        "origin"=> array('type'=>'string','required'=>true),
        "destination"=> array('type'=>'string','required'=>true),
        "depdate"=> array('type'=>'string','required'=>true),
        "adult"=> array('type'=>'string','required'=>true),
        "input_pergi"=> array('type'=>'string','required'=>true),
        "airline"=> array('type'=>'string','required'=>true),
        "ruteKembali"=> array('type'=>'string','required'=>false),
        "retdate"=> array('type'=>'string','required'=>false),
        "child"=> array('type'=>'string','required'=>false),
        "infant"=> array('type'=>'string','required'=>false)
    );
	
	public $PARAM_EXCHANGE_RATE = array(
        "service" => array('type'=>'string','required'=>true),                
        "username"=> array('type'=>'string','required'=>true),
        "apikey"=> array('type'=>'string','required'=>true)
    );
    
    public $ENDPOINT_SERVER_MAPPING = array(
		"print_ticket" => "ibe_ws",
		"search_flight_airlines" => "search_flights_lion",
		"book_flight_airlines" => "book_flights_lion",
		"issued_airlines" => "issued_lion",
		"cookie_lion" => "function_api",
		"check_harga" => "function_api",
		"favorite_ppob" => "ibe_ws",
		"delete_favorite_ppob" => "ibe_ws",
		"merchant_notif_list" => "ibe_ws",
		"merchant_notif_add" => "ibe_ws",
		"destinations_airlines" => "ibe_ws",
		"apikey_device_user" => "ibe_ws",
        "forget_password" => "verify_forget_password",
        "confirm_otp_verify" => "confirm_verify_otp",
        "lowfares" => "international",
        "reservation_book" => "international",
        "test" => "international",
        "auth_login" => "ibe_ws",
        "auth_logout" => "ibe_ws",
        "auth_check_session" => "ibe_ws",
        "reservation_issue" => "ibe_ws",
        "cancel_book" => "ibe_ws",
        "check_balance" => "ibe_ws",
        "get_user_by_name" => "ibe_ws",
        "read_pnr" => "ibe_ws",
        "sent_email" => "ibe_ws",
        "book_list" => "ibe_ws",
        "cancel_book" => "ibe_ws",
        "hotel_search" => "hotel",
        "hotel_detail" => "hotel_detail",
        "hotel_detail_rooms" => "hotel_detail_rooms",
        "hotel_cancel_policy" => "hotel_cancel_policy",
        "hotel_gallery_images" => "ibe_ws",
        "hotel_book" => "ibe_ws",
        "hotel_read_pnr" => "ibe_ws",
        "hotel_voucher_list" => "ibe_ws",
        "create_users" => "ibe_ws",
        "regis_billing" => "ibe_ws",
        "regis_billing_aktivasi" => "ibe_ws",
        "regis_billing_read" => "ibe_ws",
        "check_suggestion" => "check_suggestion",
        "train_search" => "train_search",
        "train_search_v2" => "train_search_v2",
        "train_book" => "ibe_ws",
        "train_takeseat" => "ibe_ws",
        "train_seatmap" => "train_seatmap",
        "train_seatmap_v2" => "train_seatmap_v2",
        "train_read_pnr" => "ibe_ws",
        "train_issue" => "ibe_ws",
        "train_cancel_book" => "ibe_ws",
        "train_reservation_list" => "ibe_ws",
        "train_cancel_direct_kai" => "train_cancel_direct_kai",
        "deposit_topup_request" => "ibe_ws",
        "deposit_topup_list" => "ibe_ws",
        "deposit_topup_confirmation" => "ibe_ws",
        "deposit_topup_payment_read" => "ibe_ws",
        "deposit_topup_payment_mocashbri" => "ibe_ws",
        "payment_inquiry" => "ibe_ws",
        "payment_issue" => "ibe_ws",
        "payment_read" => "ibe_ws",
        "payment_cancel" => "ibe_ws",
        "payment_check_status" => "ibe_ws",
        "payment_list" => "ibe_ws",
        "payment_list_lookup" => "ibe_ws",
        "content_view" => "ibe_ws",
        "content_view_read" => "ibe_ws",
        "user_change_pass" => "ibe_ws",
        "shuttle_bus_search" => "shuttle_bus_search",
        "shuttle_bus_data" => "ibe_ws",
        "shuttle_bus_book" => "ibe_ws",
        "shuttle_bus_cancel" => "ibe_ws",
        "shuttle_bus_read" => "ibe_ws",
        "shuttle_bus_issue" => "ibe_ws",
        "shuttle_bus_seat" => "shuttle_bus_seat",
        "shuttle_bus_list" => "ibe_ws",
        "report_transaction_list" => "ibe_ws",
        "send_ticket" => "ibe_ws",
        "umrah_schedule_list" => "ibe_ws",
        "umrah_registration_list" => "ibe_ws",
        "umrah_registration_detail" => "ibe_ws",
        "invoice_umrah" => "ibe_ws",
        "umrah_pilgrims_registration" => "ibe_ws",
        "exchange_rate" => "ibe_ws",
        "airlines_search" => "airlines_search"
    );
    
    public $METHOD_REQ_MAPPING = array(
        "auth_login" => "POST",
        "auth_logout" => "POST",
        "auth_check_session" => "POST",
        "search_flight_airlines" => "POST",
        "book_flight_airlines" => "POST",
        "cookie_lion" => "POST",
        "check_harga" => "POST",
        "favorite_ppob" => "POST",
        "delete_favorite_ppob" => "POST",
		"merchant_notif_list" => "POST",
		"merchant_notif_add" => "POST",
		"destinations_airlines" => "POST",
		"apikey_device_user" => "POST",
        "lowfares" => "POST",
        "reservation_book_domestik" => "POST",
        "reservation_book" => "GET",
        "test" => "POST",
        "reservation_issue" => "POST",
        "cancel_book" => "POST",
        "check_balance" => "POST",
        "get_user_by_name" => "POST",
        "read_pnr" => "POST",
        "sent_email" => "POST",
        "book_list" => "POST",
        "cancel_book" => "POST",
        "hotel_search" => "POST",
        "hotel_detail" => "GET",
        "hotel_detail_rooms" => "GET",
        "hotel_cancel_policy" => "GET",
        "hotel_gallery_images" => "POST",
        "hotel_book" => "POST",
        "hotel_read_pnr" => "POST",
        "hotel_voucher_list" => "POST",
		"create_users" => "POST",
		"regis_billing" => "POST",
        "regis_billing_aktivasi" => "POST",
        "regis_billing_read" => "POST",
        "check_suggestion" => "GET",
        "train_search" => "GET",
        "train_search_v2" => "GET",
        "train_book" => "POST",
        "train_takeseat" => "POST",
        "train_seatmap" => "GET",
        "train_read_pnr" => "POST",
        "train_issue" => "POST",
        "train_cancel_book" => "POST",
        "train_reservation_list" => "POST",
        "train_cancel_direct_kai" => "GET",
        "deposit_topup_request" => "POST",
        "deposit_topup_list" => "POST",
        "deposit_topup_confirmation" => "POST",
        "deposit_topup_payment_read" => "POST",
        "deposit_topup_payment_mocashbri" => "POST",
        "payment_inquiry" => "POST",
        "payment_issue" => "POST",
        "payment_read" => "POST",
        "payment_cancel" => "POST",
        "payment_check_status" => "POST",
        "payment_list" => "POST",
        "payment_list_lookup" => "POST",
        "content_view" => "POST",
        "content_view_read" => "POST",
        "user_change_pass" => "POST",
        "shuttle_bus_search" => "GET",
        "shuttle_bus_data" => "POST",
        "shuttle_bus_book" => "POST",
        "shuttle_bus_cancel" => "POST",
        "shuttle_bus_read" => "POST",
        "shuttle_bus_issue" => "POST",
        "shuttle_bus_seat" => "POST",
        "shuttle_bus_list" => "POST",
        "report_transaction_list" => "POST",
        "send_ticket" => "POST",
        "print_ticket" => "POST",
        "umrah_schedule_list" => "POST",
        "umrah_registration_list" => "POST",
        "umrah_registration_detail" => "POST",
        "invoice_umrah" => "POST",
        "umrah_pilgrims_registration" => "POST",
		"exchange_rate" => "POST",
        "airlines_search" => "GET"
    );        
    
    public $PAYMENT_TYPE = array(
        /* TELKOM */        
        "1|001001|1" => array("category"=>"TELKOM", "name"=>"Telkom"),
        "2|001001|1" => array("category"=>"TELKOM", "name"=>"Speedy"),
        "5|001001|1" => array("category"=>"TELKOM", "name"=>"Flexy"),
        "1|001001|5" => array("category"=>"TELKOM", "name"=>"Kartu Halo"),
        /* PLN */
        "pr|PLN PREPAID|11" => array("category"=>"PLN", "name"=>"PLN Prepaid"), //
        "po|PLN POSTPAID|12" => array("category"=>"PLN", "name"=>"PLN Postpaid"),
        "pn|NONTAGLIS|13" => array("category"=>"PLN", "name"=>"PLN Non Taglis"),
        /* PDAM */
        "2|001406|2" => array("category"=>"PDAM", "name"=>"PDAM AETRA/TPJ"),
        "2|001401|2" => array("category"=>"PDAM", "name"=>"PDAM KOT BANDUNG"),
        "1|001303|2" => array("category"=>"PDAM", "name"=>"PDAM KAB BANDUNG"),
        "2|001403|2" => array("category"=>"PDAM", "name"=>"PDAM JAMBI"),
        "2|001405|2" => array("category"=>"PDAM", "name"=>"PDAM PALEMBANG"),
        "2|001404|2" => array("category"=>"PDAM", "name"=>"PDAM LAMPUNG"),
        "2|001402|2" => array("category"=>"PDAM", "name"=>"PDAM KAB MALANG"),
        "2|001407|2" => array("category"=>"PDAM", "name"=>"PDAM PALYJA"),
        /* Multifinance */
        "1|020013|6" => array("category"=>"MULTIFINANCE", "name"=>""),
        "PAY-0017" => array("category"=>"MULTIFINANCE", "name"=>""),
        /* TV Kabel */
        "1|090001|3" => array("category"=>"TVCABLE", "name"=>"Indovision"),
        "1|006001|4" => array("category"=>"TVCABLE", "name"=>"AORA TV"),
        "3|001001|9" => array("category"=>"TVCABLE", "name"=>"Telkom Vision Postpaid"),
        "4|001001|1" => array("category"=>"TVCABLE", "name"=>"Yes TV"),
        /* PULSA */
        "vp|XL|10" => array("category"=>"PULSA", "name"=>"XL"),
        "vp|AXIS|10" => array("category"=>"PULSA", "name"=>"Axis"),
        "vp|INDOSAT|10" => array("category"=>"PULSA", "name"=>"IM3"),
        "vp|INDOSAT|10" => array("category"=>"PULSA", "name"=>"Mentari"),
        "vp|SMARTFREN|10" => array("category"=>"PULSA", "name"=>"SmartFren"),
        "vp|SIMPATI|10" => array("category"=>"PULSA", "name"=>"Simpati/AS"),
        "vp|THREE|10" => array("category"=>"PULSA", "name"=>"Three"),
        "vp|ESIA|10" => array("category"=>"PULSA", "name"=>"Esia"),
        "vp|FLEXI|10" => array("category"=>"PULSA", "name"=>"Flexi"),
        /* INTERNET DATA */
        "vi|AXIS DATA|18" => array("category"=>"INETDATA", "name"=>"Axis Data"),
        "vi|BIZNET|18" => array("category"=>"INETDATA", "name"=>"Biznet"),
        "vi|BOLT|18" => array("category"=>"INETDATA", "name"=>"BOLT"),
        "vi|INDOSAT DATA|18" => array("category"=>"INETDATA", "name"=>"Indosat Data"),
        "vi|TELKOMSEL DATA|18" => array("category"=>"INETDATA", "name"=>"Telkomsel Data"),
        "vi|THREE DATA|18" => array("category"=>"INETDATA", "name"=>"Three Data"),
        "vi|XL DATA|18" => array("category"=>"INETDATA", "name"=>"XL Data"),
        /* BPJS */
        "bp|080003|17" => array("category"=>"BPJS", "name"=>"BPJS Keluarga"),
    );
    
    public $ROOT_PATH;
    public $LIB_PATH;
    public $SERVICES_PATH;
    
}