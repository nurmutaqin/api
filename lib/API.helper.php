<?php
error_reporting(E_ALL);
ini_set('display_errors', 0);
require_once "lib/API.exception.php";

class API_Helper {
    
    private static $request=null;
    
    function __construct() {
        
    }

    public static function extractBetweenKeywordAbac($s, $keyword1, $keyword2) {
            // Mengambil string diantara dua keyword
            $l1=strlen($keyword1);
            $x1=strpos($s, $keyword1);
            $x2=strpos($s, $keyword2, $x1+$l1);
            $l=$x2-($x1+$l1);
            return substr($s, $x1+$l1, $l); 
    
	}

    public static function abacus_getAfterKeyword($s, $keyword) {
            //mengambil seluruh setelah keyword yg ditentukan
            $l=strlen($keyword);
            $x1=strpos($s, $keyword);
            return substr($s, $x1+$l);
    }

    public static function restructData($data=null) {
        $tmp_data = new stdClass();         
        
        $fields = array(
            "passport_number"=>"", 
            "passport_expired"=>"", 
            "passport_issue_country"=>"passport_issuecountry", 
            "nationality_doc"=>"passport_nasionalitydocument"
        );
        
        for ($i=0;$i<count($data);$i++) {
            $item = $data[$i];
            if ($item->label === "adultchilds") {
                $prefix = "adultchild_";
            }
            
            else if ($item->label === "childs") {
                $prefix = "child_";
            }
            
            else if ($item->label === "infants") {
                $prefix = "infant_";
            }
            
            foreach ($item as $k=>$v) {
                if (array_key_exists($k, $fields)) {
                    $infix = (!empty($fields[$k])?$fields[$k]:$k);
                    if (\property_exists($item, $k)) {
                        unset($data[$i]->$k);
                        $new_prop = $prefix.$infix;
                        $data[$i]->$new_prop = $v;
                    }
                }
            }
            
            if (property_exists($item, 'label')) {
                unset($data[$i]->label);
            }
        }
        
        $tmp_data->data = $data;
        $tmp_data->_count = count($data);

        return $tmp_data;
    }

    public static function validateDate($date, $format = 'Y-m-d H:i:s') {
        $d = DateTime::createFromFormat($format, $date);
        return $d && $d->format($format) == $date;
    }
    
    public static function validationInit($params, $setting) {
        /*
         * Mapping parameters passed to the parameter that has been configured.
         * Return : Array of diff between these two parameters
         */
        
        $diff = array_diff(array_keys($setting), array_keys((array)$params));
        $fields = array_values($diff);
        
        $dttype = array('string'=>'','int'=>'','array'=>array(),'object'=>new stdClass(),'arrayobject'=>array(new stdClass()));
        
        foreach ($fields as $value) {
            $params->$value = $dttype[$setting[$value]['type']];
        }
        
        return $params;
    }

    public static function validateForms($params, $setting) {
        $params = self::validationInit($params, $setting);        //print_r($params); exit;
        
        foreach ($params as $key => $value) {
            $empty_value=false;
            if (array_key_exists($key, $setting)) {
                if (is_string($value)) {
                    if (empty($value)) {
                        $empty_value=true;
                    }                            
                }

                else if (is_array($value)) { //if ($key === 'seatid_dep') {echo "hai ", (count($value));exit;}
                    if (count($value)<1) {
                        $empty_value=true;
                    }
                    
                    else {
                        foreach ($value as $v) {
                            if (is_object($v)) {
                                if (count((array)$v) == 0) {
                                    $empty_value=true;
                                }
                            }
                            
                            else if (is_string($v)) {
                                if (empty($v)) {
                                    $empty_value=true;
                                }
                            }
                        }
                    }
                }

                else if (is_object($value)) {
                    if (count((array)$value)<1) {
                        $empty_value=true;
                    }
                }                
                
                if ($setting[$key]['required'] === true && $empty_value) {
                    APIException::_throwException(APIException::ERROR_PARAMS_ISREQUIRED, $key, 10004);
                }
                                
                if (!$empty_value) {

                    /*
                     * Data type
                     */

                    if ($setting[$key]['type'] === "string") {
                        if (!is_string($value)) {                            
                            APIException::_throwException(APIException::ERROR_PARAMS_INVALIDPARAM_TYPE_STRING, $key, 10005);
                        }
                    }

                    else if ($setting[$key]['type'] === "int") {
                        if (!filter_var($value, FILTER_VALIDATE_INT)) { //!is_int($value)
                            APIException::_throwException(APIException::ERROR_PARAMS_INVALIDPARAM_TYPE_INT, $key, 10005);
                        }
                    }

                    else if ($setting[$key]['type'] === "bool") {
                        $arr_str_bool = array('true', 'false', 'TRUE', 'FALSE');

                        if (!filter_var($value, FILTER_VALIDATE_BOOLEAN)) {//!is_bool($value)
                            if (is_string($value)) {
                                if (!in_array($value, $arr_str_bool)) {
                                    APIException::_throwException(APIException::ERROR_PARAMS_INVALIDPARAM_TYPE_BOOL, $key, 10005);
                                }                                                        
                            }

                            else {
                                APIException::_throwException(APIException::ERROR_PARAMS_INVALIDPARAM_TYPE_BOOL, $key, 10005);
                            }
                        }                                        
                    }

                    else if ($setting[$key]['type'] === "array") {
                        if (!is_array($value)) {
                            APIException::_throwException(APIException::ERROR_PARAMS_INVALIDPARAM_TYPE_ARRAY, $key, 10005);
                        }
                    }
                    
                    else if ($setting[$key]['type'] === "arrayobject") {
                        if (!is_array($value)) {
                            APIException::_throwException(APIException::ERROR_PARAMS_INVALIDPARAM_TYPE_ARRAY, $key, 10005);
                        }

                        else {
                            foreach ($value as $v) {
                                if (!is_object($v)) {
                                    APIException::_throwException(APIException::ERROR_PARAMS_INVALIDPARAM_TYPE_ARRAYOBJ, $key, 10005);
                                }
                                
                                else if (count((array)$v) == 0) {
                                    APIException::_throwException(APIException::ERROR_PARAMS_INVALIDPARAM_TYPE_NODATA, $key, 10005);
                                }
                            }
                        }
                    }

                    /*
                     * Length
                     */

                    if (isset($setting[$key]['length'])) {
                        $val_length = strlen($value);
                        if (is_string($setting[$key]['length'])) {
                            if ($val_length !== $setting[$key]['length']) {
                                $code = array($key, $setting[$key]['length']); 
                                
                                APIException::_throwException(APIException::ERROR_PARAMS_INVALIDPARAM_FORM_LENGTH, $code, 10006);
                            }
                        }
                        
                        else {
//                            $
//                            if ($val_length >= $setting[$key]['length']["min"]) {
//                                
//                            }
                        }
                    }

                    /*
                     * Format: Date, Currency, etc
                     */

                    if (isset($setting[$key]['format'])) {                    
                        if (array_key_exists('date', $setting[$key]['format'])) {
                            if (!self::validateDate($value, $setting[$key]['format']['date'])) {                                
                                APIException::_throwException(APIException::ERROR_PARAMS_INVALIDPARAM_FORM_DATE, $key, 10006);
                            }
                        }

                        else if (array_key_exists('seatid', $setting[$key]['format'])) {
                            if ($setting[$key]['format']['seatid'] === 'string') {
                                foreach ($value as $k => $v) {
                                    $expl = explode("|", $v);
                                    if (count($expl)!==9) {                                        
                                        APIException::_throwException(APIException::ERROR_PARAMS_INVALIDPARAM_FORM_SEATID, $key, 10006);
                                    }
                                }                                
                            }
                        }
                        
                        else if (array_key_exists('email', $setting[$key]['format'])) {                             
                            if (!filter_var($value, FILTER_VALIDATE_EMAIL)) { //!is_int($value)                                
                                APIException::_throwException(APIException::ERROR_PARAMS_INVALIDPARAM_FORM_EMAIL, $key, 10006);
                            }
                        }
                    }
                
                }
            }                        
        }                
    }

    public static function setFilterRequest($request, $method="POST") {
        if ($method === "POST") {
            $clean_input = Array();
            if (is_array($request)) {
                foreach ($request as $k => $v) {
                    $clean_input[$k] = self::setFilterRequest($v);
                }

            } else if (is_object($request)) {
                $request = (array) $request;
                foreach ($request as $k => $v) {
                    $clean_input[$k] = self::setFilterRequest($v);
                }

                $clean_input = (object) $clean_input;

            }
			// else if(self::isJSON($request)){
				// $request = json_decode($request,true);
				// if (is_array($request)) {
					// foreach ($request as $k => $v) {
						// $clean_input[$k] = self::setFilterRequest($v);
					// }

				// }
			// }
			else {
                $clean_input = trim(strip_tags($request));
            }
            
            return $clean_input;
        }
        
        else {        
            APIException::_throwException(APIException::ERROR_HTTP_INVALIDMETHOD, null, 10002); 
        }                
    }
	
	// public static function isJSON($string){
	   // return is_string($string) && is_array(json_decode($string, true)) && (json_last_error() == JSON_ERROR_NONE) ? true : false;
	// }
    
    public static function convertArray($data, $assoc=true) {
        return json_decode(json_encode($data), $assoc);
    }
}