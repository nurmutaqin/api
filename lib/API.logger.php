<?php
$QXCLIBPATH='../webservice.sro/1.0/qxcUniLib/1.0';
require("$QXCLIBPATH/lib/qxc-1.0-php/System.php");

class APILogger {
	public $requestId=null;
	public $sequenceNumber=0;
	public $folderName='/var/www/html/sro/static/temp/api';
	public $username=null;
	public $activity=null;
	public function __construct($requestId=null, $sequenceNumber=null,$username=null,$activity=null) {
		if (!$requestId) {
			$this->requestId=qxc\System::NowAsISOFilenameFrendly().'_'.qxc\System::UUID()->string;
		} else {
			$this->requestId=$requestId;
			if ($sequenceNumber) {
				$this->sequenceNumber=$sequenceNumber;
			} else {
				$this->sequenceNumber=0;
			}
		}
		
		$this->tahun = date('Y');
		$this->bln = date('M');
		$this->tgl = date('d');
		
		$dir_origin = "/client";
                
                $sabre_endpoint = array("lowfares", "reservation-book", "reservation-issue", "check-balance", "read-pnr", "sent-email", "book-list", "cancel-book");
                
                if (in_array($activity, $sabre_endpoint)) {
                    $dir = "/international";
                }
                
                else if (strpos($activity, "hotel-") > -1) {
                    $dir = "/hotel";
                } 
                
                else if (strpos($activity, "train-") > -1) {
                    $dir = "/train";
                } 
                
                else if (strpos($activity, "deposit-") > -1) {
                    $dir = "/deposit";
                } 
                
                else if (strpos($activity, "payment-") > -1) {
                    $dir = "/ppob";
                } 
                
                else if (strpos($activity, "shuttle-") > -1) {
                    $dir = "/shuttle";
                }
                
                else {
                    $dir = "/misc";
                }
		
		$this->folderName_Client=$this->folderName.$dir_origin.'/'.$this->tahun.'/'.$this->bln.'/'.$this->tgl;
		$this->folderName=$this->folderName.$dir.'/'.$this->tahun.'/'.$this->bln.'/'.$this->tgl;
			
		$this->username = $username;
		$this->activity = $activity;
	}
        
	public function logfile($content) {
		$this->lastfilename=$this->folderName.'/'.$this->requestId.'_'.$this->sequenceNumber.'_'.$this->activity.'_'.$this->username.'.xml';
		file_put_contents($this->lastfilename, $content);
		$this->log("Saving content to $this->lastfilename");
		$this->sequenceNumber=$this->sequenceNumber+1;
	}
        
	public function log($text) {
		file_put_contents($this->folderName.'/'.$this->requestId.'_'.$this->activity.'_'.$this->username.'.log', $text."\r\n", FILE_APPEND);
	}
	
	public function log_Client($text) {
		file_put_contents($this->folderName_Client.'/'.$this->requestId.'_'.$this->activity.'_'.$this->username.'.log', $text."\r\n", FILE_APPEND);
	}	
}