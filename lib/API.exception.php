<?php
error_reporting(E_ALL);
ini_set('display_errors', 0);

class APIException extends Exception {    
    const ERROR_CONNECTIONERROR = 'Connection to Server Error';    
    const ERROR_HTTP_INVALIDMETHOD = 'Only POST method allowed';
    const ERROR_PARAMS_INVALIDSERVICE = "Invalid service %s";
    const ERROR_PARAMS_NOAPIKEY = 'No API Key Provided';
    const ERROR_PARAMS_INVALIDAPIKEY = 'Invalid API key / Your session has expired. Please contact provider Asia Wisata for support';
    const ERROR_PARAMS_INVALIDUSER = 'Invalid API User. Please contact provider Asia Wisata for support';
    const ERROR_PARAMS_ISREQUIRED = "Field '%s' is required";
    const ERROR_PARAMS_INVALIDPARAM_TYPE_STRING = "Field '%s' must be a String";
    const ERROR_PARAMS_INVALIDPARAM_TYPE_INT = "Field '%s' must be an Integer";
    const ERROR_PARAMS_INVALIDPARAM_TYPE_BOOL = "Field '%s' must be an Boolean";
    const ERROR_PARAMS_INVALIDPARAM_TYPE_ARRAY = "Field '%s' must be an Array";
    const ERROR_PARAMS_INVALIDPARAM_TYPE_ARRAYOBJ = "Field '%s' must be an Array[Object]";
    const ERROR_PARAMS_INVALIDPARAM_TYPE_NODATA = "Field '%s' has no data";
    const ERROR_PARAMS_INVALIDPARAM_FORM_DATE = "Field '%s' format must be 'YYYY-MM-DD'";
    const ERROR_PARAMS_INVALIDPARAM_FORM_SEATID = "Field '%s' has invalid format";
    const ERROR_PARAMS_INVALIDPARAM_FORM_LENGTH = "Field '%s' has invalid length. Only %s characters allowed";
    const ERROR_PARAMS_INVALIDPARAM_FORM_EMAIL = "Field '%s' must be valid e-mail address";
    const ERROR_PARAMS_INVALIDPARAM_INPUT_DATA = "There was a missing or invalid data";
    const ERROR_REGIS_BILLING = "Registration Billing FAILED";
    const ERROR_REGIS_BILLING_READ = "Read Registration Billing FAILED";
        
    const ERROR_AUTH_MUSTLOGIN = 'You have to login to use an API';
    const ERROR_SABRE_XMLERROR = 'Invalid XML';
    const ERROR_SABRE_NONESCHEDULE = 'Schedule Not Available';
    const ERROR_SABRE_NOFARES = 'No Fares';
    const ERROR_SABRE_NULLRESPONSE = 'Null Response';
    const ERROR_SABRE_INVALID_RSVID = 'Invalid Reservation ID';
    const ERROR_HOTEL_IMAGES_NOTFOUND = 'Images not found';
    const ERROR_HOTEL_CANCELPOLICY_NOTFOUND = 'Cancel policy not found';
    const ERROR_HOTEL_HOTELDETAILS_NOTFOUND = 'Detail hotel not found';
    const ERROR_HOTEL_HOTELDETAILROOMS_NOTFOUND = 'Detail rooms not found';
    const ERROR_TRAIN_SCHEDULE_NOTFOUND = 'Schedule not available';
    const ERROR_DEPOSIT_TOPUP_NOTAUTHORIZED = "NOT AUTHORIZED";
	const ERROR_UMRAH_UMRAHDETAILS_NOTFOUND = 'Detail Umrah not found';
	const ERROR_NOTIF_ADD_FAIL = 'ADD NOTIF FAIL';
    const ERROR_UNKNOWNERROR = 'Unknown Error';
    const ERROR_AUTH_SESSIONEXP = 'You are already logged out / Your session has expired';
    
    public static $http_stat = array(
        200 => "OK",
        400 => "Bad Request",
        401 => "Unauthorized",
        405 => "Method Not Allowed",
        408 => "Request Timeout",
        422 => "Unprocessable Entity",
        500 => "Internal Server Error"
    ); 
    
    public static $error = array(
        "status" => array(
            500 => array(
                "message" => array(
                    10000 => 'Unknown Error',
                    10001 => 'Connection to Server Error'                    
                )
            ),
            
            405 => array(
                "message" => array(
                    10002 => 'Invalid Method Request'
                )
            ),
			
			408 => array(
                "message" => array(
                    10014 => 'Network Disruption'
                )
            ),
            
            401 => array(
                "message" => array(
                    10007 => 'Authentication Failed',
                )
            ),

            400 => array(
                "message" => array(                        
                    10003 => 'Invalid Service',
                    10004 => 'Parameter is Required',
                    10005 => 'Invalid Parameter Type',
                    10006 => 'Invalid Parameter Format',                    
                    10012 => 'Invalid Parameter Request Data',
                    10013 => 'Invalid Password',
                    10008 => 'Invalid XML',
                    10036 => 'Registration Failed',
                    10041 => 'Registration Billing Read',
                )
            ),
            
            200 => array( //IBE success, provider (sabre, mg, etc) error
                "message" => array(
                    10009 => 'API Error',
                    10010 => 'Hotel Error',
                    10011 => 'KAI Error',
                    10020 => 'Deposit Error',                    
                    10021 => 'Payment PPOB',
                    10022 => 'Shuttle Bus',
                    10030 => 'Create User',
                    10031 => 'Role Assignment',
                    10032 => 'Create Merchant',
                    10033 => 'Create Merchant Members',
                    10034 => 'Domestic Airlines',
                    10035 => 'Request Topup',
                    10037 => 'Topup Confirmation',
                    10038 => 'UMRAH FAIL',
                    10039 => 'Registration Activation',
                    10040 => 'Registration Billing Read',
                    10042 => 'Apikey Device User',
                    10043 => 'Favorite PPOB',
                    10044 => 'NOTIFICATION',
                    10045 => 'Train Take Seat'
                )
            )
        )
    );       
    
    public static function _response($error) {
        $error = (object) $error;
		
        if (!empty($error->message)) {
            foreach (self::$error["status"] as $key=>$value) {
                if (array_key_exists($error->code, self::$error["status"][$key]["message"])) {
                    $http_status = $key;
                    break;
                }
            }            
            
            $http_text = self::$http_stat[$http_status];
            $exception_type = self::$error["status"][$http_status]["message"][$error->code];            
            
            $response = array(
                "result_status"=>"$http_text ($http_status)", 
                "result_code"=>$error->code, 
                "result_message"=>$exception_type, 
                "result_detail"=>$error->message
            );
            
        } else {
            $http_status = 500;
            $http_text = self::$http_stat[$http_status];
            
            $response = array(
                "result_status"=>"$http_text ($http_status)",
                "result_code"=>10000, 
                "result_message"=>self::$error["status"][500]["message"][10000], 
                "result_detail"=>$http_text
            );
        }
        
//        if ($http_status == "401") {
//            header('WWW-Authenticate: Basic realm="API"');
//        }
        
//        header("HTTP/1.1 " . $http_status . " " . $http_text);
        return $response;
    }
    
    public static function _throwException($text, $fields=null, $code=null) {
        if (is_array($fields)) {
            throw new Exception(vsprintf($text, $fields), $code);
        }
        
        else {
            throw new Exception(sprintf($text, $fields), $code);
        }
    }        
}