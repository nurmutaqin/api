<?php
error_reporting(E_ALL);
ini_set('display_errors', 0);

require_once('lib/API.helper.php');

class API_Handler {
    private $endpoint;
    private $response;
    private $exception_list = array("auth_logout", "user_change_pass", "check_suggestion");
    
    public function __construct() {
        $this->response = new stdClass();
    }        
    
    public function processXml($endpoint, $result, $options=array()) {        
        try {
            return $this->{$endpoint}($result, (object) $options);        

        } catch (Exception $e) {
            throw $e;                     
        }
    }        
    
    private function initialObject($response, $options) {
        $this->response = new stdClass();
        if (!empty($response) || in_array($options->endpoint, $this->exception_list)) {
            if (isset($response->result)) {
                $response->result = trim($response->result);
                if ($response->result === "OK") {
                    $this->response->result_status = ($response->result === "OK"?"OK":$response->result);
                    $this->response->result_code = "00000";
                    $this->response->result_message = ($response->resultText === "OK"?"Success":$response->resultText);                    
                }
                
                else if ("NOTAUTHORIZED" === $response->result) {
                    $this->response->result_status = "OK";
                    $this->response->result_code = "00000";
                    $this->response->result_message = $response->result;
                }
                
                else if ("ERROR" === $response->result && isset($response->text)) {
                    APIException::_throwException($response->text, '', 10022);
                }
                
                else if ("Could not access file: /var/www/html/sro/static/ticket/.pdf" === $response->resultText) {
                    APIException::_throwException("Ticket not available", '', 10010);
                }                                
                
                else {
                    APIException::_throwException($response->resultText, '', 10009);
                }
            }

            else {
                $this->response->result_status = "OK";
                $this->response->result_code = "00000";
                $this->response->result_message = "Success";
            }
        }
        
        else {
            APIException::_throwException(APIException::ERROR_SABRE_NULLRESPONSE, '', 10009);
        }
    }        
    
    private function auth_login($response) {
        $this->initialObject($response);        //print_r($response); exit;     
        if ($response->output->session === "in_use") {
            $this->response->session = new stdClass();
            $this->response->session->id = "0";
            $this->response->session->status = $response->output->session;
        }
        
        else if ($response->result === "NOTAUTHORIZED") {
            APIException::_throwException("Login failed. Check the user ID and password and then try again.", '', 10007);
        }
		
		// else if (empty($response)) {
            // APIException::_throwException("Mohon Maaf Untuk Sementara Operasional Asia Wisata dihentikan. Info jelasnya silahkan hubungi email info@asiawisata.com", '', 10014);
        // }
        
        else {
            $this->response->user = new stdClass();
            $this->response->user->id = $response->output->user->id;
            $this->response->user->name = $response->output->user->name;
            $this->response->user->fullname = $response->output->user->fullname;
            $this->response->user->address = $response->output->user->address;
            $this->response->user->phone = $response->output->user->phone;
            $this->response->user->merchant_id = $response->output->user->merchant_id;
            $this->response->user->merchat_fullname = $response->output->user->merchat_fullname;
            $this->response->user->merchanttype_id = $response->output->user->merchanttype_id;
            $this->response->user->merchant_code = $response->output->user->merchant_code;
            $this->response->user->role_id = $response->output->user->role_id;
            $this->response->user->role_uri = $response->output->user->role_uri;
            $this->response->user->role_fullname = $response->output->user->role_fullname;            
            $this->response->user->status_role = $response->output->user->status_role;
            $this->response->user->email = $response->output->user->roleassignments->rowset[0]->email;
            
            $this->response->session = new stdClass();
            $this->response->session->id = $response->output->session->id;
            $this->response->session->uuid = $response->output->session->UUID;
            $this->response->session->status = "free";
        }
        
        return $this->response;//
    }
    
    private function auth_logout($response, $options) {
        $this->initialObject($response, $options);        
        $this->response->logout = $response->output;
//        if (isset($response->output) && $response->output !== false) {
//            $this->response->logout = $response->output;
//        }                
        
//        else {
//            APIException::_throwException(APIException::ERROR_AUTH_SESSIONEXP, '', 10007);
//        }
        
        return $this->response;
    }
    
    private function auth_check_session($response) {
        $this->initialObject($response);        
        
        if (!isset($response->output->resultText)) {
            $this->response->session = $response->output;
        }
        
        else {            
            APIException::_throwException($response->output->resultText, '', 10014);
        }
        
        return $this->response;
    }
    
    /**
     * Sabre International
     * 
     * @param Object $response
     * @return Object
     */
    
    private function lowfares($response) {        
        $this->initialObject($response);
        
//        $res_temp = new stdClass();
//        foreach ($response as $key=>$value) {
//            if ($key === "departure" || $key === "return") {
//                $res_temp->$key = (array) $value;
//            }        
//        }
        
        $this->response->departure = $response->departure;
        
        if (isset($response->return)) {
            $this->response->return = $response->return;
        }
        
        return $this->response;
    }

    private function reservation_book_domestik($response) {        
        $this->initialObject($response);
		if (isset($response->output->new_reservation->pnr)) {
			$this->response->reservation = new stdClass();
            $this->response->reservation->id = $response->output->new_reservation->reservation_id;
            $this->response->reservation->pnr = $response->output->new_reservation->pnr;
            $this->response->reservation->TimeLimit = $response->output->TimeLimit;
            $this->response->reservation->status = $response->output->new_reservation->status;
            $this->response->reservation->paxpay = $response->output->new_reservation->paxpaid;
            $this->response->reservation->totalpay = $response->output->new_reservation->totalpaid;
            $this->response->reservation->TripDetail = $response->output->TripDetail;
            $this->response->reservation->FareCalculation = $response->output->FareCalculation;
		} else if(!empty($response->output->text)){
			APIException::_throwException($response->output->text, '', 10034);
		}
		
		return $this->response;
	}
	
	private function reservation_book($response) {        
        $this->initialObject($response);
        
        if (isset($response->output->new_reservation->pnr)) {
            $this->response->reservation = new stdClass();
            $this->response->reservation->id = $response->output->new_reservation->reservation_id;
            $this->response->reservation->pnr = $response->output->new_reservation->pnr;
            $this->response->reservation->pnr_airline = $response->output->new_reservation->pnr_airline;
            $this->response->reservation->timelimit = $response->request->input->timelimit;
            $this->response->reservation->status = $response->output->new_reservation->status;
            $this->response->reservation->paxpay = $response->output->new_reservation->paxpaid;
            $this->response->reservation->totalpay = $response->output->new_reservation->totalpaid;
            $this->response->reservation->flights = $response->request->input->flights;
            $this->response->reservation->passengers = $response->request->input->passengers;
            $this->response->reservation->fare_calculation = $response->request->input->fareCalculation;
            $this->response->reservation->contact_name = $response->request->input->contact_name;
            $this->response->reservation->contact_number = $response->request->input->contact_telp;
            $this->response->reservation->phone_area = $response->request->input->phone_area;
            $this->response->reservation->email = $response->request->input->email;
            
            foreach ($this->response->reservation->flights as $key => $value) {
                $this->response->reservation->flights[$key]->operation_code_airline = $this->response->reservation->flights[$key]->operatingAirline;
                $this->response->reservation->flights[$key]->marketing_code_airline = $this->response->reservation->flights[$key]->marketingAirline;
                $this->response->reservation->flights[$key]->departure_via = $this->response->reservation->flights[$key]->Departure_VIA;
                $this->response->reservation->flights[$key]->arrival_via = $this->response->reservation->flights[$key]->Arrival_VIA;
                $this->response->reservation->flights[$key]->meals_code = $this->response->reservation->flights[$key]->mealsCode;
                $this->response->reservation->flights[$key]->stop_qty = $this->response->reservation->flights[$key]->stopQty;
                $this->response->reservation->flights[$key]->air_equip_type = $this->response->reservation->flights[$key]->AirEquipType;
                
                unset($this->response->reservation->flights[$key]->operatingAirline);
                unset($this->response->reservation->flights[$key]->marketingAirline);
                unset($this->response->reservation->flights[$key]->Departure_VIA);
                unset($this->response->reservation->flights[$key]->Arrival_VIA);
                unset($this->response->reservation->flights[$key]->mealsCode);
                unset($this->response->reservation->flights[$key]->stopQty);
                unset($this->response->reservation->flights[$key]->AirEquipType);
            }
            
            foreach ($this->response->reservation->fare_calculation as $key => $value) {
                $this->response->reservation->fare_calculation[$key]->pax_type = $this->response->reservation->fare_calculation[$key]->PaxType;
                $this->response->reservation->fare_calculation[$key]->pax = $this->response->reservation->fare_calculation[$key]->Pax;
                $this->response->reservation->fare_calculation[$key]->basic = $this->response->reservation->fare_calculation[$key]->HargaDasar;
                $this->response->reservation->fare_calculation[$key]->tax = $this->response->reservation->fare_calculation[$key]->Pajak;
                $this->response->reservation->fare_calculation[$key]->extra_cover = $this->response->reservation->fare_calculation[$key]->ExtraCover;
                $this->response->reservation->fare_calculation[$key]->iwjr = $this->response->reservation->fare_calculation[$key]->IWJR;
                $this->response->reservation->fare_calculation[$key]->subtotal = $this->response->reservation->fare_calculation[$key]->Subtotal;
                $this->response->reservation->fare_calculation[$key]->total = $this->response->reservation->fare_calculation[$key]->Total;
                
                unset($this->response->reservation->fare_calculation[$key]->PaxType);
                unset($this->response->reservation->fare_calculation[$key]->Pax);
                unset($this->response->reservation->fare_calculation[$key]->HargaDasar);
                unset($this->response->reservation->fare_calculation[$key]->Pajak);
                unset($this->response->reservation->fare_calculation[$key]->ExtraCover);
                unset($this->response->reservation->fare_calculation[$key]->IWJR);
                unset($this->response->reservation->fare_calculation[$key]->Subtotal);
                unset($this->response->reservation->fare_calculation[$key]->Total);
            }
        }
        
        return $this->response;
    }   
    
    private function reservation_issue($response) {
        $this->initialObject($response);
        $this->response->reservation = new stdClass();
        
        if ($response->output->result === "OK" && !empty($response->output->reservation_id)) {            
            $this->response->reservation->reservation_id = $response->output->reservation_id;
//            $this->response->reservation->status = $response->output->reservation->status;
//            $this->response->reservation->pnr = $response->output->reservation->pnr;
//            $this->response->reservation->pnr_airline = $response->output->reservation->pnr_airline;
//            $this->response->reservation->paxpaid = $response->output->reservation->paxpaid;
//            $this->response->reservation->totalpaid = $response->output->reservation->totalpaid;
        } else {
            APIException::_throwException($response->output->text, '', 10009);
        }
        
        return $this->response;
    }
    
    private function cancel_book($response) {
        $this->initialObject($response);        
        if (!empty($response->output->reservation->reservation_id)) {
            $this->response->reservation = new stdClass();
            $this->response->reservation->id = $response->output->reservation->reservation_id;
            $this->response->reservation->status = $response->output->reservation->status;
            $this->response->reservation->pnr = $response->output->reservation->pnr;
            $this->response->reservation->pnr_airline = $response->output->reservation->pnr_airline;
        }
        
        return $this->response;
    }
    
    private function check_balance($response) {
        $this->initialObject($response);
        
        if ($response->result === "OK") {            
            $this->response->balance = $response->output->balance;
            $this->response->balance_dollar = $response->output->balance_dollar;
        }
        
        return $this->response;
    }
    
    private function read_pnr($response) {                
        $this->initialObject($response);        //print_r($response); exit;
        if (sizeof($response->output->reservation) > 0) {
            unset($response->output->reservation->merchant);
            unset($response->output->reservation->booking_user);
            unset($response->output->reservation->issue_user);
            unset($response->output->reservation->cancel_user);
            unset($response->output->reservation->issue_user_id);            
            unset($response->output->reservation->booking_datetime);
            unset($response->output->reservation->issue_datetime);
            unset($response->output->reservation->ttm_reservation_merchant_id);
            unset($response->output->reservation->merchant_id);
            unset($response->output->reservation->merchant_code);
            unset($response->output->reservation->merchant_fullname);
            unset($response->output->reservation->user_name);
            unset($response->output->reservation->user_fullname);
            unset($response->output->reservation->airlines_id);
            unset($response->output->reservation->canceled_user_id);
            unset($response->output->reservation->canceled_datetime);
            unset($response->output->reservation->booking_username);
            unset($response->output->reservation->issued_username);
            unset($response->output->reservation->cancel_username);
            unset($response->output->reservation->first_issued);
            unset($response->output->reservation->no_referensi);
            unset($response->output->reservation->parent_id);
            unset($response->output->reservation->operation_code_airline);
            unset($response->output->reservation->marketing_code_airline);
            unset($response->output->reservation->void_username);
            unset($response->output->reservation->void_datetime);
            unset($response->output->reservation->pct_commission);
            unset($response->output->reservation->commision);            
            unset($response->output->reservation->agen_commission);            
            unset($response->output->reservation->bfm);
            // unset($response->output->reservation->airline_name);
            unset($response->output->reservation->exchange_rate);
            // unset($response->output->reservation->fpaxpaid);
            // unset($response->output->reservation->ftotalpaid);
            unset($response->output->reservation->frealnta);
            
            $this->response->reservation = $response->output->reservation;
            $this->response->reservation->id = $response->output->reservation->reservation_id;
            $this->response->reservation->paxpay = $response->output->reservation->paxpaid;
            $this->response->reservation->totalpay = $response->output->reservation->totalpaid;            
            
            unset($this->response->reservation->reservation_id);
            unset($this->response->reservation->paxpaid);
            unset($this->response->reservation->totalpaid);            
            
            foreach ($response->output->reservation->flights as $key => $value) {
                unset($response->output->reservation->flights[$key]->reservation_id);                
            }
            
            $this->response->reservation->passenger_lists = $response->output->reservation->passengers;
            $this->response->reservation->fare_calculation = $response->output->reservation->fares;
            
            unset($this->response->reservation->passengers);
            
            foreach ($this->response->reservation->fare_calculation as $key => $value) {
                $this->response->reservation->fare_calculation[$key]->pax_type = $this->response->reservation->fare_calculation[$key]->paxtype;                
                $this->response->reservation->fare_calculation[$key]->basic = $this->response->reservation->fare_calculation[$key]->fhargadasar;
                $this->response->reservation->fare_calculation[$key]->tax = $this->response->reservation->fare_calculation[$key]->fpajak;
                $this->response->reservation->fare_calculation[$key]->iwjr = $this->response->reservation->fare_calculation[$key]->fiwjr;
                $this->response->reservation->fare_calculation[$key]->subtotal = $this->response->reservation->fare_calculation[$key]->fsubtotal;
                $this->response->reservation->fare_calculation[$key]->total = $this->response->reservation->fare_calculation[$key]->ftotal;
                $this->response->reservation->fare_calculation[$key]->extra_cover = $this->response->reservation->fare_calculation[$key]->fextra;
                
                unset($this->response->reservation->fare_calculation[$key]->id);
                unset($this->response->reservation->fare_calculation[$key]->reservation_id);
                unset($this->response->reservation->fare_calculation[$key]->paxtype);
                unset($this->response->reservation->fare_calculation[$key]->fhargadasar);                
                unset($this->response->reservation->fare_calculation[$key]->fpajak);
                unset($this->response->reservation->fare_calculation[$key]->fiwjr);
                unset($this->response->reservation->fare_calculation[$key]->fsubtotal);
                unset($this->response->reservation->fare_calculation[$key]->ftotal);
                unset($this->response->reservation->fare_calculation[$key]->fextra);
            }
            
            unset($this->response->reservation->fares);
            unset($this->response->reservation->tickets);
        }
        
        else {
            APIException::_throwException(APIException::ERROR_SABRE_INVALID_RSVID, '', 10009);
        }
        
        return $this->response;
    }
    
    private function sent_email($response) {
        $this->initialObject($response);
        
        if ($response->result === "OK") {
            $this->response->result_detail = $response->output->text;
        }
        
        return $this->response;
    }
    
    private function book_list($response) {
        $this->initialObject($response);        
        // if ($response->result === "OK") {
            $size = count($response->output->reservations->rowset);
            $this->response->reservations = array();
            
            for ($i=0;$i<$size;$i++) {
                $this->response->reservations[$i] = new stdClass();
                
                $this->response->reservations[$i]->pnr = $response->output->reservations->rowset[$i]->pnr;
                $this->response->reservations[$i]->reservation_id = $response->output->reservations->rowset[$i]->reservation_id;
                $this->response->reservations[$i]->booking_datetime = $response->output->reservations->rowset[$i]->booking_datetime;
                $this->response->reservations[$i]->timelimit = $response->output->reservations->rowset[$i]->timelimit;                
                $this->response->reservations[$i]->airline_name = $response->output->reservations->rowset[$i]->airline_name;
                $this->response->reservations[$i]->status = $response->output->reservations->rowset[$i]->status;
                $this->response->reservations[$i]->status_name = $response->output->reservations->rowset[$i]->statusx;
                $this->response->reservations[$i]->contact_name = $response->output->reservations->rowset[$i]->contact_name;                
                $this->response->reservations[$i]->topup_id = $response->output->reservations->rowset[$i]->topup_id;                
            }
            
            $this->response->recordcount = $response->output->reservations->recordcount;
            $this->response->pagecount = $response->output->reservations->pagecount;
            $this->response->pageindex = $response->output->reservations->pageindex;
        // }
        
        return $this->response;
    }   
    
    /**
     * Hotel - MG
     * 
     * @param Object $response
     * @return Object
     */
    
    private function hotel_search($response) {
        $this->initialObject($response);
        
        if (sizeof($response->output->hotel->rowset) > 0) {
            foreach ($response->output->hotel->rowset as $key=>$value) {
                $response->output->hotel->rowset[$key]->internal_code = $value->InternalCode;
                $response->output->hotel->rowset[$key]->cancel_policy_id = $value->CancelPolicyId;
                $response->output->hotel->rowset[$key]->stars = $value->bintang;
                // $response->output->hotel->rowset[$key]->currency_rate = $value->rate;
                $response->output->hotel->rowset[$key]->dest_country = $value->DestCountry;
                $response->output->hotel->rowset[$key]->dest_city = $value->DestCity;
                
                unset($response->output->hotel->rowset[$key]->bintang);
                unset($response->output->hotel->rowset[$key]->rate);
                unset($response->output->hotel->rowset[$key]->InternalCode);
                unset($response->output->hotel->rowset[$key]->CancelPolicyId);
                unset($response->output->hotel->rowset[$key]->DestCountry);
                unset($response->output->hotel->rowset[$key]->DestCity);
                unset($response->output->hotel->rowset[$key]->price_travflex);
            }
            
            $this->response->hotel = $response->output->hotel->rowset;
            $this->response->page = $response->output->page;
            $this->response->misc->dest_country = $response->output->destcountry;
            $this->response->misc->dest_city = $response->output->destcity;
        }
        
        else {
            APIException::_throwException($response->output->text, '', 10010);
        }
        
        return $this->response;
    }
    
    private function hotel_detail($response) {
        $this->initialObject($response);
        
        if (sizeof($response->output->hotelmanagement->rowset) > 0) {
            $this->response->facilities = $response->output->hotel_facilities->rowset;//
            $this->response->hotel_spec = $response->output->hotelmanagement->rowset[0];
            $this->response->hotel_spec->star = $this->response->hotel_spec->bintang;
            unset($this->response->hotel_spec->bintang);
            
            $this->response->gallery = $response->output->gallery->rowset;            
        }
        
        else {
            APIException::_throwException(APIException::ERROR_HOTEL_HOTELDETAILS_NOTFOUND, '', 10010);
        }
        
        return $this->response;
    }
    
    private function hotel_cancel_policy($response) {
        $this->initialObject($response);
        if (!empty($response->output->viewcancelpolicy->rowset[0]->from_date)) {
            $this->response->viewcancelpolicy = $response->output->viewcancelpolicy->rowset;//
        }
        
        else {
            APIException::_throwException(APIException::ERROR_HOTEL_CANCELPOLICY_NOTFOUND, '', 10010);
        }
        
        return $this->response;
    }
    
    private function hotel_detail_rooms($response,$params) {
        $this->initialObject($response);        
        
        if (count($response->output->hotel->rowset)>0) {
            foreach ($response->output->hotel->rowset as $key => $value) {
                $response->output->hotel->rowset[$key]->promo_name = $value->Promo_Name;
                $response->output->hotel->rowset[$key]->promo_value = $value->Promo_Value;
                $response->output->hotel->rowset[$key]->promo_code = $value->Promo_Code;
                $response->output->hotel->rowset[$key]->discount = $value->diskon;
                $response->output->hotel->rowset[$key]->discount_2 = $value->potongan;
                $response->output->hotel->rowset[$key]->price_ori = $value->price_asli;
                $response->output->hotel->rowset[$key]->price_publish_ori = $value->price_publish_asli;//
                
                $response->output->hotel->rowset[$key]->price_room = $response->output->hotel->rowset[$key]->price_room->rowset[0]->price;
                $response->output->hotel->rowset[$key]->room_type = $response->output->hotel->rowset[$key]->room_type->rowset;
                
                foreach ($response->output->hotel->rowset[$key]->room_type as $k => $v) {
                    $response->output->hotel->rowset[$key]->room_type[$k]->adult_num = $response->output->hotel->rowset[$key]->room_type[$k]->adult_num->rowset;
                    $response->output->hotel->rowset[$key]->room_type[$k]->child_num = $response->output->hotel->rowset[$key]->room_type[$k]->child_num->rowset;
                    $response->output->hotel->rowset[$key]->room_type[$k]->ChildAge1 = $response->output->hotel->rowset[$key]->room_type[$k]->ChildAge1->rowset;
                    $response->output->hotel->rowset[$key]->room_type[$k]->ChildAge2 = $response->output->hotel->rowset[$key]->room_type[$k]->ChildAge2->rowset;
                }                
                
                unset($response->output->hotel->rowset[$key]->Promo_Name);
                unset($response->output->hotel->rowset[$key]->Promo_Value);
                unset($response->output->hotel->rowset[$key]->Promo_Code);
                unset($response->output->hotel->rowset[$key]->diskon);
                unset($response->output->hotel->rowset[$key]->potongan);
                unset($response->output->hotel->rowset[$key]->price_asli);
                unset($response->output->hotel->rowset[$key]->price_publish_asli);
            }
            
            $this->response->rooms = $response->output->hotel->rowset;
			if ($params->device_model == "PARTNER API" || $params->device_model == "ELE-L09"){
				foreach ($this->response->rooms as $key => $value) {
					unset($this->response->rooms[$key]->price_room);
					unset($this->response->rooms[$key]->price_publish);
					unset($this->response->rooms[$key]->price_weekdays);
					unset($this->response->rooms[$key]->price_weekends);
					unset($this->response->rooms[$key]->discount_2);
					unset($this->response->rooms[$key]->price_ori);
					unset($this->response->rooms[$key]->price_publish_ori);
				}
				$this->response->rooms = $this->response->rooms;
			}
        }
        
        else {
            APIException::_throwException(APIException::ERROR_HOTEL_HOTELDETAILROOMS_NOTFOUND, '', 10010);
        }
        
        return $this->response;
    }
    
    private function hotel_gallery_images($response) {
        $this->initialObject($response);
        if (count($response->output->fachotel->rowset)>0) {
            $this->response->gallery = $response->output->fachotel->rowset;//
        }
        
        else {
            APIException::_throwException(APIException::ERROR_HOTEL_IMAGES_NOTFOUND, '', 10010);
        }
        
        return $this->response;
    }
    
    private function hotel_book($response,$params_device) {
        //new_hotelreservation
        $this->initialObject($response);
        if (isset($response->output->new_hotelreservation)) { //!isset($response->output->result) && !isset($response->output->text)
            $this->response->hotelreservation = $response->output->new_hotelreservation;
            $this->response->cancel_policy = $response->output->GetCancel_Policy;
			
			if ($params_device->device_model == "PARTNER API" || $params_device->device_model == "ELE-L09"){
				unset($this->response->hotelreservation->room_id);
				unset($this->response->hotelreservation->booking_datetime);
				unset($this->response->hotelreservation->confirm_user_id);
				unset($this->response->hotelreservation->canceled_user_id);
				unset($this->response->hotelreservation->nta);
				unset($this->response->hotelreservation->real_nta);
				unset($this->response->hotelreservation->confirm_datetime);
				unset($this->response->hotelreservation->canceled_datetime);
				unset($this->response->hotelreservation->timelimit);
				unset($this->response->hotelreservation->real_price);
				unset($this->response->hotelreservation->confirm_pay_user_id);
				unset($this->response->hotelreservation->confirm_pay_datetime);
				unset($this->response->hotelreservation->cancelhbooking_id);
				unset($this->response->hotelreservation->policy_id);
				unset($this->response->hotelreservation->contact_refund);
				unset($this->response->hotelreservation->value_refund);
				unset($this->response->hotelreservation->topup_id);
				unset($this->response->hotelreservation->device);
				unset($this->response->hotelreservation->book_app_userid);
				unset($this->response->hotelreservation->book_app_dttime);
				unset($this->response->hotelreservation->token_issued);
				unset($this->response->hotelreservation->ftotal_price);
				unset($this->response->hotelreservation->fnta);
				unset($this->response->hotelreservation->frealnta);
			}
        }
        
        else {
            APIException::_throwException($response->output->text, '', 10010);
        }
        
        return $this->response;
    }
    
    private function hotel_read_pnr($response) {        
        $this->initialObject($response);
        
        if (isset($response->output->hotelreservation)) {
            $this->response->hotel = new stdClass();
            
            $response->output->hotelreservation->viewcancelpolicy = $response->output->hotelreservation->cancel_policy;
            unset($response->output->hotelreservation->cancel_policy);
            
            foreach ($response->output->hotelreservation->rooms as $k => $v) {
                unset($response->output->hotelreservation->rooms[$k]->price);
                unset($response->output->hotelreservation->rooms[$k]->price_publish);
            }
            
            foreach ($response->output->hotelreservation->passengers as $k=>$v) {
                $response->output->hotelreservation->passengers[$k]->doctype = $response->output->hotelreservation->passengers[$k]->documenttype;
                $response->output->hotelreservation->passengers[$k]->docNumber = $response->output->hotelreservation->passengers[$k]->documentno;
            }
            
            $this->response->hotel->reservation = $response->output->hotelreservation;            
        }
        
        else {
            APIException::_throwException($response->output->text, '', 10010);
        }
        
        return $this->response;
    }
    
    private function hotel_voucher_list($response) {
        $this->initialObject($response);
        
        if (isset($response->output->ticketinglist)) {
            $this->response->ticketing = new stdClass();
            $this->response->ticketing->list = $response->output->ticketinglist->rowset;                        
            $this->response->ticketing->recordcount = (int)$response->output->ticketinglist->recordcount;                        
            $this->response->ticketing->pagecount = (int)$response->output->ticketinglist->pagecount;                        
            $this->response->ticketing->pageindex = (int)$response->output->ticketinglist->pageindex;                        
        }
        
        else {
            APIException::_throwException($response->output->text, '', 10010);
        }
        
        return $this->response;
    }
    
    private function roleassignments($response, $options) {
        $this->initialObject($response, $options);
        
        if (isset($response->output->new_roleassignment->id)) {
			$this->response->new_roleassignment = $response->output->new_roleassignment;
        }
		else{
			APIException::_throwException($response->output->text, '', 10031);
		}
        
        return $this->response;
    }
	
	private function reservations_issue($response, $options) {
        $this->initialObject($response, $options);
        
        if (!empty($response->output->reservation_id)) {
			$this->response->reservation_id = $response->output->reservation_id;
        }
		else if (!empty($response->output->text)) {
			APIException::_throwException($response->output->text, '', 10034);
		}
		else{
            $message = "Mohon maaf Issued Gagal. Silakan Issued Kembali.";
            
            APIException::_throwException($message, '', 10034);
		}
        
        return $this->response;
    }
	
	private function create_merchant_members($response, $options) {
        $this->initialObject($response, $options);
        
        if (isset($response->output->new_merchantmembers->id)) {
			$this->response->new_merchantmembers = $response->output->new_merchantmembers;
        }
		else{
			APIException::_throwException($response->output->text, '', 10033);
		}
        
        return $this->response;
    }
	
	private function create_merchant($response, $options) {
        $this->initialObject($response, $options);
        
        if (isset($response->output->new_merchant->merchant_id)) {
			$this->response->new_merchant = $response->output->new_merchant;
        }
		else{
			APIException::_throwException($response->output->text, '', 10032);
		}
        
        return $this->response;
    }
	
	private function forget_password($response, $options) {
        $this->initialObject($response, $options);
		$this->response = $response;
       
        return $this->response;
    }
	
	private function confirm_otp_verify($response, $options) {
        $this->initialObject($response, $options);
		$this->response = $response;
       
        return $this->response;
    }
	
	private function create_users($response, $options) {
        $this->initialObject($response, $options);
        
        if (isset($response->output->new_user->id)) {
			$this->response->new_user = $response->output->new_user;
        }
		else{
			APIException::_throwException($response->output->text, '', 10030);
		}
        
        return $this->response;
    }
	
	private function check_suggestion($response, $options) {
        $this->initialObject($response, $options);
        //print_r($response); exit;
        if (count($response)>0) {
            $this->response->suggestion = $response;
        } 
        
        else if (empty($response)) {
            $this->response->suggestion = array();
        }
        
        else {
            APIException::_throwException($response->output->text, '', 10010);
        }
        
        return $this->response;
    }
    
    /**
     * Kereta API
     * 
     */
    
    private function train_search($response) {
        $this->initialObject($response);
        
        if (count($response->schedule)>0) {
            $this->response->origin = $response->org;
            $this->response->destination = $response->des;
            $this->response->depdate = $response->dep_date;
			$this->response->vendor = $response->vendor;
            $this->response->schedule = array();
            
            foreach ($response->schedule as $k => $v) {
                $this->response->schedule[$k] = new stdClass();
                $this->response->schedule[$k]->no = $v[0];
                $this->response->schedule[$k]->name = $v[1];
                $this->response->schedule[$k]->deptime = $v[2];
                $this->response->schedule[$k]->arrtime = $v[3];
                $this->response->schedule[$k]->arrdate = $v[5];
                
                foreach ($v[4] as $y => $z) {
                    $this->response->schedule[$k]->seat[$y] = new stdClass();
                    $this->response->schedule[$k]->seat[$y]->class = $z[2];
                    $this->response->schedule[$k]->seat[$y]->subclass = $z[0];
                    $this->response->schedule[$k]->seat[$y]->availability = $z[1];
                    $this->response->schedule[$k]->seat[$y]->adult_price = $z[3];
                    $this->response->schedule[$k]->seat[$y]->child_price = $z[4];
                    $this->response->schedule[$k]->seat[$y]->infant_price = 0;
                }                                                
            }
        } else {
            APIException::_throwException(APIException::ERROR_TRAIN_SCHEDULE_NOTFOUND, '', 10011);
        }
        
        return $this->response;
    }
	
	private function train_search_v2($response) {
        $this->initialObject($response);
        
        if (count($response->schedule)>0) {
            $this->response->origin = $response->org;
            $this->response->destination = $response->des;
            $this->response->depdate = $response->dep_date;
            $this->response->vendor = $response->vendor;
            $this->response->schedule = array();
            
            foreach ($response->schedule as $k => $v) {
                $this->response->schedule[$k] = new stdClass();
                $this->response->schedule[$k]->no = $v[0];
                $this->response->schedule[$k]->name = $v[1];
                $this->response->schedule[$k]->deptime = $v[2];
                $this->response->schedule[$k]->arrtime = $v[3];
                $this->response->schedule[$k]->arrdate = $v[5];
                
                foreach ($v[4] as $y => $z) {
                    $this->response->schedule[$k]->seat[$y] = new stdClass();
                    $this->response->schedule[$k]->seat[$y]->class = $z[2];
                    $this->response->schedule[$k]->seat[$y]->subclass = $z[0];
                    $this->response->schedule[$k]->seat[$y]->availability = $z[1];
                    $this->response->schedule[$k]->seat[$y]->adult_price = $z[3];
                    $this->response->schedule[$k]->seat[$y]->child_price = $z[4];
                    $this->response->schedule[$k]->seat[$y]->infant_price = 0;
                }                                                
            }
        } else {
            APIException::_throwException(APIException::ERROR_TRAIN_SCHEDULE_NOTFOUND, '', 10011);
        }
        
        return $this->response;
    }
    
    private function train_seatmap($response) {
        $this->initialObject($response);
        
        foreach ($response->seat_map as $key => $value) {
            $this->response->seatmap[$key] = new stdClass();
            $this->response->seatmap[$key]->wagon_code = $value[0];
            $this->response->seatmap[$key]->wagon_no = $value[1];
            $this->response->seatmap[$key]->seat = array();
            
            foreach ($value[2] as $k => $v) {
                $seat = new stdClass();
                $seat->row = $v[0];
                $seat->column = $v[1];
                $seat->seat_row = $v[2];
                $seat->seat_column = $v[3];
                $seat->subclass = $v[4];
                $seat->status = $v[5];
                
                array_push($this->response->seatmap[$key]->seat, $seat);
            }
        }
        
        return $this->response;
    }
	
	private function train_seatmap_v2($response) {
        $this->initialObject($response);
        
        foreach ($response->seat_map as $key => $value) {
            $this->response->seatmap[$key] = new stdClass();
            $this->response->seatmap[$key]->wagon_code = $value[0];
            $this->response->seatmap[$key]->wagon_no = $value[1];
            $this->response->seatmap[$key]->seat = array();
            
            foreach ($value[2] as $k => $v) {
                $seat = new stdClass();
                $seat->row = $v[0];
                $seat->column = $v[1];
                $seat->seat_row = $v[2];
                $seat->seat_column = $v[3];
                $seat->subclass = $v[4];
                $seat->status = $v[5];
                
                array_push($this->response->seatmap[$key]->seat, $seat);
            }
        }
        
        return $this->response;
    }
    
    private function train_book($response) {
        $this->initialObject($response);
        
        if (isset($response->output->new_reservation->reservation_id)) {
            $this->response->reservation = $response->output->new_reservation;
        }
        
        else {
            APIException::_throwException($response->output->text, '', 10011);
        }
        
        return $this->response;
    }
    
    private function train_cancel_book($response) {
        $this->initialObject($response);
        
        if (!empty($response->output->reservation->reservation_id)) {
            $this->response->reservation->reservation_id = $response->output->reservation->reservation_id;
            $this->response->reservation->book_code = $response->output->reservation->book_code;
            $this->response->reservation->status = $response->output->reservation->status;
            $this->response->reservation->status_label = $response->output->reservation->status_label;
        }
        
        else {
            APIException::_throwException("PNR not available", '', 10011);
        }
        
        return $this->response;
    }
    
    private function train_takeseat($response) {
        $this->initialObject($response);
        
		APIException::_throwException($response->output->text, '', 10045);
        
        return $this->response;
    }
	
	private function train_read_pnr($response) {
        $this->initialObject($response);
        
        if (!empty($response->output->reservation->reservation_id)) {
            $this->response->reservation = $response->output->reservation;
        } else {
            $this->response->reservation = null;
        }
        
        return $this->response;
    }
    
    private function train_issue($response) {
        $this->initialObject($response);
        
        if (!empty($response->output->reservation->reservation_id)) {
            $this->response->reservation->reservation_id = $response->output->reservation->reservation_id;
            $this->response->reservation->book_code = $response->output->reservation->book_code;
            $this->response->reservation->status = $response->output->reservation->status;
            $this->response->reservation->status_label = $response->output->reservation->status_label;
            $this->response->reservation->totalpaid = $response->output->reservation->totalpaid;
            $this->response->reservation->tiket_price = $response->output->reservation->ticket_price;
			if(!empty($response->output->text)){
				$message = $response->output->text;
				APIException::_throwException($message, '', 10011);
			}
        } else {
            if (strpos($response->output->text, "Maaf Reservasi ID Kereta tidak ada.!!!") > -1) {
                $message = "Reservation ID not available";
            }
            else{
				$message = $response->output->text;
			}
			
            APIException::_throwException($message, '', 10011);
        }
        
        return $this->response;
    }
    
    private function train_reservation_list($response) {        
        $this->initialObject($response);
        
        if (count($response->output->reservations->rowset)>0) {
            $size = count($response->output->reservations->rowset);            
            
            for ($i=0;$i<$size;$i++) {
                $response->output->reservations->rowset[$i]->status_name = $response->output->reservations->rowset[$i]->statusx;
                unset($response->output->reservations->rowset[$i]->statusx);
            }
            
            $this->response->reservation = $response->output->reservations->rowset;
                                   
            $this->response->recordcount = (int)$response->output->reservations->recordcount;                        
            $this->response->pagecount = (int)$response->output->reservations->pagecount;                        
            $this->response->pageindex = (int)$response->output->reservations->pageindex;                        
        }
        
        else {
            APIException::_throwException($response->output->text, '', 10011);
        }
        
        return $this->response;
    }
    
    /**
     * Deposit
     * 
     * @param type $response
     * @return type
     */
    
    private function deposit_topup_request($response) {
        $this->initialObject($response);        
        
        if ($response->output->transaction_id === "OK") {
            $response->output->transfer_destination = $response->output->transfer_tujuan;
            unset($response->output->transfer_tujuan);
            
            $this->response->topup_request = $response->output;
        }
		else if (!empty($response->output->text)) {
			APIException::_throwException($response->output->text, '', 10035);
		}
        
        return $this->response;
    }
    
    private function deposit_topup_list($response) {
        $this->initialObject($response);
        
        if (count($response->output->topup->rowset)>0) {
            foreach ($response->output->topup->rowset as $key => $value) {
                $response->output->topup->rowset[$key]->sender_acc_number = $value->no_rek_pengirim;
                $response->output->topup->rowset[$key]->sender_bank_name = $value->bank_pengirim;
                $response->output->topup->rowset[$key]->admin_acc_number = $value->norekening_admin;
                $response->output->topup->rowset[$key]->admin_acc_holder = $value->atasnama_admin;
                $response->output->topup->rowset[$key]->topup_amount = $value->jmlnominal_topup;
                
                unset($response->output->topup->rowset[$key]->no_rek_pengirim);
                unset($response->output->topup->rowset[$key]->bank_pengirim);
                unset($response->output->topup->rowset[$key]->norekening_admin);
                unset($response->output->topup->rowset[$key]->atasnama_admin);
                unset($response->output->topup->rowset[$key]->jmlnominal_topup);
            }
            
            $this->response->topup = $response->output->topup->rowset;            
        } 
        
        else {
            $this->response->topup = array();
        }
        
        $this->response->recordcount = $response->output->topup->recordcount;
        $this->response->pagecount = $response->output->topup->pagecount;
        $this->response->pageindex = $response->output->topup->pageindex;
        
        return $this->response;
    }
    
    private function deposit_topup_confirmation($response) {
        $this->initialObject($response);
        $this->response->confirmed = $response->output->cap;
		if($response->output->cap == false && (isset($response->output->text) && !empty($response->output->text))){
			APIException::_throwException($response->output->text, '', 10037);
		}
        
        return $this->response;
    }
    
    private function deposit_topup_payment_read($response) {
        $this->initialObject($response);
        
        $payment_channel_list = array("7"=>"400", "8"=>"401", "9"=>"402", "10"=>"406", "13"=>"700");
        
        if (!in_array(trim($response->output->payment_method), array("6", "7", "8", "9", "13"))) { // visa, mocash-bri, epay-bri, permata, cimbclicks
            $response->output->sender_acc_number = $response->output->no_rek_pengirim;
            $response->output->sender_bank_name = $response->output->bank_pengirim;
            $response->output->admin_acc_number = $response->output->norekening_admin;
            $response->output->admin_acc_holder = $response->output->atasnama_admin;
            $response->output->topup_amount = $response->output->jmlnominal_topup;

            unset($response->output->no_rek_pengirim);
            unset($response->output->bank_pengirim);
            unset($response->output->norekening_admin);
            unset($response->output->atasnama_admin);
            unset($response->output->jmlnominal_topup);
        }
        
        $this->response->payment = $response->output;
        $this->response->payment->pay_channel = $payment_channel_list[$response->output->payment_method];
        
        return $this->response;
    }
    
    private function deposit_topup_payment_mocashbri($response) {
        $this->initialObject($response);
        $this->response->mocash = $response->output;
        
        return $this->response;
    }
    
    /**
     * Payment PPOB
     */
    
    private function payment_inquiry($response, $options) {
        $this->initialObject($response);
//        print_r($response); exit;
        if (!isset($response->output->desc_code39) && trim($response->output->result) !== "Error" && strpos($response->output->respon_desc, "Wrong Data Input") === false) {
            $this->response->inquiry = $response->output;
			if(!empty($options->device_model) && $options->device_model == "API"){
				$this->response->inquiry->payment_id = $this->response->inquiry->telkom_ppob;
				unset($this->response->inquiry->telkom_ppob);
			}
        } else {
            if (trim($response->output->text) === "sudah payment" || strpos(strtoupper($response->output->desc_code39), "TAGIHAN SUDAH") > -1 || strpos(strtoupper($response->output->desc_code39), "ALREADY PAID") > -1) {
                $message = "Already paid";
            } else if (strpos($response->output->desc_code39, "prefix tidak terdaftar di host") > -1) {
                $message = "Invalid Customer ID";
            } else if (strpos($response->output->text, "Mohon maaf, sedang ada gangguan") > -1) {
                $message = "%s Payment system error";
            } else if (strpos($response->output->desc_code39, "Transaksi ini sedang dalam gangguan") > -1) {
                $message = "%s Payment system error";
            } else if (strpos($response->output->desc_code39, "Transaksi ditolak karena terjadi error di host") > -1) {
                $message = "Your transaction has been rejected by our system";
            } else if (strpos($response->output->respon_desc, "Wrong Data Input") > -1) {
                $message = "Invalid input data";
            }
            
            APIException::_throwException($message, array($options->payment_category), 10021);
        }
        
        return $this->response;
    }
    
    private function payment_read($response, $options) {
        $this->initialObject($response);
        if (!empty($response->output->telkom_ppob->ppob_id)) {
            $this->response->payment = $response->output->telkom_ppob;
			if(!empty($options->device_model) && $options->device_model == "API"){
				$this->response->payment->payment_id = $this->response->payment->ppob_id;
				$this->response->payment->status = $this->response->payment->status_ppob;
				$this->response->payment->total_harga = $this->response->payment->total_tagihan;
				
				if($this->response->payment->catg == 'pulsa' || $this->response->payment->catg == 'datainet') {
					unset($this->response->payment->nama_ppob);
					unset($this->response->payment->periode);
					unset($this->response->payment->biaya_admin);
					unset($this->response->payment->subproduct_code);
				}
				
				if(!empty($this->response->payment->nama_ppob)){
					$this->response->payment->customer_name = $this->response->payment->nama_ppob;
					unset($this->response->payment->nama_ppob);
				}
				
				if(!empty($this->response->payment->biaya_admin)){
					$this->response->payment->fee_admin = $this->response->payment->biaya_admin;
					unset($this->response->payment->biaya_admin);
				}
				
				if(!empty($this->response->payment->catg)){
					$this->response->payment->category = $this->response->payment->catg;
					unset($this->response->payment->catg);
				}
				
				if($this->response->payment->subproduct_code == "pr"){
					unset($this->response->payment->fee_admin);
					unset($this->response->payment->periode);
				}
				
				unset($this->response->payment->ppob_id);
				unset($this->response->payment->status_ppob);
				unset($this->response->payment->total_tagihan);
				unset($this->response->payment->respon_code);
				unset($this->response->payment->respon_desc);
				unset($this->response->payment->finnet_id);
				unset($this->response->payment->ttm_payment_ppob_merchant_code);
				unset($this->response->payment->merchant_number);
				unset($this->response->payment->fee_amount);
				unset($this->response->payment->transaction_type);
				unset($this->response->payment->bit48);
				unset($this->response->payment->bit61);
				unset($this->response->payment->trax_id);
				unset($this->response->payment->ppob_datetime);
				unset($this->response->payment->ref_tagihan);
				unset($this->response->payment->ttm_payment_ppob_merchant_id);
				unset($this->response->payment->merchant_id);
				unset($this->response->payment->merchant_code);
				unset($this->response->payment->merchanttype_id);
				unset($this->response->payment->merchanttype_name);
				unset($this->response->payment->merchant_fullname);
				unset($this->response->payment->parent_id);
				unset($this->response->payment->inquiry_username);
				unset($this->response->payment->payment_username);
				unset($this->response->payment->cancel_username);
				unset($this->response->payment->cancel_datetime);
				unset($this->response->payment->real_nta);
				unset($this->response->payment->trxid_jempol);
				unset($this->response->payment->harga_cabang);
				unset($this->response->payment->first_pay);
				unset($this->response->payment->timelimit);
				unset($this->response->payment->reqid_xl);
				unset($this->response->payment->vendor);
				unset($this->response->payment->sn_pulsa);
				unset($this->response->payment->create_dt);
				unset($this->response->payment->topup_id);
				unset($this->response->payment->userid_inq);
				unset($this->response->payment->token_issued);
				unset($this->response->payment->userid_pay);
				unset($this->response->payment->merchant_status);
				unset($this->response->payment->ppob_id_new);
				unset($this->response->payment->subproduct_code);
				
			}
            
        } else {            
            APIException::_throwException($response->resultText, '', 10021);
        }
        
        return $this->response;
    }
    
    private function payment_issue($response) {
        $this->initialObject($response);
        
        if (!empty($response->output->telkom_ppob->ppob_id)) {
            $this->response->payment = $response->output->telkom_ppob;
            $this->response->payment->text = $response->output->text;
            
        } else {
            APIException::_throwException($response->output->text, '', 10021);
        }
        
        return $this->response;
    }
    
    private function payment_check_status($response) {
        $this->initialObject($response);
        
        if (strpos($response->output->text, "Mohon maaf, Pembayaran  belum di terima") === false) {
            $this->response->status = $response->output;
            
        } else {
            APIException::_throwException("Payment has not been received yet", '', 10021);
        }
        
        return $this->response;
    }
    
    private function payment_cancel($response) {
        $this->initialObject($response);
        if (isset($response->output->telkom_ppob->ppob_id)) {
            $this->response->payment = $response->output->telkom_ppob;
        }
		else if (isset($response->output->text)) {
            APIException::_throwException($response->output->text, '', 10021);
        }
        
        return $this->response;
    }
    
    private function payment_list($response) {
        $this->initialObject($response);
        $this->response->payment_list = array();
        
        if (count($response->output->payment_list->rowset)>0) {
            $this->response->payment_list = $response->output->payment_list->rowset;
        }
        
        $this->response->recordcount = $response->output->payment_list->recordcount;
        $this->response->pagecount = $response->output->payment_list->pagecount;
        $this->response->pageindex = $response->output->payment_list->pageindex;
        
        return $this->response;
    }
    
    private function payment_list_lookup($response) {
        $this->initialObject($response);
        $this->response->list = array();
        
        if (count($response->output)>0) {
            $this->response->list = $response->output;
        }
        
        return $this->response;
    }
    
    private function content_view($response) {
        $this->initialObject($response);
        if (count($response->output->contents->rowset)>0) {
            $this->response->contents = $response->output->contents->rowset;
            $this->response->recordcount = $response->output->contents->recordcount;
            $this->response->pagecount = $response->output->contents->pagecount;
            $this->response->pageindex = $response->output->contents->pageindex;
            
        } else {            
            APIException::_throwException($response->resultText, '', 10009);
        }
        
        return $this->response;
    }
    
    private function content_view_read($response) {
        $this->initialObject($response);
        if (count($response->output->contents->rowset)>0) {
            $this->response->contents = $response->output->contents->rowset;
            $this->response->recordcount = $response->output->contents->recordcount;
            $this->response->pagecount = $response->output->contents->pagecount;
            $this->response->pageindex = $response->output->contents->pageindex;
            
        } else {            
            APIException::_throwException($response->resultText, '', 10009);
        }
        
        return $this->response;
    }
    
    private function shuttle_bus_search($response) {
        $this->initialObject($response);
        
        if (count($response->pergi)>0) {
            $this->response->departure = $response->pergi;
        }
        
        return $this->response;
    }
    
    private function shuttle_bus_data($response) {
        $this->initialObject($response);
        if (isset($response->output->sources)) {
            $this->response->sources = $response->output->sources;
        } else if (is_array($response->output) && count($response->output)>0) {
            $this->response->bus = $response->output;
        } else if (isset($response->output->days)) {
            $this->response->days = $response->output->days;
        } else {
            $this->response->bus = array();
        }
        
        return $this->response;
    }
	
    private function shuttle_bus_cancel($response) {
        $this->initialObject($response);
        if (isset($response->output->reservation->reservation_id)) {
            $this->response->bus = $response->output->reservation;
        }
        
        return $this->response;
    }
    
    private function shuttle_bus_read($response) {
        $this->initialObject($response);
		
		if (!empty($response->output->reservation->reservation_id)) {
            $this->response->bus = $response->output->reservation;            
            
        } else {            
            APIException::_throwException($response->resultText, '', 10022);
        }
		
        return $this->response;
    }
	
    private function shuttle_bus_issue($response) {
        $this->initialObject($response);
        
        if (!empty($response->output->reservation->reservation_id)) {
            $this->response->bus = $response->output->reservation;
            $this->response->bus->text = $response->output->text;
            
        } else {
            APIException::_throwException($response->output->text, '', 10022);
        }
        
        return $this->response;
    }
    
    private function shuttle_bus_book($response) {
        $this->initialObject($response); 
        
        if (isset($response->output->reservation_id)) {
            $this->response->reservation_id = $response->output->reservation_id;
        } else {
            APIException::_throwException($response->output->text, '', 10022);
        }
        
        return $this->response;
    }
    
    private function shuttle_bus_seat($response) {
        $this->initialObject($response); 
        if ($response->olplus->status === "OK") {
            $this->response->seat = $response->olplus->results;
        } else {
            APIException::_throwException($response->text, '', 10022);
        }
        
        return $this->response;
    }
    
    private function shuttle_bus_list($response) {
        $this->initialObject($response); 
        
        if (count($response->output->reservations->rowset)>0) {
            $this->response->reservations = $response->output->reservations->rowset;
        } else {
            $this->response->reservations = array();
        }
        
        $this->response->recordcount = $response->output->reservations->recordcount;
        $this->response->pagecount = $response->output->reservations->pagecount;
        $this->response->pageindex = $response->output->reservations->pageindex;
        
        return $this->response;
    }
    
    private function report_transaction_list($response) {
        $this->initialObject($response); 
        
        if (count($response->output->transactions->rowset)>0) {
            $this->response->transactions = $response->output->transactions->rowset;
        } else {
            $this->response->transactions = array();
        }
        
        $this->response->recordcount = $response->output->transactions->recordcount;
        $this->response->pagecount = $response->output->transactions->pagecount;
        $this->response->pageindex = $response->output->transactions->pageindex;
        
        return $this->response;
    }
    
    private function send_ticket($response) {
        $this->initialObject($response); 
        
        if ($response->output === "OK") {
            $this->response->status = "sent";
        } else {
            $this->response->status = "failed";
        }
        
        return $this->response;
    }

	private function print_ticket($response) {
        $this->initialObject($response); 
        
        if (!empty($response->output) && $response->result === "OK") {
			if(isset($response->output->reservations)){
				$row_ticket = $response->output->reservations->rowset;
				$url_ticket = array();
				for($i=0;$i<count($row_ticket);$i++){
					$url_ticket[$i] = $row_ticket[$i]->ticket_url;
				}
			}
			else{
				$url_ticket = array(str_replace("/var/www/html/sro","https://www.ibe.co.id",$response->output));
			}
			$this->response->url_ticket = $url_ticket;
        } else {
            $this->response->url_ticket = "";
        }
        
        return $this->response;
    }
    
    /**
     * Umrah
     */
    
    private function umrah_schedule_list($response) {
        $this->initialObject($response);
        
        if (count($response->output->umrah_list->rowset)>0) {
            $this->response->list = $response->output->umrah_list->rowset;
        } else {
            $this->response->list = array();
        }
        
        $this->response->recordcount = $response->output->umrah_list->recordcount;
        $this->response->pagecount = $response->output->umrah_list->pagecount;
        $this->response->pageindex = $response->output->umrah_list->pageindex;
        
        return $this->response;
    }
	
	private function umrah_registration_list($response) {
        $this->initialObject($response);
        
        if (count($response->output->umrah_list->rowset)>0) {
            $this->response->list = $response->output->umrah_list->rowset;
        } else {
            $this->response->list = array();
        }
        
        $this->response->recordcount = $response->output->umrah_list->recordcount;
        $this->response->pagecount = $response->output->umrah_list->pagecount;
        $this->response->pageindex = $response->output->umrah_list->pageindex;
        
        return $this->response;
    }
	
	private function invoice_umrah($response) {
        $this->initialObject($response);
        
        if (!empty($response->output->url_file)) {
            $this->response->url_file = $response->output->url_file;
        } else {
			$text = $response->output->text;
			APIException::_throwException($text, '', 10038);
        }
        
        return $this->response;
    }
	
	private function umrah_registration_detail($response) {
        $this->initialObject($response);
        
        if (!empty($response->output->umrah->umrah_id)) {
            $this->response->umrah = $response->output->umrah;
        } else {
           APIException::_throwException(APIException::ERROR_UMRAH_UMRAHDETAILS_NOTFOUND, '', 10038);
        }
        
        return $this->response;
    }
    
    private function umrah_pilgrims_registration($response) {
        $this->initialObject($response);
        
        if (count($response->output->new_umrah)>0) {
            $this->response->umrah_id = $response->output->new_umrah->id;
            $this->response->umrah = $response->output->new_umrah;
        } else {
            $this->response->umrah = null;
        }
        
        return $this->response;
    }
    
	private function merchant_notif_add($response) {
        $this->initialObject($response);
        
        if (!empty($response->output->notif->title)) {
            $this->response->notif = $response->output->notif;
        } else {
            APIException::_throwException(APIException::ERROR_NOTIF_ADD_FAIL, '', 10044);
        }
        
        return $this->response;
    }
	
	private function merchant_notif_list($response) {
        $this->initialObject($response);
        
        if (count($response->output->notif->rowset)>0) {
            $this->response->list = $response->output->notif->rowset;
        } else {
            $this->response->list = array();
        }
        
        $this->response->recordcount = $response->output->notif->recordcount;
        $this->response->pagecount = $response->output->notif->pagecount;
        $this->response->pageindex = $response->output->notif->pageindex;
        
        return $this->response;
    }
	
	private function destinations_airlines($response) {
        $this->initialObject($response);
        
        if (count($response->output->destinations->rowset)>0) {
            $this->response->list = $response->output->destinations->rowset;
        } else {
            $this->response->list = array();
        }
        
        $this->response->recordcount = $response->output->destinations->recordcount;
        $this->response->pagecount = $response->output->destinations->pagecount;
        $this->response->pageindex = $response->output->destinations->pageindex;
        
        return $this->response;
    }
	
	private function apikey_device_user($response) {
        $this->initialObject($response);
        if (isset($response->output->text)) {
            APIException::_throwException($response->output->text, '', 10042);
        }
        
        return $this->response;
    }
	
    /**
     * Misc
     * 
     * @param type $response
     * @return type
     */
    
    private function user_change_pass($response, $options) {
        $this->initialObject($response, $options); 
        
        if ($response->result !== "NOTAUTHORIZED") {
            if ($response->output) {
                $this->response->result = $response->output;
            } else {
                APIException::_throwException(APIException::ERROR_PARAMS_INVALIDPARAM_INPUT_DATA, '', 10013);
            }
        }
        
        else {
            APIException::_throwException($response->result, '', 10014);
        }
        
        return $this->response;
    }
	
	private function regis_billing($response, $options) {
        $this->initialObject($response, $options); 
        
        if (!empty($response->output->regisbil->id)) {
            $this->response->regisbil = $response->output->regisbil->id;
            $this->response->regisbil_hash = $response->output->regisbil->hash;
        }
		else if (!empty($response->output->text)) {
            APIException::_throwException($response->output->text, '', 10039);
        }
		else {
			APIException::_throwException(APIException::ERROR_REGIS_BILLING, '', 10036);
		}
        
        return $this->response;
    }
	
	private function regis_billing_read($response, $options) {
        $this->initialObject($response, $options); 
        
        if (!empty($response->output->regisbil->id)) {
            $this->response->regisbil = $response->output->regisbil;
        }
		else if (!empty($response->output->text)) {
            APIException::_throwException($response->output->text, '', 10040);
        }
		else {
			APIException::_throwException(APIException::ERROR_REGIS_BILLING_READ, '', 10041);
		}
        
        return $this->response;
    }
	
	private function regis_billing_aktivasi($response, $options) {
        $this->initialObject($response, $options); 
        
        if (!empty($response->output->regisbil->id)) {
            $this->response->regisbil = $response->output->regisbil->id;
        }
		else if (!empty($response->output->text)) {
            APIException::_throwException($response->output->text, '', 10036);
        }
		else {
			APIException::_throwException(APIException::ERROR_REGIS_BILLING, '', 10036);
		}
        
        return $this->response;
    }
	
	private function favorite_ppob($response, $options) {
        $this->initialObject($response, $options); 

       $this->response->favo = $response->output->favo;
        
        return $this->response;
    }
	
	private function delete_favorite_ppob($response, $options) {
        $this->initialObject($response, $options); 

        $this->response->text = $response->output->text;
        
        return $this->response;
    }
	
	private function cookie_lion($response, $options) {
        $this->initialObject($response, $options); 
		
        if ($response->islogin2 || $response->cek == true) {
            $this->response->cookie = $response->cookie;
            $this->response->cons_id = $response->cid;
        }
		else{
			 $this->response->cookie = '';
		}
        
        return $this->response;
    }
	
	private function search_flight_airlines($response, $options) {
        $this->initialObject($response, $options);
		
		$this->response->searchflight = $response;
       
        return $this->response;
    }
	
	private function book_flight_airlines($response, $options) {
        $this->initialObject($response, $options);
		
		$this->response->book = $response;
       
        return $this->response;
    }
	
	private function issued_airlines($response, $options) {
        $this->initialObject($response, $options);
		
		$this->response->issued = $response;
       
        return $this->response;
    }
	
	private function airlines_search($response) {
        $this->initialObject($response);
        
        if (count($response->pergi)>0) {
            $this->response->departure = $response->pergi;
			if (count($response->pulang)>0) {
				$this->response->return = $response->pulang;
			}
        }
		else {
			$this->response->text = $response->text;
		}
        
        return $this->response;
    }
	
	private function check_harga($response, $options) {
        $this->initialObject($response, $options);
		if($response->total_harga){
			if(!empty($response->total_harga)){
				$this->response->harga_basic = $response->harga_basic;
				$this->response->harga_tax = $response->harga_tax;
				$this->response->harga_total = $response->total_harga;
			}
			else{
				$this->response->harga_basic = 0;
				$this->response->harga_tax = 0;
				$this->response->harga_total = 0;
			}
		}
		else{
			$this->response->harga = $response;
		}
       
        return $this->response;
    }
    
    private function get_user_by_name($response) {
        $this->initialObject($response);
        
        if ($response->result === "OK") {            
            $this->response->user = new stdClass();
            $this->response->user->id = $response->output->user->user_id;
            $this->response->user->name = $response->output->user->name;
            $this->response->user->merchant_id = $response->output->user->merchant_id;
            $this->response->user->merchant_code = $response->output->user->code;
            $this->response->user->key = $response->output->user->password;
        }
        
        return $this->response;
    }
    
    private function level1convert() {
        $ko_temp = new stdClass();
        foreach ($ko as $key=>$value) {
            if ($key === "departure" || $key === "return") {
                $ko_temp->$key = (array) $value;
            }        
        }
    }
	
	private function exchange_rate($response) {
        $this->initialObject($response);
        
        if (count($response->output->exchange->rowset)>0) {
            $this->response->list = $response->output->exchange->rowset;
        } else {
            $this->response->list = array();
        }
		
		foreach ($this->response->list as $key => $value) {
			$this->response->list[$key]->currency = $this->response->list[$key]->currency_id;
			unset($this->response->list[$key]->adm_fee_tiger);
			unset($this->response->list[$key]->rate_beli);
			unset($this->response->list[$key]->currency_id);
		}
        
        $this->response->recordcount = $response->output->exchange->recordcount;
        $this->response->pagecount = $response->output->exchange->pagecount;
        $this->response->pageindex = $response->output->exchange->pageindex;
        
        return $this->response;
    }
	
	
}