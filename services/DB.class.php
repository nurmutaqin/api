<?php
error_reporting(E_ALL);
ini_set('display_errors', 0);
//require_once '../config/config.php';

class Db_Connect {
    private $T_USER_MGMT = "ttm_airlines_user_management";
    private $T_USER_LOCK = "ttm_airlines_user_lock";
    private $T_EXCHANGE_RATE = "exchange_rate";
    private $T_AIRCRAFT_MNF = "mm_aircraft_mnf";
    private $T_USERS = "um_users";
    private $conn = null;

    public function Db_Connect() {        
        $conn=\pg_connect("dbname='sro1dev' host='127.0.0.1' user='postgres' password='Airticket24'");

        $this->conn = $conn;
    }

    public function Save($data=array(), $table="") {
        if ($table === "T_USER_MGMT") {
            $id_seq = ' ttm_airlines_user_managemnt_id_seq ';
            $get_id=pg_exec("select nextval('$id_seq')");
            while ($row=pg_fetch_assoc($get_id)) { $id = $row['nextval']; }

            $fields = ' id, airlines_id, username, password, time_login, status_aktif, status_login, session, viewstate, cons_id, status_proses ';
//            $values = "$id, $data[airline_code], '$data[username]', '$data[password]', '$data[time_login]', '$data[status_aktif]',"
//                    . "'$data[status_login]', '$data[session]', '$data[viewstate]', '$data[cons_id]', '$data[status_proses]'";
            $params = " VALUES($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11) ";
            $values = array($id, $data["airlines_id"], $data["username"], $data["password"], $data["time_login"], $data["status_aktif"],
                            $data["status_login"], $data["session"], $data["viewstate"], $data["cons_id"], $data["status_proses"]);

//            echo 'query: ', " INSERT INTO " . $this->{$table} . " ($fields) " . $params;
//            print_r($values); exit;
        }

        else if ($table === "T_USER_LOCK") {
            $id = $data['id'];
            $fields = ' id, username, dttime ';
            $params = " VALUES( $1, $2, $3 ) ";
            $values = array($id, $data["username"], $data["dttime"]);
            // array($id, $data["username"], $data["dttime"]);
        }
        
        else if ($table === 'T_AIRCRAFT_MNF') {
            $id_seq = ' mm_aircraft_mnf_id_seq ';
            $get_id=pg_exec("select nextval('$id_seq')");
            while ($row=pg_fetch_assoc($get_id)) { $id = $row['nextval']; }
            
            $fields = ' id, aircraft_code, aircraft_manufacture, aircraft_seat ';
            $params = " VALUES($1,$2,$3,$4) ";
            $values = array($id, $data["aircraft_code"], $data["aircraft_manufacture"], $data["aircraft_seat"]);
        }

//        $query = " INSERT INTO ". $this->{$table} . " ($fields) " . " VALUES( $values ) ";
//        $execute = pg_result(pg_exec($this->conn, $query));
//        echo " INSERT INTO ". $this->{$table} . " ($fields) " . " VALUES( " . print_r($values, true) . " )"; exit;
        $query = pg_query_params($this->conn, " INSERT INTO " . $this->{$table} . " ($fields) " . $params,  $values);
//        var_dump($query);
        if (!$query) {
            echo pg_result_error($query);
        }
        return array('execute'=>$query, 'id'=>$id);
    }

    public function Update($data=array(), $where="", $param="", $table="T_USER_MGMT") {
        $fields="";
        end($data); $currkey=key($data);
        foreach ($data as $key => $value) {
            $delimiter= ($currkey !== $key ? ", " : " ");
//            echo '$currkey : ', $currkey, '$key : ', $key, " | ";

            if (!empty($value) && is_string($value)) {
                $value = "'$value'";
            }

            else if (empty($value)) {
                $value = "' '";
            }

            $fields .= "$key=$value".$delimiter;
        }

        $query = " UPDATE " . $this->{$table} . " SET "
                . $fields
                . " WHERE " . $where;

//        echo $query; exit;
        $execute = pg_query_params($this->conn, $query, array($param)); //pg_exec($query);
        
        return array('execute'=>$execute, 'params'=>$param);
    }

    public function Select($data=array(), $table="", $where="") {          
        $session=array();
        $query=" SELECT * FROM ". $this->{$table} ." WHERE $where";    

        $execute = pg_query_params($this->conn, $query, $data);                
        
        while ($row=pg_fetch_assoc($execute)) {
            $session['id'] = $row['id'];            
            $session['name']  = $row['name'];
            $session['password'] = $row['password'];
        }
//        echo(sha1("115537027c67e3a5a7f317b19450d9ed")); exit;
        return $session;
    }

    public function Delete($id=null, $table="") {
        $query=" DELETE FROM " . $this->{$table} . " WHERE id = $1";
        $execute = pg_query_params($this->conn, $query, array($id));                
        
        return $execute;
    }
}
