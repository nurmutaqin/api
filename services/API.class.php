<?php

error_reporting(E_ALL);
ini_set('display_errors',1);

define("ROOT_PATH", dirname(__FILE__) . "/");
define("LIB_PATH", ROOT_PATH . "lib/");
define("SERVICES_PATH", ROOT_PATH . "services/");

require_once 'lib/API.config.php';
require_once "services/DB.class.php";
require_once "lib/API.helper.php";
require_once 'lib/API.resParsing.php';
require_once 'lib/API.logger.php';
require_once 'lib/API.session.save.php';

abstract class API {
    /**
     * Property: method
     * The HTTP method this request was made in, either GET, POST, PUT or DELETE
     */
    protected $method = '';
    /**
     * Property: endpoint
     * The Model requested in the URI. eg: /files
     */
    protected $endpoint = '';
    /**
     * Property: verb
     * An optional additional descriptor about the endpoint, used for things that can
     * not be handled by the basic methods. eg: /files/process
     */
    protected $verb = '';
    /**
     * Property: args
     * Any additional URI components after the endpoint and verb have been removed, in our
     * case, an integer ID for the resource. eg: /<endpoint>/<verb>/<arg0>/<arg1>
     * or /<endpoint>/<arg0>
     */
    protected $args = Array();
    /**
     * Property: file
     * Stores the input of the PUT request
     */
    protected $file = Null;
         
    protected $user = "";
    protected $config = "";
    protected $apikey = "";
    protected $baseUrl = "https://www.ibe.co.id"; 
    protected $baseUrl_aws = "https://aws.asiawisata.com";
    protected $search_flight_garuda = "WebBot_GA_AGS/garuda_searchflight.php";
    protected $search_flights_lion = "WebBot_Lion/lion_searchflight.api.php";
    protected $book_flights_lion = "WebBot_Lion/lion_bookflight_air.php";
    protected $book_flights_lionweb = "WebBot_LionWeb/lion_bookflight_new.php";
    protected $issued_lion = "WebBot_Lion/lion_issued.php";
    protected $function_api = "webservice.sro/1.0/func_api2.php";
    protected $international = "international/api.php";
    protected $ibe_ws = "webservice.sro/1.0/api.php";//"api/services/wsibe/api.php";
    // protected $hotel = "WebBot_Hotel/search_hotel.php";
    protected $hotel = "WebBot_Hotel/search_hotel_new.php";
    protected $hotel_detail = "WebBot_Hotel/get_hotel_detail.php";
    protected $hotel_detail_rooms = "WebBot_Hotel/hotel_detail.php";
    protected $hotel_cancel_policy = "WebBot_Hotel/view_cancel_policy.php";
    protected $check_suggestion = "autocomplete_query.php";
    protected $train_search = "WebBot_KAI/ka_searchschedule.php";
    protected $train_search_v2 = "WebBot_KAI/ka_searchschedule_darma.php";
    protected $train_seatmap = "WebBot_KAI/ka_show_seatmap.php";
    protected $train_seatmap_v2 = "WebBot_KAI/ka_show_seatmap_darma.php";
    protected $train_cancel_direct_kai = "WebBot_KAI/ka_cancel_book.php";
    protected $shuttle_bus_search = "WebBot_Travel/travel_searchflight.php";    
    protected $shuttle_bus_issue = "WebBot_Travel/travel_issue.php";
    protected $shuttle_bus_seat = "WebBot_Travel/travel_seat.php";
    protected $verify_forget_password = "forget.password.php";
    protected $confirm_verify_otp = "confirm_otp_verify.php";
    protected $target_server;
    protected $handler;
    protected $sesshandler;
    protected $info_;
    
    private $endpoint_list = array("auth_login");
    
    protected function __construct($request) {
        header("Access-Control-Allow-Methods: POST");
        
        $this->init();
        $this->method = filter_input(INPUT_SERVER, "REQUEST_METHOD");
        $INPUT_SERVER = filter_input_array(INPUT_SERVER);                
        
        $this->args = API_Helper::setFilterRequest($request, $this->method);
		
        $this->endpoint = (strpos($this->args->service, "-") !== false?str_replace("-", "_", $this->args->service):$this->args->service);
        
        $this->logger = new APILogger(null, null, $this->args->username, $this->args->service);
		$param_client = json_encode($this->args);
		$this->logger->log_Client("\r\n\r\nPARAMS CLIENT: \r\n\r\n" . $param_client);
		
        $this->config = new API_Config();
        $this->handler = new API_Handler();
                        
        if (array_key_exists(0, $this->args) && !is_numeric($this->args[0])) {
            $this->verb = array_shift($this->args);
        }                
        
        if ($this->method == 'POST') {
            if (array_key_exists('HTTP_X_HTTP_METHOD', $INPUT_SERVER)) {
                if ($INPUT_SERVER["HTTP_X_HTTP_METHOD"] === 'DELETE') {
                    $this->method = 'DELETE';
                } else if ($INPUT_SERVER["HTTP_X_HTTP_METHOD"] === 'PUT') {
                    $this->method = 'PUT';
                } else {
                    throw new Exception("Unexpected Header");
                }
            }                                   
        }                
        
        $this->validateAPI();                                                        
    }
    
    private function init() {
        $this->sessionSaveHandler();
    }
    
    private function sessionSaveHandler() {
        if (empty($this->sesshandler)) {
            $this->sesshandler = new FileSessionHandler();
            session_set_save_handler(
                array($this->sesshandler, 'open'),
                array($this->sesshandler, 'close'),
                array($this->sesshandler, 'read'),
                array($this->sesshandler, 'write'),
                array($this->sesshandler, 'destroy'),
                array($this->sesshandler, 'gc')
                );

            // the following prevents unexpected effects when using objects as save handlers //
            register_shutdown_function('session_write_close');
        }
        session_start();        
    }
    
    public function processAPI() {
        return $this->_response($this->{$this->endpoint}($this->args));
    }

    private function _response($data, $status = 200) {
        header("HTTP/1.1 " . $status . APIException::$http_stat[$status]);
		$this->logger->log_Client("\r\n\r\RESPON CLIENT: \r\n\r\n" . $data);
        return $data;
    }   
    
    public function filterResponse($response) {
        $resp_pattern = array('{"result":', '{"departure":', '{"error":', '{"TripDetail":', '{"pergi":', '{"olplus":');
        
        foreach ($resp_pattern as $value) {
            $start=strpos($response, $value);
            
            if ($start !== false) {
                break;
            }
        }                
        
        return ($start !== false ?substr($response, $start):$start);
    }
    
    private function pre_execute($params) {
        return $this->user_activity_information($params);
    }
    
    private function user_activity_information($params) {
        /**
        * UAI = User Activity Information
        */       
        
        $ui = 'web_app';

        if (isset($this->args->device_model) && !empty($this->args->device_model)) {
            $device_model = $this->args->device_model;
            $ui = 'android';
        }
        
        if ($this->target_server === "https://www.ibe.co.id/webservice.sro/1.0/api.php") {
            // include these after session successfully created
            if (!in_array($this->endpoint, array("auth_login"))) {
                if (isset($this->info_->user)) {
                    $par = json_decode($params["r"]);
                    $par->input->user_name = $this->info_->user->name;
                    $par->input->user_password = $this->info_->user->password;                                        
                    
                    $par->UAI = new stdClass();
                    $par->UAI->access_via = $ui;
                    $par->UAI->access_datetime = date("Y-m-d H:i:s");
                    $par->UAI->device_model = $device_model;
                    $par->UAI->version_code = $this->args->version_code;
                    $par->UAI->ip_address = filter_input(INPUT_SERVER, "REMOTE_ADDR");
                    $par->UAI->api = $this->endpoint;
                    $par->api = true;

                    $params = array("r"=>json_encode($par));                                                             
                }
				else{
					$par = json_decode($params["r"]);
					$par->UAI = new stdClass();
                    $par->UAI->access_via = $ui;
                    $par->UAI->access_datetime = date("Y-m-d H:i:s");
                    $par->UAI->device_model = $device_model;
                    $par->UAI->version_code = $this->args->version_code;
                    $par->UAI->ip_address = filter_input(INPUT_SERVER, "REMOTE_ADDR");
                    $par->UAI->api = $this->endpoint;
                    $par->api = true;

                    $params = array("r"=>json_encode($par));   
				}
            }
			// print_r($params);exit;			
        }
        
        else {
            $par = new stdClass();
			$par->access_via = $ui;
			$par->access_datetime = date("Y-m-d H:i:s");
			$par->device_model = $device_model;
			$par->version_code = $this->args->version_code;
			$par->ip_address = filter_input(INPUT_SERVER, "REMOTE_ADDR");
			$par->api = $this->endpoint;
			
			$params["UAI"] = json_encode($par);
            $params["api"] = true;
                        
        }
        
        return $params;
    }
    
    public function execute($params) {
        try {
            $params = $this->pre_execute($params);
			
            $c = curl_init();
            curl_setopt($c, CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt($c, CURLOPT_SSLVERSION, 0);
            curl_setopt($c, CURLOPT_SSL_VERIFYHOST, 2);
            curl_setopt($c, CURLOPT_AUTOREFERER, 1);            
            curl_setopt($c, CURLINFO_HEADER_OUT,1);
            curl_setopt($c, CURLOPT_VERBOSE, 1); 
            curl_setopt($c, CURLOPT_HEADER, 0); 
            curl_setopt($c, CURLOPT_RETURNTRANSFER, 1); 
            curl_setopt($c, CURLOPT_USERAGENT, filter_input(INPUT_SERVER, "HTTP_USER_AGENT"));
			if (in_array($this->endpoint, array("auth_login"))) {
				curl_setopt($c, CURLOPT_TIMEOUT, 30);
			} else {
				curl_setopt($c, CURLOPT_TIMEOUT, 180);
			}
            
            if ("POST" === $this->config->METHOD_REQ_MAPPING[$this->endpoint]) {
                curl_setopt($c, CURLOPT_POST, 1);
                curl_setopt($c, CURLOPT_POSTFIELDS, $params);
                $extparams = "";
                
            } else {
                curl_setopt($c, CURLOPT_HTTPGET, 1);
                $extparams = "?". http_build_query($params);                                
            }
            
            curl_setopt($c, CURLOPT_URL, $this->target_server . $extparams);
           
            $response = curl_exec($c);
            curl_close($c);
			
            return $response;
        } catch (Exception $e) {
            throw $e;
        }        
    }
    
    public function validateAPI() {
        if (method_exists($this, $this->endpoint)) {
            $this->target_server = $this->baseUrl . "/" . $this->{$this->config->ENDPOINT_SERVER_MAPPING[$this->endpoint]};            
            
            if (!in_array($this->endpoint, $this->endpoint_list)) {
				if(($this->args->service != 'create-users' && $this->args->service != 'forget-password' && $this->args->service != 'merchant-notif-list' && $this->args->service != 'otp-verify' && $this->args->service != 'confirm-otp-verify' && $this->args->service != 'regis-billing' && $this->args->service != 'regis-billing-aktivasi' && $this->args->service != 'regis-billing-read' && $this->args->service != 'umrah-schedule-list') || !empty($this->args->apikey)){
					if (!array_key_exists('apikey', $this->args)) {
						APIException::_throwException(APIException::ERROR_PARAMS_NOAPIKEY, null, 10007);
					}

					else {
						$session_ = $this->sesshandler->read("info_{$this->args->apikey}");
						if (!empty($session_)) {
							$this->info_ = unserialize($this->sesshandler->read("info_{$this->args->apikey}"));
							
							if (isset($this->info_->user)) {
								if ($this->info_->user->name !== $this->args->username) {
									APIException::_throwException(APIException::ERROR_PARAMS_INVALIDUSER, null, 10007);
								}                

								else if ($this->args->apikey === $this->info_->user->key) {
									$this->user = $this->args->username;

									return true;
								}
							}                       
						}
						else {
							APIException::_throwException(APIException::ERROR_AUTH_MUSTLOGIN, null, 10007);
						}
					}
				}
				else if($this->args->service == 'create-users' || $this->args->service == 'regis-billing' || $this->args->service == 'regis-billing-aktivasi' || $this->args->service == 'regis-billing-read'){
					$this->info_->user->name = 'admin_billing';
					return true;
				}
				else if($this->args->service == 'merchant-notif-list'){
					$this->info_->user->name = 'support';
					return true;
				}
            }                        
        }
        
        else {
            APIException::_throwException(APIException::ERROR_PARAMS_INVALIDSERVICE, $this->endpoint, 10003);
        }
    }
    
    /**
     * 
     * Authentication
     */
    
    public function auth_login() {
        try {
            $args = array();
            $args["function"] = "api.signin";
            $args["input"] = array("user_name"=>$this->args->username, "user_password"=>$this->args->password);
            
            API_Helper::validateForms($this->args, $this->config->PARAM_AUTH_LOGIN);
			$dateCutOff = date("Y-m-d H:i:s");
			/*if($dateCutOff >= "2020-04-06 00:00:00"){
				$response = array(
					"result_status"=>"Request Timeout (408)", 
					"result_code"=>"10014", 
					"result_message"=>"Network Disruption", 
					"result_detail"=>"Mohon Maaf Untuk Sementara Operasional Asia Wisata dihentikan. Info jelasnya silahkan hubungi email info@asiawisata.com"
				);
			} else {*/
				$params = json_encode($args);
				$response_ = json_decode($this->filterResponse($this->execute(array("r" =>$params))));
            
				if (!$response_) {
					$response_new = new stdClass();
					$response_new->result = "ERROR";
					$response_new->resultText = "Login Not Valid";
					$response_ = json_encode($response_new);
				}

				$response = $this->handler->processXml($this->endpoint, $response_);                

				if (isset($response->user)) {
					$response->user->apikey = $response_->output->user->roleassignments->rowset[0]->user_password;

					unset($this->info_->user);

					$this->info_->user = new stdClass();
					$this->info_->user->id = $response->user->id;
					$this->info_->user->name = $response->user->name;
					$this->info_->user->fullname = $response->user->fullname;
					$this->info_->user->merchant_id = $response->user->merchant_id;
					$this->info_->user->merchanttype_id = $response->user->merchanttype_id;
					$this->info_->user->merchant_code = $response->user->merchant_code;
					$this->info_->user->phone = $response->user->phone;
					$this->info_->user->role_id = $response->user->role_id;
					$this->info_->user->role_uri = $response->user->role_uri;
					$this->info_->user->role_fullname = $response->user->role_fullname;
					$this->info_->user->status_role = $response->user->status_role;
					$this->info_->user->email = $response->user->email;
					$this->info_->user->password = $this->args->password;
					$this->info_->user->key = $response->user->apikey;
					$this->info_->session->id = $response->session->id;
					$this->info_->session->uuid = $response->session->uuid;
		   
					$this->sesshandler->write("info_{$response->user->apikey}", serialize($this->info_));
				}
			// }

            return json_encode($response);
        }
        
        catch (Exception $e) {
            throw $e;
        }
    }        
    
    public function auth_logout() {
        $args = array();
        $args["function"] = "api.signout";
        $args["input"] = array(
                    "id"=>$this->info_->session->id, //$session->session->id,
                    "UUID"=>$this->info_->session->uuid //$session->session->uuid
                );         
                
        $params = json_encode($args);        //print_r($this->execute(array("r" => $params))); exit;
        $exec = $this->filterResponse($this->execute(array("r" => $params)));
        if (!empty($exec)) {
            $exec = json_decode($exec);
        }
        
        $options = array("endpoint"=> $this->endpoint);
        $response = $this->handler->processXml($this->endpoint, $exec, $options);
        
        session_unset();
        session_destroy();
        session_write_close();
        setcookie(session_name(),'',0,'/');
        session_regenerate_id(true);
        $this->sesshandler->destroy("info_{$this->args->apikey}");
        unset($this->info_->user);
                
        return json_encode($response); //print_r($response); exit;
    }
    
    public function auth_check_session() {
        $ori_endpoint = $this->endpoint;
        $this->endpoint = 'auth_check_session';
        
        $args = array();
        $args["function"] = "api.checksession";
        $args["input"] = array(
            "user_id"=> (isset($this->info_->user)?$this->info_->user->id:9999999999)            
        );
        
        $params = json_encode($args); //print_r($this->execute(array("r" => $params))); exit;
        $exec = $this->filterResponse($this->execute(array("r"=>$params))); //print_r(json_decode($exec)); exit;
        $response = $this->handler->processXml($this->endpoint, json_decode($exec));
        
        $this->endpoint = $ori_endpoint;
        
        return json_encode($response);
    }
    
    public function get_user_by_name() {
        try {
            $endpoint_ = $this->endpoint;
            $this->endpoint = "get_user_by_name";
            $this->target_server = $this->baseUrl . "/" . $this->{$this->config->ENDPOINT_SERVER_MAPPING[$this->endpoint]};
            
            $args["function"] = "api.usermanager.users.byname";
            $args["input"] = array(
                "user_name" => $this->args->username
            );
            
            $response_ = $this->filterResponse($this->execute(array("r"=>json_encode($args))));
            $response = json_encode($this->handler->processXml($this->endpoint, json_decode($response_)));
            
            $this->endpoint = $endpoint_;
            $this->target_server = $this->baseUrl . "/" . $this->{$this->config->ENDPOINT_SERVER_MAPPING[$this->endpoint]};
            
            return $response;
        
        } catch (Exception $e) {
            throw $e;
        }
    }
    
    public function errorMessage($message) {
        return constant("APIException::".$message);
    }   
}