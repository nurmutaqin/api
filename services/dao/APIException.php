<?php
namespace ECS\Exception;
use \Exception;

class ECSException extends Exception {
    private $ERROR_TYPE = array(
        0 => "airlines"
    );
    
    public static $http_stat = array(
        200 => "OK",
        400 => "Bad Request",
        401 => "Unauthorized",
        405 => "Method Not Allowed",
        422 => "Unprocessable Entity",
        500 => "Internal Server Error"
    );
    
    public static $error = array(
        "status" => array(
            500 => array(
                "message" => array(
                    10000 => 'Unknown Error',
                    10001 => 'Connection to Server Error'                    
                )
            ),
            
            405 => array(
                "message" => array(
                    10002 => 'Invalid Method Request'
                )
            ),
            
            401 => array(
                "message" => array(
                    10007 => 'Authentication Failed',
                )
            ),

            400 => array(
                "message" => array(                        
                    10003 => 'Invalid Endpoint',
                    10004 => 'Parameter is Required',
                    10005 => 'Invalid Parameter Type',
                    10006 => 'Invalid Parameter Format',                    
                    10008 => 'Invalid XML'                            
                )
            ),
            
            200 => array( //IBE success, provider (sabre, mg, etc) error
                "message" => array(
                    10009 => 'Sabre-ws Error',
                    10010 => 'Hotel Error'
                )
            )
        )
    );       
    
    public static function _response($error) {
        $error = (object) $error;
        if (!empty($error->message)) {
            foreach (self::$error["status"] as $key=>$value) {
                if (array_key_exists($error->code, self::$error["status"][$key]["message"])) {
                    $http_status = $key;
                    break;
                }
            }            
                        
            $http_text = self::$http_stat[$http_status];
            $exception_type = self::$error["status"][$http_status]["message"][$error->code];            
            
            $response = array(
                "result_status"=>"$http_text ($http_status)", 
                "result_code"=>$error->code, 
                "result_message"=>$exception_type, 
                "result_detail"=>$error->message
            );
            
        } else {
            $http_status = 500;
            $http_text = self::$http_stat[$http_status];
            
            $response = array(
                "result_status"=>"$http_text ($http_status)",
                "result_code"=>10000, 
                "result_message"=>self::$error["status"][500]["message"][10000], 
                "result_detail"=>$http_text
            );
        }
        
//        if ($http_status == "401") {
//            header('WWW-Authenticate: Basic realm="API"');
//        }
        
//        header("HTTP/1.1 " . $http_status . " " . $http_text);
        return $response;
    }
    
    public static function _throwException($dao, $params) {        
        try {
            $dao->db_call()->beginTransaction();
            
            if (isset($params->error_desc)) {
                $sql_newid = "SELECT NEXTVAL('error_log_id_seq')";
                $id = $dao->db_call()->query($sql_newid, \PDO::FETCH_ASSOC)->fetch();                        

                $dao->save($id["nextval"], $params);
            }
            
            $dao->db_call()->commit();            
            
        } catch (Exception $e) {
            $dao->db_call()->rollBack();            
            throw $e;
        }                
    }        
}