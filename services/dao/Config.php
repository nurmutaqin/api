<?php
/**
 * Description of Config
 *
 * @author Gunawan
 */

namespace API\Config;
use \Exception;

class Config {
    private static $config = null;
    
    public static function getConfig($directive = null) {
        if ($directive === null) {
            return self::getConfigSetting();
        }
        
        $data = self::getConfigSetting();
        
        if (!array_key_exists($directive, $data)) {
            throw new Exception('Unknown config directive:' . $directive);
        }
        
        return $data[$directive];
    }
    
    private static function getConfigSetting() {
        if (!empty(self::$config)) {
            return self::$config;
        }
        
        self::$config = parse_ini_file(__DIR__.'/config.ini', true);
        return self::$config;
    }
}
