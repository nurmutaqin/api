<?php
//namespace API;

class Index {    
    public function loadClass($class) {
        $classes = array(
            "API\Config\Config" => "/Config.php",
            "API\Dao\APIDao" => "/APIDao.php",
            "API\Exception\APIException" => "/APIException.php"
        );

        if (!array_key_exists($class, $classes)) {
            die("Class $class not found.");
        }

        require_once __DIR__ . $classes[$class];
    }
}