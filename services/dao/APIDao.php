<?php
namespace API\Dao;

use \PDO;
use \PDOStatement;
use \API\Config\Config;
use \Exception;

/**
 * Description of APIDao
 *
 * @author Gunawan
 */
class APIDao {
    public $db = null;
    public $foo;
    
    public function destruct__() {
        $this->db = null;
    }        
    
    public function db_call() {
        if ($this->db !== null) {
            return $this->db;
        }

        $config = Config::getConfig('db');
        
        try {
            $this->db=new PDO("pgsql:host=$config[host];port=$config[port];dbname=$config[dbname];user=$config[user];password=$config[password]");

        } catch (Exception $e) {
            throw new Exception('DB connection error: '. $e->getMessage());
        }
        
        return $this->db;
    }
    
    private function execute($sql, $params) {
        $statement = $this->db_call()->prepare($sql);
        $this->executeStatement($statement, $params);
        
        if (!$statement->rowCount()) {
            throw new Exception("Data not found");
        }
        
        return $this->getById($this->db_call()->lastInsertId());
    }
    
    private function executeStatement(PDOStatement $statement, array $params) {
        if ($statement->execute($params) === false) {
            self::throwDbError($this->db_call()->errorInfo());
        }
    }
    
    public function getById($id) {
        return $row = $this->query('SELECT * FROM mobile_activity_log WHERE id = ' . (int) $id)->fetch();
    }        
    
    private function query($sql) {
        $statement = $this->db_call()->query($sql, \PDO::FETCH_ASSOC);
        if (!$statement) {
            self::throwDbError($this->db_call()->errorInfo());
        }
        
        return $statement;
    }
    
    public function save($id, $args) {
        $params = array(
            ":id"=> $id,
            ":error_date"=> $args->date,
            ":error_user_id"=> $args->user_id,
            ":error_user_name"=> $args->user_name,
            ":error_request"=> json_encode($args->request),
            ":airlines_id"=> $args->error_type,
            ":error_desc"=> $args->error_desc
        );
        
        return $this->insert($params);
    }
    
    private function insert($params) {
        $sql = "INSERT INTO mobile_activity_log (id, error_date, error_user_id, error_user_name, error_request, airlines_id, error_desc) VALUES(:id, :error_date, :error_user_id, :error_user_name, :error_request, :airlines_id, :error_desc)";
        return $this->execute($sql, $params);
    }
    
    private static function throwDbError(array $errorinfo) {
        throw new Exception("Database error $errorinfo[0], $errorinfo[1], $errorinfo[2]");
    }
}